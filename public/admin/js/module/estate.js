$(function() {

    $('.city-list').on('change', function() {
        var cityId = $(this).val();
        var town = $('.town-list');

        town.html('');

        $.post('advertising/towns', {'cityId': cityId}, function(response) {
            if (response.success) {
                response.data.map( function(item) {
                    town.append('<option value="'+ item.value +'">'+ item.label +'</option>');
                });
            }
        }, 'json');


    });


    $('.town-list').on('change', function() {
        var townId = $(this).val();
        var district = $('.district-list');

        district.html('');

        $.post('advertising/districts', {'townId': townId}, function(response) {
            if (response.success) {
                response.data.map( function(item) {
                    district.append('<option value="'+ item.value +'">'+ item.label +'</option>');
                });
            }
        }, 'json');


    });

    $('.investor').on('change', function () {
        var investor = $('#investorD'),
            value = $(this).val();

        if (value == 1) {
            investor.removeClass('hide');
        } else {
            investor.addClass('hide');
        }
    });


    $('.kind-list').on('change', function() {
        var kindId = $(this).val();
        var type = $('.type-list'),
            apart = $('#apart'),
            arsa = $('#arsa'),
            dukkan = $('#dukkan');

        type.html('');

        $.post('advertising/types', {'kindId': kindId}, function(response) {
            if (response.success) {
                response.data.map( function(item) {
                    type.append('<option value="'+ item.value +'">'+ item.label +'</option>');
                });
            }
        }, 'json');

        if (kindId == "3") {
            apart.addClass('hide');
            arsa.removeClass('hide');
        } else {
            if (apart.hasClass('hide')) {
                apart.removeClass('hide');
                arsa.addClass('hide');
            }
        }

        if (kindId == "2") {
            dukkan.removeClass('hide');
        } else {
            if (! dukkan.hasClass('hide')) {
                dukkan.addClass('hide');
            }
        }

    });



    $('#video-info-button').on('click', function(){
        var url = $(this).data('url');
        var videoDetail = $('#video-detail');

        $('input[name="videoId"]', videoDetail).val('');
        $('input[name="title"]', videoDetail).val('');
        $('textarea[name="summary"]', videoDetail).val('');
        $('img.img-thumbnail', videoDetail).attr('src', '../public/admin/img/noimage.jpg');
        $('input[name="imageUrl"]', videoDetail).val('');

        videoDetail.addClass('hide');

        $.post(url, {video: $('#video').val()}, function(response){
            if (! response.error){
                $('input[name="videoId"]', videoDetail).val(response.id);
                $('input[name="title"]', videoDetail).val(response.entry.title.$t);
                $('textarea[name="summary"]', videoDetail).val(response.entry.content.$t);
                $('img.img-thumbnail', videoDetail).attr('src', response.thumbnail);
                $('input[name="imageUrl"]', videoDetail).val(response.thumbnail);

                videoDetail.removeClass('hide');
            }

        }, 'json');

        return false;
    });

});