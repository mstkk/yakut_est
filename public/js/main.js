var baseUrl     = document.getElementsByTagName('base')[0].href;
var language    = document.getElementsByTagName('html')[0].getAttribute('lang');
var queryString = (function (a) {
    if (a === "") return {};
    var b = {};
    for (var i = 0; i < a.length; ++i) {
        var p = a[i].split('=');
        if (p.length !== 2) continue;
        b[p[0]] = decodeURIComponent(p[1].replace(/\+/g, " "));
    }
    return b;
})(window.location.search.substr(1).split('&'));

jQuery(window).load(function () {
    var $ = jQuery;

    $('img:not(".logo-img")').each(function () {
        if (/MSIE (\d+\.\d+);/.test(navigator.userAgent)) {
            var ieversion = new Number(RegExp.$1)
            if (ieversion >= 9)
                if (typeof this.naturalWidth === "undefined" || this.naturalWidth === 0) {
                    this.src = "http://placehold.it/" + ($(this).attr('width') || this.width || $(this).naturalWidth()) + "x" + (this.naturalHeight || $(this).attr('height') || $(this).height());
                }
        } else {
            if (!this.complete || typeof this.naturalWidth === "undefined" || this.naturalWidth === 0) {
                this.src = "http://placehold.it/" + ($(this).attr('width') || this.width || $(this).naturalWidth()) + "x" + (this.naturalHeight || $(this).attr('height') || $(this).height());
            }
        }
    });
});


$(function () {

    $('[data-toggle="tooltip"]').tooltip();
    $('[data-tooltip="tooltip"]').tooltip();
    $('[data-tooltip="true"]').tooltip();

    $('.fancybox').fancybox({
        padding: 0,
        openEffect: 'elastic',
        openSpeed: 150,
        closeEffect: 'elastic',
        closeSpeed: 150,
        closeClick: false
    });
	
	$('#wecallyou').on('click', function(e) {
        e.preventDefault();
        $('#modal-wecallyou').modal('show');
    });



    $('#wecallyou-button').on('click', function (e) {
        e.preventDefault();

        var modal = $('#modal-wecallyou');
        var fullname = $('input[name="fullname"]', modal).val();
        var phone = $('input[name="phone"]', modal).val();
        var alert = $('#modal-wecallyou').find('.alert');
        var href = $('#wecallyou').attr('href');

        alert.addClass('hide').html('');

        $.post(href, {'fullname': fullname, 'phone': phone}, function(response) {

            if (response.success) {
                modal.find('.modal-body').html(response.message);
                $('#wecallyou-button').hide();
            } else {
                alert.html(response.message);
                alert.removeClass('hide');
            }
        }, 'json');
    });
	
    $('.galeriListe').fancybox({
        padding: 0,
        openEffect: 'elastic',
        openSpeed: 150,
        closeEffect: 'elastic',
        closeSpeed: 150,
        closeClick: false,
        autoPlay: true,
        playSpeed: 3000,
        //overlayShow: true
    });
    $(".mask-phone").mask("0(999) 999 99 99", {placeholder: "_"});
    $(".numeric").numeric();


    $('.share-box a').on('click', function () {
        window.open($(this).attr('href'), 'sharewindow', 'width=600,height=400');
        return false;
    });

    $('#kinds').on('change', function () {
        var status = $(this).attr('data-status')
        builds = $(this).attr('data-build'),
            emergency = $(this).attr('data-emergency'),
            investor = $(this).attr('data-investor'),
            monthly = $(this).attr('data-monthly'),
            value = $(this).val();

        if (builds == "" && emergency == "" && investor == "" && monthly == "") {
            window.location.href = "ilanlar?status=" + status + '&kind=' + value;
        }
        if (builds) {
            window.location.href = "ilanlar?status=" + status + '&kind=' + value + '&build=' + builds;
        }
        if (emergency) {
            window.location.href = "ilanlar?status=" + status + '&kind=' + value + '&emergency=' + emergency;
        }
        if (investor) {
            window.location.href = "ilanlar?status=" + status + '&kind=' + value + '&investor=' + investor;
        }
        if (monthly) {
            window.location.href = "ilanlar?status=" + status + '&kind=' + value + '&monthly=' + monthly;
        }
    });


});


function initialize() {
    var $         = jQuery,
        mapCanvas = $('.map-canvas');

    mapCanvas.each(function () {
        var $this           = $(this),
            zoom            = 16,
            lat             = -34,
            lng             = 150,
            scrollwheel     = false,
            draggable       = true,
            mapType         = google.maps.MapTypeId.ROADMAP,
            title           = '',
            contentString   = '',
            dataZoom        = $this.attr('data-zoom'),
            dataLat         = $this.attr('data-lat'),
            dataLng         = $this.attr('data-lng'),
            dataType        = $this.attr('data-type'),
            dataScrollwheel = $this.attr('data-scrollwheel'),
            dataHue         = $this.attr('data-hue'),
            dataTitle       = $this.attr('data-title'),
            dataContent     = $this.attr('data-content');

        if (dataZoom !== undefined && dataZoom !== false) {
            zoom = parseFloat(dataZoom);
        }

        if (dataLat !== undefined && dataLat !== false) {
            lat = parseFloat(dataLat);
        }

        if (dataLng !== undefined && dataLng !== false) {
            lng = parseFloat(dataLng);
        }

        if (dataScrollwheel !== undefined && dataScrollwheel !== false) {
            scrollwheel = dataScrollwheel;
        }

        if (dataType !== undefined && dataType !== false) {
            if (dataType == 'satellite') {
                mapType = google.maps.MapTypeId.SATELLITE;
            } else if (dataType == 'hybrid') {
                mapType = google.maps.MapTypeId.HYBRID;
            } else if (dataType == 'terrain') {
                mapType = google.maps.MapTypeId.TERRAIN;
            }
        }

        if (dataTitle !== undefined && dataTitle !== false) {
            title = dataTitle;
        }

        if (navigator.userAgent.match(/iPad|iPhone|Android/i)) {
            draggable = false;
        }

        var mapOptions = {
            zoom: zoom,
            scrollwheel: scrollwheel,
            draggable: draggable,
            center: new google.maps.LatLng(lat, lng),
            mapTypeId: mapType
        };

        var map = new google.maps.Map($this[0], mapOptions);

        //var image = 'public/img/map-marker.svg';

        if (dataContent !== undefined && dataContent !== false) {
            contentString = '<div class="map-content">' +
                '<h3 class="title">' + title + '</h3>' +
                dataContent +
                '</div>';
        }

        var infowindow = new google.maps.InfoWindow({
            content: contentString
        });

        var marker = new google.maps.Marker({
            position: new google.maps.LatLng(lat, lng),
            map: map,
            //icon     : image,
            title: title
        });

        if (dataContent !== undefined && dataContent !== false) {
            google.maps.event.addListener(marker, 'click', function () {
                infowindow.open(map, marker);
            });
        }

        if (dataHue !== undefined && dataHue !== false) {
            var styles = [
                {
                    stylers: [
                        {hue: dataHue}
                    ]
                }
            ];

            map.setOptions({styles: styles});
        }

        $('#mapModal').on('shown.bs.modal', function () {
            google.maps.event.trigger(map, 'resize');
            map.setCenter(new google.maps.LatLng(lat, lng));
        });


        $('a[href="#detMap"]').on('shown.bs.tab', function () {
            google.maps.event.trigger(map, 'resize');
            map.setCenter(new google.maps.LatLng(lat, lng));
        });
    });
}

function HariciLinkler() {

    if (!document.getElementsByTagName) return;
    var linkler     = document.getElementsByTagName("a");
    var linklerAdet = linkler.length;
    for (var i = 0; i < linklerAdet; i++) {
        var tekLink = linkler[i];
        if (tekLink.getAttribute("href") && tekLink.getAttribute("rel") == "external") {
            tekLink.target = "_blank";
        }

    }

}

$(function () {
    HariciLinkler();
});


$(function () {

    $('.city-list').on('change', function () {
        var cityId = $(this).val();
        var town   = $('.town-list');

        town.html('');

        $.post('ilanlar/towns', {'cityId': cityId}, function (response) {
            if (response.success) {
                response.data.map(function (item) {
                    town.append('<option value="' + item.value + '">' + item.label + '</option>');
                });
            }
        }, 'json');


    });


    $('.town-list').on('change', function () {
        var townId   = $(this).val();
        var district = $('.district-list');

        district.html('');

        $.post('ilanlar/districts', {'townId': townId}, function (response) {
            if (response.success) {
                response.data.map(function (item) {
                    district.append('<option value="' + item.value + '">' + item.label + '</option>');
                });
            }
        }, 'json');

    });


    $('.kind-list').on('change', function () {
        var kindId = $(this).val();
        var type   = $('.type-list');

        type.html('');

        $.post('advertising/types', {'kindId': kindId}, function (response) {
            if (response.success) {
                response.data.map(function (item) {
                    type.append('<option value="' + item.value + '">' + item.label + '</option>');
                });
            }
        }, 'json');


    });


    $('#video-info-button').on('click', function () {
        var url         = $(this).data('url');
        var videoDetail = $('#video-detail');

        $('input[name="videoId"]', videoDetail).val('');
        $('input[name="title"]', videoDetail).val('');
        $('textarea[name="summary"]', videoDetail).val('');
        $('img.img-thumbnail', videoDetail).attr('src', '../public/admin/img/noimage.jpg');
        $('input[name="imageUrl"]', videoDetail).val('');

        videoDetail.addClass('hide');

        $.post(url, {video: $('#video').val()}, function (response) {
            if (!response.error) {
                $('input[name="videoId"]', videoDetail).val(response.id);
                $('input[name="title"]', videoDetail).val(response.entry.title.$t);
                $('textarea[name="summary"]', videoDetail).val(response.entry.content.$t);
                $('img.img-thumbnail', videoDetail).attr('src', response.thumbnail);
                $('input[name="imageUrl"]', videoDetail).val(response.thumbnail);

                videoDetail.removeClass('hide');
            }

        }, 'json');

        return false;
    });


    $('.update-childs').on('change', function () {
        var element = $(this);
        var id      = element.val();
        var target  = $(element.data('target'));
        var url     = element.data('url');

        target.html('');

        if (id.length > 0) {
            $.post(url, {'id': id}, function (response) {
                if (response.success) {
                    response.data.map(function (item) {
                        target.append('<option value="' + item.value + '">' + item.label + '</option>');
                    });

                    target.trigger("chosen:updated");
                }
            }, 'json');
        } else {
            target.trigger("chosen:updated");
        }
    });

});

/**********************************************************************************************/
/**********************************************************************************************/
/* CSS Design *********************************************************************************/

$(function () {
    $(".searchButton").click(function () {
        $("#searchInput").toggleClass("searchOpen");
    });
});


/**********************************************************************************************/
/**********************************************************************************************/
/* BX Slider Plugin ***************************************************************************/

$(document).ready(function () {
    $('.bxsliderBig').bxSlider({
        pagerCustom: '#bx-pager',
        slideMargin: 0
    });

    $('.bxsliderThumb').bxSlider({
        prevSelector: '#bxsliderThumb-next',
        nextSelector: '#bxsliderThumb-prev',
        nextText: '<i class="fa fa-angle-up"></i>',
        prevText: '<i class="fa fa-angle-down"></i>',
        minSlides: 4,
        mode: 'vertical',
        slideMargin: 5,
        pager: false,
        slideMargin: 0
    });

});

$(function () {
    $("#closer").on('click', function () {
        var $row = $('#result');

        if (document.getElementById('result').style.display == "none") {
            $row.slideDown(500);
        } else {
            $row.slideUp(500);
        }

    })
});