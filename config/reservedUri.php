<?php

$config['reservedUri'] = array();
$config['reservedUri']['tr']['@home'] = './';
$config['reservedUri']['tr']['@advertising'] = 'ilanlar';
$config['reservedUri']['tr']['@contact'] = 'iletisim';
$config['reservedUri']['tr']['@content'] = 'icerik';
$config['reservedUri']['tr']['@news'] = 'haber';
$config['reservedUri']['tr']['@consultant'] = 'danismanlar';
$config['reservedUri']['tr']['@expertise'] = 'ekspertiz';
$config['reservedUri']['tr']['@wecallyou'] = 'biz-sizi-arayalim';
