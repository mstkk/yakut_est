<?php

use Sirius\Admin\Manager;

class AdvertisingAdminController extends Manager
{
    public $moduleTitle = 'İlanlar';
    public $module = 'advertising';
    public $table = 'advertisings';
    public $model = 'advertising';
    public $type = 'public';
    public $icon = 'fa fa-building-o';
    public $menuPattern = array(
        'title' => 'title',
        'hint' => 'title',
        'link' => array('slug', 'id'),
        'moduleLink' => true,
        'language' => true
    );

    // Arama yapılacak kolonlar.
    public $search = array('id','title','estateNo');


//     Filtreleme yapılacak querystring/kolonlar.
     public $filter = array('statusId', 'isActive', 'price','updateDate');

    public $actions = array(
        'towns' => 'list',
        'districts' => 'list',
        'types' => 'list',
        'records' => 'list',
        'estateinsert' => 'estate-insert',
        'order' => 'list',
        'insert' => 'insert',
        'approve' => 'list',
        'isActive' => 'list',
        'update' => 'update',
        'delete' => 'delete',
        'alldateupdate' => 'alldateupdate',
        'images' => 'image-list',
        'imageOrder' => 'image-list',
        'imageInsert' => 'image-insert',
        'imageUpdate' => 'image-update',
        'imageDelete' => 'image-delete',
    );

    public function estateinsert()
    {
        $this->load->model('estateinsert');
        $this->load->model('advertising');
        $this->load->helper('url');
        $record = $this->estateinsert->find($this->uri->segment(3));
        $this->load->vars('public', array('js' => array(
            '../public/admin/plugin/ckeditor/ckeditor.js',
            '../public/admin/plugin/ckfinder/ckfinder.js'
        )));
        $this->render('insertestate', array(

            'record' => $record));

        if ($this->input->post('cityId')) {

                $this->utils
                    ->uploadInput('imageFile')
                    ->minSizes(10, 10)
                    ->addProcessSize('normal', 506, 470, 'ilanlar/thumb', 'thumbnail')
                    ->addProcessSize('big', 1024, null, 'ilanlar', 'fit-width');




            if( $_FILES['imageFile']['name'] != "")
            {
                $image = $this->utils->imageUpload(false);
                $this->modelData['image'] = $image;
            }
            if ($this->input->post('imageUrl')) {
                $this->modelData['image'] = $this->utils->imageDownload(false, $this->input->post('imageUrl'));
            }
            if( !$this->input->post('imageUrl') && $_FILES['imageFile']['name'] == "" ) {
                $this->modelData['image'] = $this->utils->imageDownload(false, 'http://nazaremlak.com.tr/public/upload/ilanlar/'.$record->image);
            }



            if (! empty($this->modelData['image']['name'])) {
                $this->load->library('SimpleImage');
                $image_normal = $this->simpleimage->load('../public/upload/ilanlar/'. $this->modelData['image']['name']);
                $image_normal->overlay('../public/img/watermark.png', 'center', .3);
                $image_normal->save('../public/upload/ilanlar/' . $this->modelData['image']['name']);
                $image_thumb = $this->simpleimage->load('../public/upload/ilanlar/thumb/'. $this->modelData['image']['name']);
                $image_thumb->overlay('../public/img/watermark.png', 'center', .3);
                $image_thumb->save('../public/upload/ilanlar/thumb/' . $this->modelData['image']['name']);
            }
            $dataimg=$this->modelData['image']['name'];
            if (! $this->utils->isAlert()) {
                $success = $this->advertising->insertestate($dataimg);
                if ($success) {
                    $this->utils->setAlert('success', 'İlan Başarıyla Taşındı !.');
                    redirect(clink(array('advertising/update',$success)));
                }
                $this->utils->setAlert('warning', 'İlan Taşınırken Hata!.');
            }
        }

    }

    public function approve()
    {
        if (! $record = $this->appmodel->id($this->uri->segment(3))) {
            show_404();
        }

        $this->db->where('id',$record->id);
        if ( $record->vitrinIlan == 1 ) {

            $this->db->update(
                $this->table, array(
                    'vitrinIlan' => 0
                )
            );

        } else {

            $this->db->update(
                $this->table, array(
                    'vitrinIlan' => 1
                )
            );

        }

        redirect($this->module.'/records');

    }

    public function isActive()
    {
        if (! $record = $this->appmodel->id($this->uri->segment(3))) {
            show_404();
        }

        $this->db->where('id',$record->id);
        if ( $record->isActive == 1 ) {

            $this->db->update(
                $this->table, array(
                    'isActive' => 0
                )
            );

        } else {

            $this->db->update(
                $this->table, array(
                    'isActive' => 1
                )
            );

        }

        redirect($this->module.'/records');

    }

    public function towns()
    {
        $json = array('success' => true, 'data' => array());

        $cityId = $this->input->post('cityId');
        $towns = $this->appmodel->getTowns($cityId);

        $json['data'][] = array('value' => '', 'label' => 'Seçiniz');
        foreach ($towns as $town) {
            $json['data'][] = array('value' => $town->id, 'label' => $town->title);
        }

        echo json_encode($json);
    }



    public function districts()
    {
        $json = array('success' => true, 'data' => array());

        $townId = $this->input->post('townId');
        $districts = $this->appmodel->getDistricts($townId);

        $json['data'][] = array('value' => '', 'label' => 'Seçiniz');
        foreach ($districts as $district) {
            $json['data'][] = array('value' => $district->id, 'label' => $district->title);
        }

        echo json_encode($json);
    }


    public function types()
    {
        $json = array('success' => true, 'data' => array());

        $kindId = $this->input->post('kindId');
        $types = $this->appmodel->getTypes($kindId);


        $json['data'][] = array('value' => '', 'label' => 'Seçiniz');
        foreach ($types as $type) {
            $json['data'][] = array('value' => $type->id, 'label' => $type->title);
        }

        echo json_encode($json);
    }



    protected function insertRequest()
    {
        if ($this->uri->segment(3) > 0) {
            $this->viewData['parentId'] = $this->uri->segment(3);

            // Navigasyon eklemeleri yapılır
            $parents = $this->appmodel->parents($this->viewData['parentId']);

            foreach ($parents as $bread){
                $this->breadcrumb($bread['title'], $bread['url']);
            }
        }



        $this->load->vars('public', array('js' => array(
            '../public/admin/plugin/ckeditor/ckeditor.js',
            '../public/admin/plugin/ckfinder/ckfinder.js'
        )));
    }

    protected function insertValidateRules()
    {
        $this->form_validation->set_rules('title', 'Lütfen Başlık yazınız.', 'required');

        if ($this->input->post('autoSlug') !== 'true') {
            $this->form_validation->set_rules('slug', 'Lütfen slug yazınız.', 'required');
        }
    }

    protected function insertAfterValidate()
    {
        $this->utils
            ->uploadInput('imageFile')
            ->minSizes(10, 10)
            ->addProcessSize('normal', 506, 470, 'ilanlar/thumb', 'thumbnail')
            ->addProcessSize('big', 1024, null, 'ilanlar', 'fit-width');


        if ($this->input->post('imageUrl')) {
            $this->modelData['image'] = $this->utils->imageDownload(false, $this->input->post('imageUrl'));
        } else {
            $image = $this->utils->imageUpload(false);
            $this->modelData['image'] = $image;
        }
        if (! empty($this->modelData['image']['name'])) {
            $this->load->library('SimpleImage');
            $image_normal = $this->simpleimage->load('../public/upload/ilanlar/'. $this->modelData['image']['name']);
            $image_normal->overlay('../public/img/watermark.png', 'center', .3);
            $image_normal->save('../public/upload/ilanlar/' . $this->modelData['image']['name']);
            $image_thumb = $this->simpleimage->load('../public/upload/ilanlar/thumb/'. $this->modelData['image']['name']);
            $image_thumb->overlay('../public/img/watermark.png', 'center', .3);
            $image_thumb->save('../public/upload/ilanlar/thumb/' . $this->modelData['image']['name']);
        }
    }


    protected function updateRequest($record)
    {

        $this->load->vars('public', array('js' => array(
            '../public/admin/plugin/ckeditor/ckeditor.js',
            '../public/admin/plugin/ckfinder/ckfinder.js'
        )));
    }

    protected function updateValidateRules()
    {
        $this->form_validation->set_rules('title', 'Lütfen Başlık yazınız.', 'required');

        if ($this->input->post('autoSlug') !== 'true') {
            $this->form_validation->set_rules('slug', 'Lütfen slug yazınız.', 'required');
        }
    }

    protected function updateAfterValidate($record)
    {

        $this->utils
            ->uploadInput('imageFile')
            ->minSizes(10, 10)
            ->addProcessSize('normal', 506, 470, 'ilanlar/thumb', 'thumbnail')
            ->addProcessSize('big', 1024, null, 'ilanlar', 'fit-width');


        if ($this->input->post('imageUrl')) {
            $this->modelData['image'] = $this->utils->imageDownload(false, $this->input->post('imageUrl'), $record->image);
        } else {
            $image = $this->utils->imageUpload(false, $record->image);
            $this->modelData['image'] = $image;
        }
        if (! empty($this->modelData['image']['name'])) {
            $this->load->library('SimpleImage');
            $image_normal = $this->simpleimage->load('../public/upload/ilanlar/'. $this->modelData['image']['name']);
            $image_normal->overlay('../public/img/watermark.png', 'center', .3);
            $image_normal->save('../public/upload/ilanlar/' . $this->modelData['image']['name']);
            $image_thumb = $this->simpleimage->load('../public/upload/ilanlar/thumb/'. $this->modelData['image']['name']);
            $image_thumb->overlay('../public/img/watermark.png', 'center', .3);
            $image_thumb->save('../public/upload/ilanlar/thumb/' . $this->modelData['image']['name']);
        }
    }


    public function images()
    {
        if (! $parent = $this->appmodel->id($this->uri->segment(3))) {
            show_404();
        }

        $records = array();
        $pagination = null;
        $recordCount = $this->appmodel->imageCount($parent);

        if ($recordCount > 0) {
            $config = array(
                'base_url' => clink(array($this->module, 'images', $parent->id)),
                'total_rows' => $recordCount,
                'per_page' => 20
            );

            $this->load->library('pagination');
            $this->pagination->initialize($config);


            $records = $this->appmodel->imageAll($parent, $this->pagination->per_page, $this->pagination->offset);
            $pagination = $this->pagination->create_links();
        }



        $this->breadcrumb($parent->title, clink(array($this->module, 'images', $parent->id)));
        $this->breadcrumb('Resimler');
        $this->viewData['parent'] = $parent;
        $this->viewData['records'] = $records;
        $this->viewData['pagination'] = $pagination;

        $this->load->vars('public', array('js' => array(
            '../public/admin/plugin/plupload/plupload.js',
            '../public/admin/plugin/plupload/plupload.flash.js',
            '../public/admin/plugin/plupload/plupload.html4.js',
            '../public/admin/plugin/plupload/plupload.html5.js',
        )));

        $this->render('images/records');
    }




    public function imageInsert()
    {
        if (! $parent = $this->appmodel->id($this->uri->segment(3))) {
            echo json_encode(array(
                'jsonrpc'	=> '2.0',
                'error'		=> array('code' => '500', 'message' => 'Kayıt bulunamadı.'),
                'id'		=> 'id'
            ));
        }

        $this->load->library('image_lib');

        $this->utils
            ->setPlupload()
            ->uploadInput('file')
            ->minSizes(10, 10)
            ->addProcessSize('thumb', 506, 470, 'ilanlar/thumb', 'thumbnail')
            ->addProcessSize('normal', 1024, null, 'ilanlar/normal', 'fit-width');

        $image = $this->utils->imageUpload(true);
        $img = $image['name'];

        $config['image_library'] = 'gd2';
        $config['source_image']     = '../public/upload/ilanlar/normal/'.$img;
        $config['wm_type']          = 'overlay';
        $config['wm_overlay_path']  = '../public/img/watermark.png'; //the overlay image
        $config['wm_opacity']       = 20;
        $config['wm_vrt_alignment'] = 'middle';
        $config['wm_hor_alignment'] = 'middle';

        $this->image_lib->initialize($config);
        if (! $this->image_lib->watermark()) {
            echo $this->image_lib->display_errors();
            exit;
        }

        $this->modelData['image'] = $image;

        $success = $this->appmodel->imageInsert($parent, $this->modelData);

        if ($success) {
            echo json_encode(array(
                'jsonrpc'	=> '2.0',
                'error'		=> array(),
                'id'		=> 'id'
            ));

            return;
        }
    }




    public function imageUpdate()
    {
        if (! $record = $this->appmodel->image($this->uri->segment(3))) {
            show_404();
        }

        $this->load->library('image_lib');

        $parent = $this->appmodel->id($record->advertisingId);

        if ($this->input->post()) {

            if ($this->form_validation->run() === false) {
                $this->utils->setAlert('danger', $this->form_validation->error_string('<div>&bull; ', '</div>'));
            }

            $this->utils
                ->uploadInput('imageFile')
                ->minSizes(10, 10)
                ->addProcessSize('thumb', 506, 470, 'ilanlar/thumb', 'thumbnail')
                ->addProcessSize('normal', 1024, null, 'ilanlar/normal', 'fit-width');


            if ($this->input->post('imageUrl')) {

                $image = $this->utils->imageDownload(false, $this->input->post('imageUrl'), $record->image);
                $img = $image['name'];

                $config['image_library']    = 'gd2';
                $config['source_image']     = '../public/upload/ilanlar/normal/'.$img;
                $config['wm_type']          = 'overlay';
                $config['wm_overlay_path']  = '../public/img/watermark.png'; //the overlay image
                $config['wm_opacity']       = 20;
                $config['wm_vrt_alignment'] = 'middle';
                $config['wm_hor_alignment'] = 'middle';

                $this->image_lib->initialize($config);
                if (! $this->image_lib->watermark()) {
                    echo $this->image_lib->display_errors();
                    exit;
                }

                $this->modelData['image'] = $image;
            } else {

                $image = $this->utils->imageUpload(false, $record->image);
                $img = $image['name'];

                $config['image_library']    = 'gd2';
                $config['source_image']     = '../public/upload/ilanlar/normal/'.$img;
                $config['wm_type']          = 'overlay';
                $config['wm_overlay_path']  = '../public/img/watermark.png'; //the overlay image
                $config['wm_opacity']       = 20;
                $config['wm_vrt_alignment'] = 'middle';
                $config['wm_hor_alignment'] = 'middle';

                $this->image_lib->initialize($config);
                if (! $this->image_lib->watermark()) {
                    echo $this->image_lib->display_errors();
                    exit;
                }

                $this->modelData['image'] = $image;
            }

            if (! $this->utils->isAlert()) {
                $success = $this->appmodel->imageUpdate($record, $this->modelData);

                if ($success) {
                    $this->utils->setAlert('success', 'Kayıt düzenlendi.');
                    redirect(clink(array($this->module, 'imageUpdate', $record->id)));
                }
                $this->utils->setAlert('warning', 'Kayıt düzenlenmedi.');
            }
        }

        $this->breadcrumb($parent->title, clink(array($this->module, 'images', $parent->id)));
        $this->breadcrumb('Kayıt Düzenle');

        $this->viewData['parent'] = $parent;
        $this->viewData['record'] = $record;

        $this->render('images/update');
    }




    public function imageDelete()
    {
        // Ajax sorgusu  ise toplu silme uygulanır
        if ($this->input->is_ajax_request()) {
            $ids = $this->input->post('ids');

            if (count($ids) == 0) {
                $this->utils->setAlert('danger', 'Lütfen kayıt seçiniz.');
                echo $this->input->server('HTTP_REFERER');
            }

            $success = $this->appmodel->imageDelete($ids);

            if ($success) {
                $this->utils->setAlert('success', "Kayıtlar başarıyla silindi.");
                echo $this->input->server('HTTP_REFERER');
            }

            return true;
        }

        // Normal sorgu ise tekli silme uygulanır
        if (! $record = $this->appmodel->image($this->uri->segment(3))) {
            show_404();
        }

        $success = $this->appmodel->imageDelete($record);

        if ($success) {
            $this->utils->setAlert('success', "Kayıt kaldırıldı. (#{$record->id})");
            redirect($this->input->server('HTTP_REFERER'));
        }

        $this->utils->setAlert('danger', 'Kayıt kaldırılamadı.');
        redirect($this->input->server('HTTP_REFERER'));

    }


    /**
     * Sıralama işlemi yapar
     */
    public function imageOrder()
    {
        $ids = explode(',', $this->input->post('ids'));

        if (count($ids) == 0){
            $this->utils->setAlert('danger', 'Lütfen kayıt seçiniz.');
        }

        $success = $this->appmodel->imageOrder($ids);

        if ($success){
            $this->utils->setAlert('success', "Kayıtlar başarıyla sıralandı.");
        }
    }



}