<?php

use Sirius\Admin\Manager;

class CityAdminController extends Manager
{
    public $moduleTitle = 'Şehirler';
    public $module = 'city';
    public $table = 'cities';
    public $model = 'city';

    // Arama yapılacak kolonlar.
    public $search = array('title');

    // Filtreleme yapılacak querystring/kolonlar.
    // public $filter = array('type');

    public $actions = array(
        'records' => 'list',
        'order' => 'list',
        'insert' => 'insert',
        'update' => 'update',
        'delete' => 'delete',
        'towns' => 'town-list',
        'townOrder' => 'town-list',
        'townInsert' => 'town-insert',
        'townUpdate' => 'town-update',
        'townDelete' => 'town-delete',
        'districts' => 'district-list',
        'districtOrder' => 'district-list',
        'districtInsert' => 'district-insert',
        'districtUpdate' => 'district-update',
        'districtDelete' => 'district-delete',
    );



    protected function insertValidateRules()
    {
        $this->form_validation->set_rules('title', 'Lütfen Başlık yazınız.', 'required');

        if ($this->input->post('autoSlug') !== 'true') {
            $this->form_validation->set_rules('slug', 'Lütfen slug yazınız.', 'required');
        }
    }




    protected function updateValidateRules()
    {
        $this->form_validation->set_rules('title', 'Lütfen Başlık yazınız.', 'required');

        if ($this->input->post('autoSlug') !== 'true') {
            $this->form_validation->set_rules('slug', 'Lütfen slug yazınız.', 'required');
        }
    }




    public function towns()
    {
        if (! $parent = $this->appmodel->id($this->uri->segment(3))) {
            show_404();
        }

        $records = array();
        $pagination = null;
        $recordCount = $this->appmodel->townCount($parent);

        if ($recordCount > 0) {
            $config = array(
                'base_url' => clink(array($this->module, 'towns', $parent->id)),
                'total_rows' => $recordCount,
                'per_page' => 19
            );

            $this->load->library('pagination');
            $this->pagination->initialize($config);


            $records = $this->appmodel->townAll($parent, $this->pagination->per_page +1, $this->pagination->offset);
            $pagination = $this->pagination->create_links();
        }

        $this->breadcrumb($parent->title, clink(array($this->module, 'towns', $parent->id)));
        $this->breadcrumb('İlçeler');

        $this->viewData['parent'] = $parent;
        $this->viewData['records'] = $records;
        $this->viewData['pagination'] = $pagination;

        $this->render('towns/records');
    }





    public function townInsert()
    {
        if (! $parent = $this->appmodel->id($this->uri->segment(3))) {
            show_404();
        }

        if ($this->input->post()) {
            $this->form_validation->set_rules('title', 'Lütfen Başlık yazınız.', 'required');

            if ($this->input->post('autoSlug') !== 'true') {
                $this->form_validation->set_rules('slug', 'Lütfen slug yazınız.', 'required');
            }

            if ($this->form_validation->run() === false) {
                $this->setAlert('danger', $this->form_validation->error_string('<div>&bull; ', '</div>'));
            }


            if (! $this->utils->isAlert()) {
                $success = $this->appmodel->townInsert($parent, $this->modelData);

                if ($success) {
                    $this->utils->setAlert('success', 'Kayıt eklendi.');
                    redirect(clink(array($this->module, 'towns', $parent->id)));
                }
            }
        }

        $this->breadcrumb($parent->title, clink(array($this->module, 'towns', $parent->id)));
        $this->breadcrumb('Yeni Kayıt');

        $this->viewData['parent'] = $parent;

        $this->render('towns/insert');
    }





    public function townUpdate()
    {
        if (! $record = $this->appmodel->town($this->uri->segment(3))) {
            show_404();
        }

        $parent = $this->appmodel->id($record->cityId);


        if ($this->input->post()) {
            $this->form_validation->set_rules('title', 'Lütfen Başlık yazınız.', 'required');

            if ($this->input->post('autoSlug') !== 'true') {
                $this->form_validation->set_rules('slug', 'Lütfen slug yazınız.', 'required');
            }

            if ($this->form_validation->run() === false) {
                $this->utils->setAlert('danger', $this->form_validation->error_string('<div>&bull; ', '</div>'));
            }


            if (! $this->utils->isAlert()) {
                $success = $this->appmodel->townUpdate($record, $this->modelData);

                if ($success) {
                    $this->utils->setAlert('success', 'Kayıt düzenlendi.');
                    redirect(clink(array($this->module, 'townUpdate', $record->id)));
                }
                $this->utils->setAlert('warning', 'Kayıt düzenlenmedi.');
            }
        }

        $this->breadcrumb($parent->title, clink(array($this->module, 'towns', $parent->id)));
        $this->breadcrumb('Kayıt Düzenle');

        $this->viewData['parent'] = $parent;
        $this->viewData['record'] = $record;

        $this->render('towns/update');
    }




    public function townDelete()
    {
        if ($this->input->is_ajax_request()) {
            $ids = $this->input->post('ids');

            if (count($ids) == 0) {
                $this->utils->setAlert('danger', 'Lütfen kayıt seçiniz.');
                echo $this->input->server('HTTP_REFERER');
            }

            $success = $this->appmodel->townDelete($ids);

            if ($success) {
                $this->utils->setAlert('success', "Kayıtlar başarıyla silindi.");
                echo $this->input->server('HTTP_REFERER');
            }

            return true;
        }

        if (! $record = $this->appmodel->town($this->uri->segment(3))) {
            show_404();
        }

        $success = $this->appmodel->townDelete($record);

        if ($success) {
            $this->utils->setAlert('success', "Kayıt kaldırıldı. (#{$record->id})");
            redirect($this->input->server('HTTP_REFERER'));
        }

        $this->utils->setAlert('danger', 'Kayıt kaldırılamadı.');
        redirect($this->input->server('HTTP_REFERER'));

    }




    public function townOrder()
    {
        $ids = explode(',', $this->input->post('ids'));

        if (count($ids) == 0){
            $this->utils->setAlert('danger', 'Lütfen kayıt seçiniz.');
        }

        $success = $this->appmodel->townOrder($ids);

        if ($success){
            $this->utils->setAlert('success', "Kayıtlar başarıyla sıralandı.");
        }
    }




    public function districts()
    {
        if (! $parent = $this->appmodel->town($this->uri->segment(3))) {
            show_404();
        }

        $city = $this->appmodel->id($parent->cityId);

        $records = array();
        $pagination = null;
        $recordCount = $this->appmodel->districtCount($parent);

        if ($recordCount > 0) {
            $config = array(
                'base_url' => clink(array($this->module, 'districts', $parent->id)),
                'total_rows' => $recordCount,
                'per_page' => 19
            );

            $this->load->library('pagination');
            $this->pagination->initialize($config);


            $records = $this->appmodel->districtAll($parent, $this->pagination->per_page +1, $this->pagination->offset);
            $pagination = $this->pagination->create_links();
        }

        $this->breadcrumb($city->title, clink(array($this->module, 'towns', $city->id)));
        $this->breadcrumb($parent->title, clink(array($this->module, 'districts', $parent->id)));
        $this->breadcrumb('Semtler');

        $this->viewData['parent'] = $parent;
        $this->viewData['records'] = $records;
        $this->viewData['pagination'] = $pagination;

        $this->render('districts/records');
    }





    public function districtInsert()
    {
        if (! $parent = $this->appmodel->town($this->uri->segment(3))) {
            show_404();
        }

        $city = $this->appmodel->id($parent->cityId);

        if ($this->input->post()) {
            $this->form_validation->set_rules('title', 'Lütfen Başlık yazınız.', 'required');

            if ($this->input->post('autoSlug') !== 'true') {
                $this->form_validation->set_rules('slug', 'Lütfen slug yazınız.', 'required');
            }

            if ($this->form_validation->run() === false) {
                $this->setAlert('danger', $this->form_validation->error_string('<div>&bull; ', '</div>'));
            }


            if (! $this->utils->isAlert()) {
                $success = $this->appmodel->districtInsert($parent, $this->modelData);

                if ($success) {
                    $this->utils->setAlert('success', 'Kayıt eklendi.');
                    redirect(clink(array($this->module, 'districts', $parent->id)));
                }
            }
        }

        $this->breadcrumb($city->title, clink(array($this->module, 'towns', $city->id)));
        $this->breadcrumb($parent->title, clink(array($this->module, 'districts', $parent->id)));
        $this->breadcrumb('Yeni Kayıt');

        $this->viewData['parent'] = $parent;

        $this->render('districts/insert');
    }





    public function districtUpdate()
    {
        if (! $record = $this->appmodel->district($this->uri->segment(3))) {
            show_404();
        }

        $parent = $this->appmodel->town($record->townId);
        $city = $this->appmodel->id($parent->cityId);


        if ($this->input->post()) {
            $this->form_validation->set_rules('title', 'Lütfen Başlık yazınız.', 'required');

            if ($this->input->post('autoSlug') !== 'true') {
                $this->form_validation->set_rules('slug', 'Lütfen slug yazınız.', 'required');
            }

            if ($this->form_validation->run() === false) {
                $this->utils->setAlert('danger', $this->form_validation->error_string('<div>&bull; ', '</div>'));
            }


            if (! $this->utils->isAlert()) {
                $success = $this->appmodel->districtUpdate($record, $this->modelData);

                if ($success) {
                    $this->utils->setAlert('success', 'Kayıt düzenlendi.');
                    redirect(clink(array($this->module, 'districtUpdate', $record->id)));
                }
                $this->utils->setAlert('warning', 'Kayıt düzenlenmedi.');
            }
        }

        $this->breadcrumb($city->title, clink(array($this->module, 'towns', $city->id)));
        $this->breadcrumb($parent->title, clink(array($this->module, 'districts', $parent->id)));
        $this->breadcrumb('Kayıt Düzenle');

        $this->viewData['parent'] = $parent;
        $this->viewData['record'] = $record;

        $this->render('districts/update');
    }




    public function districtDelete()
    {
        if ($this->input->is_ajax_request()) {
            $ids = $this->input->post('ids');

            if (count($ids) == 0) {
                $this->utils->setAlert('danger', 'Lütfen kayıt seçiniz.');
                echo $this->input->server('HTTP_REFERER');
            }

            $success = $this->appmodel->districtDelete($ids);

            if ($success) {
                $this->utils->setAlert('success', "Kayıtlar başarıyla silindi.");
                echo $this->input->server('HTTP_REFERER');
            }

            return true;
        }

        if (! $record = $this->appmodel->district($this->uri->segment(3))) {
            show_404();
        }

        $success = $this->appmodel->districtDelete($record);

        if ($success) {
            $this->utils->setAlert('success', "Kayıt kaldırıldı. (#{$record->id})");
            redirect($this->input->server('HTTP_REFERER'));
        }

        $this->utils->setAlert('danger', 'Kayıt kaldırılamadı.');
        redirect($this->input->server('HTTP_REFERER'));

    }




    public function districtOrder()
    {
        $ids = explode(',', $this->input->post('ids'));

        if (count($ids) == 0){
            $this->utils->setAlert('danger', 'Lütfen kayıt seçiniz.');
        }

        $success = $this->appmodel->districtOrder($ids);

        if ($success){
            $this->utils->setAlert('success', "Kayıtlar başarıyla sıralandı.");
        }
    }



} 