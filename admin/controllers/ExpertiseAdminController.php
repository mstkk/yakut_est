<?php

use Sirius\Admin\Manager;

class ExpertiseAdminController extends Manager
{
    public $moduleTitle = 'Ekspertiz Talepleri';
    public $module = 'expertise';
    public $table = 'expertise';
    public $model = 'expertise';
    public $type = 'public';
    public $icon = 'fa fa-line-chart';
    public $menuPattern = array(
        'moduleLink' => true
    );

    // Arama yapılacak kolonlar.
    public $search = array('t_fulname', 't_email');


    // Filtreleme yapılacak querystring/kolonlar.
     public $filter = array('viewed');

    public $actions = array(
        'records' => 'list',
        'view' => 'view',
        'delete' => 'delete',
    );


    public function view()
    {
        if (! $record = $this->appmodel->id($this->uri->segment(3))) {
            show_404();
        }

        $this->appmodel->viewed($record);

        $this->breadcrumb('Detaylar');

        $this->viewData['record'] = $record;

        $this->render('view');

    }





} 