<?php

use Sirius\Admin\Manager;

class ConsultantAdminController extends Manager
{
    public $moduleTitle = 'Profilim';
    public $module = 'consultant';
    public $table = 'profiles';
    public $model = 'consultant';
    public $type = 'public';
    public $icon = 'fa fa-users';
    public $menuPattern = array(
        'title' => 'name',
        'hint' => 'name',
        'link' => array('slug', 'id'),
        'language' => true,
        'moduleLink' => true,
    );

    public $actions = array(
        'records' => 'list',
        'order' => 'list',
        'insert' => 'insert',
        'update' => 'update',
        'delete' => 'delete',

    );

    protected function insertValidateRules()
    {
        $this->form_validation->set_rules('name', 'Lütfen İsim Soyisim yazınız.', 'required');

        if ($this->input->post('autoSlug') !== 'true') {
            $this->form_validation->set_rules('slug', 'Lütfen slug yazınız.', 'required');
        }
    }

    protected function insertAfterValidate()
    {
        $this->utils
            ->uploadInput('imageFile')
            ->minSizes(400, 400)
            ->addProcessSize('normal', 400, 400, 'consultant', 'thumbnail');


        if ($this->input->post('imageUrl')) {
            $this->modelData['image'] = $this->utils->imageDownload(true, $this->input->post('imageUrl'));
        } else {
            $this->modelData['image'] = $this->utils->imageUpload(true);
        }
    }


    protected function updateValidateRules()
    {
        $this->form_validation->set_rules('name', 'Lütfen İsim Soyisim yazınız.', 'required');

        if ($this->input->post('autoSlug') !== 'true') {
            $this->form_validation->set_rules('slug', 'Lütfen slug yazınız.', 'required');
        }
    }


    protected function updateAfterValidate($record)
    {
        $this->utils
            ->uploadInput('imageFile')
            ->minSizes(400, 400)
            ->addProcessSize('normal', 400, 400, 'consultant', 'thumbnail');


        if ($this->input->post('imageUrl')) {
            $this->modelData['image'] = $this->utils->imageDownload(false, $this->input->post('imageUrl'), $record->image);
        } else {
            $this->modelData['image'] = $this->utils->imageUpload(false, $record->image);
        }
    }

} 