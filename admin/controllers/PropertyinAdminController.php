<?php

use Sirius\Admin\Manager;

class PropertyinAdminController extends Manager
{
    public $moduleTitle = 'İç Özellikler';
    public $module = 'propertyin';
    public $table = 'properties2';
    public $model = 'propertyin';

    // Arama yapılacak kolonlar.
    public $search = array('title');


    // Filtreleme yapılacak querystring/kolonlar.
    // public $filter = array('type');

    public $actions = array(
        'records' => 'list',
        'order' => 'list',
        'insert' => 'insert',
        'update' => 'update',
        'delete' => 'delete',
        'approve' => 'list'

    );



    protected function insertValidateRules()
    {
        $this->form_validation->set_rules('title', 'Lütfen Başlık yazınız.', 'required');

        if ($this->input->post('autoSlug') !== 'true') {
            $this->form_validation->set_rules('slug', 'Lütfen slug yazınız.', 'required');
        }
    }




    protected function updateValidateRules()
    {
        $this->form_validation->set_rules('title', 'Lütfen Başlık yazınız.', 'required');

        if ($this->input->post('autoSlug') !== 'true') {
            $this->form_validation->set_rules('slug', 'Lütfen slug yazınız.', 'required');
        }
    }

    public function approve()
    {
        if (! $record = $this->appmodel->id($this->uri->segment(3))) {
            show_404();
        }

        $this->db->where('id',$record->id);
        if ( $record->approved == 1 ) {

            $this->db->update(
                $this->table, array(
                    'approved' => 0
                )
            );

        } else {

            $this->db->update(
                $this->table, array(
                    'approved' => 1
                )
            );

        }

        redirect($this->module.'/records');

    }
} 