<?php

use Sirius\Admin\Manager;

class KindAdminController extends Manager
{
    public $moduleTitle = 'Emlak Türleri';
    public $module = 'kind';
    public $table = 'kinds';
    public $model = 'kind';

    // Arama yapılacak kolonlar.
    public $search = array('title');


    // Filtreleme yapılacak querystring/kolonlar.
    // public $filter = array('type');

    public $actions = array(
        'records' => 'list',
        'order' => 'list',
        'insert' => 'insert',
        'update' => 'update',
        'delete' => 'delete',
        'types' => 'type-list',
        'typeOrder' => 'type-list',
        'typeInsert' => 'type-insert',
        'typeUpdate' => 'type-update',
        'typeDelete' => 'type-delete',
    );



    protected function insertValidateRules()
    {
        $this->form_validation->set_rules('title', 'Lütfen Başlık yazınız.', 'required');

        if ($this->input->post('autoSlug') !== 'true') {
            $this->form_validation->set_rules('slug', 'Lütfen slug yazınız.', 'required');
        }
    }




    protected function updateValidateRules()
    {
        $this->form_validation->set_rules('title', 'Lütfen Başlık yazınız.', 'required');

        if ($this->input->post('autoSlug') !== 'true') {
            $this->form_validation->set_rules('slug', 'Lütfen slug yazınız.', 'required');
        }
    }




    public function types()
    {
        if (! $parent = $this->appmodel->id($this->uri->segment(3))) {
            show_404();
        }

        $records = array();
        $pagination = null;
        $recordCount = $this->appmodel->typeCount($parent);

        if ($recordCount > 0) {
            $config = array(
                'base_url' => clink(array($this->module, 'types', $parent->id)),
                'total_rows' => $recordCount,
                'per_page' => 19
            );

            $this->load->library('pagination');
            $this->pagination->initialize($config);


            $records = $this->appmodel->typeAll($parent, $this->pagination->per_page +1, $this->pagination->offset);
            $pagination = $this->pagination->create_links();
        }

        $this->breadcrumb($parent->title, clink(array($this->module, 'types', $parent->id)));
        $this->breadcrumb('Tipler');

        $this->viewData['parent'] = $parent;
        $this->viewData['records'] = $records;
        $this->viewData['pagination'] = $pagination;

        $this->render('types/records');
    }





    public function typeInsert()
    {
        if (! $parent = $this->appmodel->id($this->uri->segment(3))) {
            show_404();
        }

        if ($this->input->post()) {
            $this->form_validation->set_rules('title', 'Lütfen Başlık yazınız.', 'required');

            if ($this->input->post('autoSlug') !== 'true') {
                $this->form_validation->set_rules('slug', 'Lütfen slug yazınız.', 'required');
            }

            if ($this->form_validation->run() === false) {
                $this->setAlert('danger', $this->form_validation->error_string('<div>&bull; ', '</div>'));
            }


            if (! $this->utils->isAlert()) {
                $success = $this->appmodel->typeInsert($parent, $this->modelData);

                if ($success) {
                    $this->utils->setAlert('success', 'Kayıt eklendi.');
                    redirect(clink(array($this->module, 'types', $parent->id)));
                }
            }
        }

        $this->breadcrumb($parent->title, clink(array($this->module, 'types', $parent->id)));
        $this->breadcrumb('Yeni Kayıt');

        $this->viewData['parent'] = $parent;

        $this->render('types/insert');
    }





    public function typeUpdate()
    {
        if (! $record = $this->appmodel->type($this->uri->segment(3))) {
            show_404();
        }

        $parent = $this->appmodel->id($record->cityId);


        if ($this->input->post()) {
            $this->form_validation->set_rules('title', 'Lütfen Başlık yazınız.', 'required');

            if ($this->input->post('autoSlug') !== 'true') {
                $this->form_validation->set_rules('slug', 'Lütfen slug yazınız.', 'required');
            }

            if ($this->form_validation->run() === false) {
                $this->utils->setAlert('danger', $this->form_validation->error_string('<div>&bull; ', '</div>'));
            }


            if (! $this->utils->isAlert()) {
                $success = $this->appmodel->typeUpdate($record, $this->modelData);

                if ($success) {
                    $this->utils->setAlert('success', 'Kayıt düzenlendi.');
                    redirect(clink(array($this->module, 'typeUpdate', $record->id)));
                }
                $this->utils->setAlert('warning', 'Kayıt düzenlenmedi.');
            }
        }

        $this->breadcrumb($parent->title, clink(array($this->module, 'types', $parent->id)));
        $this->breadcrumb('Kayıt Düzenle');

        $this->viewData['parent'] = $parent;
        $this->viewData['record'] = $record;

        $this->render('types/update');
    }




    public function typeDelete()
    {
        if ($this->input->is_ajax_request()) {
            $ids = $this->input->post('ids');

            if (count($ids) == 0) {
                $this->utils->setAlert('danger', 'Lütfen kayıt seçiniz.');
                echo $this->input->server('HTTP_REFERER');
            }

            $success = $this->appmodel->typeDelete($ids);

            if ($success) {
                $this->utils->setAlert('success', "Kayıtlar başarıyla silindi.");
                echo $this->input->server('HTTP_REFERER');
            }

            return true;
        }

        if (! $record = $this->appmodel->type($this->uri->segment(3))) {
            show_404();
        }

        $success = $this->appmodel->typeDelete($record);

        if ($success) {
            $this->utils->setAlert('success', "Kayıt kaldırıldı. (#{$record->id})");
            redirect($this->input->server('HTTP_REFERER'));
        }

        $this->utils->setAlert('danger', 'Kayıt kaldırılamadı.');
        redirect($this->input->server('HTTP_REFERER'));

    }




    public function typeOrder()
    {
        $ids = explode(',', $this->input->post('ids'));

        if (count($ids) == 0){
            $this->utils->setAlert('danger', 'Lütfen kayıt seçiniz.');
        }

        $success = $this->appmodel->typeOrder($ids);

        if ($success){
            $this->utils->setAlert('success', "Kayıtlar başarıyla sıralandı.");
        }
    }




} 