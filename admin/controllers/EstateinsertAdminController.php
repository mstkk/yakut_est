<?php

use Sirius\Admin\Manager;

class EstateinsertAdminController extends Manager
{
    public $moduleTitle = 'Eklenen İlanlar';
    public $module = 'estateinsert';
    public $table = 'estate_inserts';
    public $model = 'estateinsert';
    public $icon = 'fa fa-plus';

    // Arama yapılacak kolonlar.
    public $search = array('fullname');


    // Filtreleme yapılacak querystring/kolonlar.
     public $filter = array('moved');

    public $actions = array(
        'records' => 'list',
        'move' => 'move',
        'delete' => 'delete',
    );


    public function move()
    {
        if (! $record = $this->appmodel->id($this->uri->segment(3))) {
            show_404();
        }

        if ($this->input->post()) {
            $this->form_validation->set_rules('no', 'Lütfen Emlak Numarasını yazınız.', 'required');
            $this->form_validation->set_rules('city', 'Lütfen Şehir seçiniz.', 'required');
            $this->form_validation->set_rules('town', 'Lütfen İlçe seçiniz.', 'required');
            $this->form_validation->set_rules('district', 'Lütfen Semt seçiniz.', 'required');
            $this->form_validation->set_rules('status', 'Lütfen Emlak Durumu seçiniz.', 'required');
            $this->form_validation->set_rules('kind', 'Lütfen Emlak Türü seçiniz.', 'required');
            $this->form_validation->set_rules('type', 'Lütfen Tipi Türü seçiniz.', 'required');
            $this->form_validation->set_rules('price', 'Lütfen Fiyat yazınız.', 'required');
            $this->form_validation->set_rules('description', 'Lütfen Açıklama yazınız.', 'required');


            if ($this->form_validation->run() === false) {
                $this->utils->setAlert('danger', $this->form_validation->error_string('<div>&bull; ', '</div>'));
            }



            if ($this->appmodel->isEmptyNo($this->input->post('no')) === false) {
                $this->utils->setAlert('danger','Bu emlak numarası kullanımda.');
            }


            $this->utils
                ->uploadInput('imageFile')
                ->minSizes(600, 450)
                ->addProcessSize('normal', 600, 450, 'estate', 'thumbnail');




            if ($this->input->post('imageUrl')) {
                $this->modelData['image'] = $this->utils->imageDownload(true, $this->input->post('imageUrl'));
            } elseif (! empty($_FILES['imageFile']['name'])) {
                $this->modelData['image'] = $this->utils->imageUpload(true);
            } else {

                $this->modelData['image'] = $this->utils->imageDownload(true, '../public/upload/estate/insert/'.$record->image);
            }



            if (! $this->utils->isAlert()) {

                $success = $this->appmodel->move($record, $this->modelData);

                if ($success) {

                    $this->utils->setAlert('success', 'Kayıt Aktarıldı.');

                    if ($this->input->post('redirect')) {
                        $redirect = $this->input->post('redirect');
                    } else {
                        $redirect = clink(array($this->module, 'move', $record->id));
                    }

                    redirect($redirect);
                }
                $this->utils->setAlert('warning', 'Kayıt aktarılamadı.');
            }
        }


        $this->load->vars('public', array('js' => array(
            '../public/admin/plugin/ckeditor/ckeditor.js',
            '../public/admin/plugin/ckfinder/ckfinder.js',
            '../public/admin/js/module/estate.js'
        )));

        $this->breadcrumb('Kayıt Aktar');

        $this->viewData['record'] = $record;

        $this->render('move');
    }





} 