<?php

class Advertising extends CI_Model
{

    public function getKaks()
    {
        return $this->db
            ->from('kaks')
            //            ->order_by('order', 'asc')
            ->order_by('id', 'asc')
            ->get()
            ->result();
    }

    public function getGabari()
    {
        return $this->db
            ->from('gabari')
            //            ->order_by('order', 'asc')
            ->order_by('id', 'asc')
            ->get()
            ->result();
    }

    public function getImar()
    {
        return $this->db
            ->from('imar')
            //            ->order_by('order', 'asc')
            ->order_by('id', 'asc')
            ->get()
            ->result();
    }

    public function getTapu()
    {
        return $this->db
            ->from('tapu')
            //            ->order_by('order', 'asc')
            ->order_by('id', 'asc')
            ->get()
            ->result();
    }

    public function getCities()
    {
        return $this->db
            ->from('cities')
            ->order_by('order', 'asc')
            ->order_by('id', 'asc')
            ->get()
            ->result();
    }


    public function getTowns($cityId)
    {
        return $this->db
            ->from('towns')
            ->where('cityId', $cityId)
            ->order_by('title', 'asc')
            ->order_by('id', 'asc')
            ->get()
            ->result();
    }


    public function getDistricts($townId)
    {
        return $this->db
            ->from('districts')
            ->where('townId', $townId)
            ->order_by('title', 'asc')
            ->order_by('id', 'asc')
            ->get()
            ->result();
    }

    public function getKinds()
    {
        return $this->db
            ->from('kinds')
            ->order_by('order', 'asc')
            ->order_by('id', 'asc')
            ->get()
            ->result();
    }


    public function getTypes($kindId)
    {
        return $this->db
            ->from('types')
            ->where('kindId', $kindId)
            ->order_by('order', 'asc')
            ->order_by('id', 'asc')
            ->get()
            ->result();
    }

    public function getHeatings()
    {
        return $this->db
            ->from('heatings')
            ->order_by('order', 'asc')
            ->order_by('id', 'asc')
            ->get()
            ->result();
    }

    public function getRooms()
    {
        return $this->db
            ->from('rooms')
            ->order_by('order', 'asc')
            ->order_by('id', 'asc')
            ->get()
            ->result();
    }

    public function getFloors()
    {
        return $this->db
            ->from('floors')
            ->order_by('order', 'asc')
            ->order_by('id', 'asc')
            ->get()
            ->result();
    }

    public function getBuildingAges()
    {
        return $this->db
            ->from('building_ages')
            ->order_by('order', 'asc')
            ->order_by('id', 'asc')
            ->get()
            ->result();
    }

    public function getUsageStatuses()
    {
        return $this->db
            ->from('usage_statuses')
            ->order_by('order', 'asc')
            ->order_by('id', 'asc')
            ->get()
            ->result();
    }

    public function getProperties()
    {
        return $this->db
            ->from('properties')
            ->order_by('order', 'asc')
            ->order_by('id', 'asc')
            ->get()
            ->result();
    }
    public function getProperties2()
    {
        return $this->db
            ->from('properties2')
            ->order_by('order', 'asc')
            ->order_by('id', 'asc')
            ->get()
            ->result();
    }
    public function getProperties3()
    {
        return $this->db
            ->from('properties3')
            ->order_by('order', 'asc')
            ->order_by('id', 'asc')
            ->get()
            ->result();
    }

    public function propertiesValidation($id)
    {
        return $this->db
            ->from('advertise_properties')
            ->where('estateId',  $id)
            ->order_by('id', 'asc')
            ->get()
            ->result();
    }
    public function propertiesValidation2($id)
    {
        return $this->db
            ->from('advertise_properties2')
            ->where('estateId',  $id)
            ->order_by('id', 'asc')
            ->get()
            ->result();
    }

    public function propertiesValidation3($id)
    {
        return $this->db
            ->from('advertise_properties3')
            ->where('estateId',  $id)
            ->order_by('id', 'asc')
            ->get()
            ->result();
    }



    public function getPropertyRelations($estate)
    {
        $properties = $this->db
            ->from('properties')
            ->join('advertise_properties', 'advertise_properties.propertyId = properties.id')
            ->where('advertise_properties.estateId', $estate->id)
            ->get()
            ->result();

        $result = array();

        foreach ($properties as $property) {
            $result[] = $property->propertyId;
        }

        return $result;
    }

    public function getConsultant()
    {
        return $this->db
            ->from('profiles')
            ->order_by('order')
            ->get()
            ->result();
    }

    public function id($id)
    {
        return $this->db
            ->from($this->table)
            ->where('id', $id)
            ->where('language', $this->language)
            ->get()
            ->row();
    }


    public function all($limit = null, $offset = null)
    {
        $this->utils->filter();


        if ($limit != null) {
            $this->db->limit($limit, $offset);
        }

        $user = $this->session->userdata('adminuser');
        $cons = $this->session->userdata('consultant');

        if ($user->admin != 1) {
            $this->db->where('consultantId', $cons->id);
        }

        if ($sortBy = $this->input->get('sortBy')) {
            $this->db->order_by($sortBy, $this->input->get('sortType'));
        }

        return $this->db
            ->select("{$this->table}.*,
                types.title typeTitle, kinds.title kindTitle, districts.title districtTitle, towns.title townTitle, cities.title cityTitle,
                (SELECT COUNT(images.id) FROM advertising_images images WHERE images.advertisingId = {$this->table}.id)images,
                (SELECT COUNT(properties.estateId) FROM advertise_properties properties WHERE properties.estateId = {$this->table}.id) properties", false)
            ->from($this->table)
            ->join('cities', 'cities.id = advertisings.cityId', "left")
            ->join('towns', 'towns.id = advertisings.townId', "left")
            ->join('districts', 'districts.id = advertisings.localeId', "left")
            ->join('kinds', 'kinds.id = advertisings.estateKindId', "left")
            ->join('types', 'types.id = advertisings.estateTypeId', "left")
            ->order_by("isActive", 'desc')
            ->order_by("{$this->table}.date", 'desc')
            ->get()
            ->result();
    }


    public function count()
    {
        $this->utils->filter();

        return $this->db
            ->from($this->table)
            ->where('language', $this->language)
            ->count_all_results();
    }


    /**
     * @param array $data
     * @return mixed
     */
    public function insert($data = array())
    {
        $order = 1;
        $lastOrderRecord = $this->db
            ->from($this->table)
            ->where('language', $this->language)
            ->order_by('order', 'desc')
            ->limit(1)
            ->get()
            ->row();

        if ($lastOrderRecord) {
            $order = $lastOrderRecord->order + 1;
        }

        $user = $this->session->userdata('consultant');

        $this->db->insert($this->table, array(
            'title'           => $this->input->post('title'),
            'slug'            => $this->input->post('autoSlug') === 'true' ? makeSlug($this->input->post('title')) : makeSlug($this->input->post('slug')),
            'image'           => $data['image']['name'],
            'consultantId'    => $user->id,
            'cityId'          => $this->input->post('cityId'),
            'townId'          => $this->input->post('townId'),
            'localeId'        => $this->input->post('localeId'),
            'estateNo'        => $this->input->post('estateNo'),
            'estateKindId'    => $this->input->post('kind'),
            'estateTypeId'    => $this->input->post('type'),
            'video'           => $this->input->post('video'),
            'detail'          => $this->input->post('detail'),
            'price'           => $this->input->post('price'),
            'squareMeter'     => $this->input->post('squareMeter'),
            'roomId'          => $this->input->post('roomId'),
            'bathCount'       => $this->input->post('bathCount'),
            'floorId'         => $this->input->post('floor'),
            'floorCount'      => $this->input->post('floorCount'),
            'ageId'           => $this->input->post('buildingAge'),
            'heatId'          => $this->input->post('heating'),
            'statusId'        => $this->input->post('statusId'),
            'usageId'         => $this->input->post('usageStatus'),
            'map'             => $this->input->post('map'),
            'emergency'       => $this->input->post('emergency'),
            'metaTitle'       => $this->input->post('metaTitle'),
            'metaDescription' => $this->input->post('metaDescription'),
            'metaKeywords'    => $this->input->post('metaKeywords'),
            'date'            => $this->date->set()->mysqlDatetime(),
            'updateDate'      => $this->date->set()->mysqlDatetime(),
            'order'           => $order,
            'language'        => $this->language,
            'imar'            => $this->input->post('imar'),
            'ada'             => $this->input->post('ada'),
            'parsel'          => $this->input->post('parsel'),
            'pafta'           => $this->input->post('pafta'),
            'kaks'            => $this->input->post('kaks'),
            'gabari'          => $this->input->post('gabari'),
            'tapu'            => $this->input->post('tapu'),
            'enlem'            => $this->input->post('enlem'),
            'boylam'            => $this->input->post('boylam'),
            'cephee'            => $this->input->post('cephe'),
            'sitein'            => $this->input->post('sitein'),
            'durumuyapi'            => $this->input->post('durumuyapi'),
            'videoo'            => $this->input->post('videoo'),
            'kat'             => $this->input->post('kat'),
            'kredi'           => $this->input->post('kredi'),
            'takas'           => $this->input->post('takas'),
            'investor'        => $this->input->post('investor'),
            'new_build'       => $this->input->post('new_build'),
            'cepheGen'        => $this->input->post('cepheGen'),
            'tavanYuk'        => $this->input->post('tavanYuk'),
            'bolumSay'        => $this->input->post('bolumSay'),
            'kiraGetirisi'    => $this->input->post('kiraGetirisi'),
            'depozito'        => $this->input->post('depozito'),
            'depozitoMoneyType'    => $this->input->post('depozitoMoneyType'),
            'aidat'           => $this->input->post('aidat'),
            'aidatMoneyType'    => $this->input->post('aidatMoneyType'),
            'month'           => $this->input->post('month'),
            'showcase'        => $this->input->post('showcase'),
            'moneyType'       => $this->input->post('moneyType'),
            'adresss'       => $this->input->post('adresss'),
        ));

        $id = $this->db->insert_id();

        $properties = $this->input->post('properties');
        $properties2 = $this->input->post('properties2');
        $properties3 = $this->input->post('properties3');

        if ($properties) {
                foreach ($properties as $property) {
                    $this->db->insert('advertise_properties', [
                        'estateId'   => $id,
                        'propertyId' => $property
                    ]);
            }
        }
        if ($properties2) {
                foreach ($properties2 as $property) {
                    $this->db->insert('advertise_properties2', [
                        'estateId'   => $id,
                        'propertyId' => $property
                    ]);
            }
        }
        if ($properties3) {
                foreach ($properties3 as $property) {
                    $this->db->insert('advertise_properties3', [
                        'estateId'   => $id,
                        'propertyId' => $property
                    ]);
            }
        }

        return $id;
    }

    public function insertestate($dataimg)
    {

        $img  = $this->db
            ->from('estate_inserts')
            ->where('id', $this->uri->segment(3))
            ->limit(1)
            ->get()
            ->row();

        $order = 1;
        $lastOrderRecord = $this->db
            ->from($this->table)
            ->where('language', $this->language)
            ->order_by('order', 'desc')
            ->limit(1)
            ->get()
            ->row();

        if ($lastOrderRecord) {
            $order = $lastOrderRecord->order + 1;
        }

        $user = $this->session->userdata('consultant');

        $this->db->insert($this->table, array(
            'title'           => $this->input->post('title'),
            'slug'            => $this->input->post('autoSlug') === 'true' ? makeSlug($this->input->post('title')) : makeSlug($this->input->post('slug')),
            'image'           => $dataimg,
            'cityId'          => $this->input->post('cityId'),
            'townId'          => $this->input->post('townId'),
            'localeId'        => $this->input->post('localeId'),
            'estateNo'        => $this->input->post('estateNo'),//brb
            'estateKindId'    => $this->input->post('kind'),
            'estateTypeId'    => $this->input->post('type'),
            'video'           => $this->input->post('video'),
            'detail'          => $this->input->post('detail'),
            'price'           => $this->input->post('price'),
            'squareMeter'     => $this->input->post('squareMeter'),
            'roomId'          => $this->input->post('roomId'),
            'bathCount'       => $this->input->post('bathCount'),
            'floorId'         => $this->input->post('floor'),
            'floorCount'      => $this->input->post('floorCount'),
            'ageId'           => $this->input->post('buildingAge'),
            'heatId'          => $this->input->post('heating'),
            'statusId'        => $this->input->post('statusId'),
            'usageId'         => $this->input->post('usageStatus'),
            'map'             => $this->input->post('map'),
            'emergency'       => $this->input->post('emergency'),
            'metaTitle'       => $this->input->post('metaTitle'),
            'metaDescription' => $this->input->post('metaDescription'),
            'metaKeywords'    => $this->input->post('metaKeywords'),
            'date'            => $this->date->set()->mysqlDatetime(),
            'order'           => $order,
            'language'        => $this->language,
            'imar'            => $this->input->post('imar'),
            'ada'             => $this->input->post('ada'),
            'parsel'          => $this->input->post('parsel'),
            'pafta'           => $this->input->post('pafta'),
            'kaks'            => $this->input->post('kaks'),
            'gabari'          => $this->input->post('gabari'),
            'tapu'            => $this->input->post('tapu'),
            'enlem'            => $this->input->post('enlem'),
            'boylam'            => $this->input->post('boylam'),
            'cephee'            => $this->input->post('cephe'),
            'sitein'            => $this->input->post('sitein'),
            'durumuyapi'            => $this->input->post('durumuyapi'),
            'videoo'            => $this->input->post('videoo'),
            'kat'             => $this->input->post('kat'),
            'kredi'           => $this->input->post('kredi'),
            'takas'           => $this->input->post('takas'),
            'investor'        => $this->input->post('investor'),
            'new_build'       => $this->input->post('new_build'),
            'cepheGen'        => $this->input->post('cepheGen'),
            'tavanYuk'        => $this->input->post('tavanYuk'),
            'bolumSay'        => $this->input->post('bolumSay'),
            'kiraGetirisi'    => $this->input->post('kiraGetirisi'),
            'kiraGetirisiMoneyType'    => $this->input->post('kiraGetirisiMoneyType'),
            'depozito'        => $this->input->post('depozito'),
            'depozitoMoneyType'    => $this->input->post('depozitoMoneyType'),
            'aidat'           => $this->input->post('aidat'),
            'aidatMoneyType'    => $this->input->post('aidatMoneyType'),
            'month'           => $this->input->post('month'),
            'showcase'        => $this->input->post('showcase'),
            'moneyType'       => $this->input->post('moneyType'),
            'adresss'       => $this->input->post('adresss'),

        ));

        $id = $this->db->insert_id();

        $properties = $this->input->post('properties');

        if ($properties) {
            foreach ($properties as &$property) {
                $property = array(
                    'estateId'   => $id,
                    'propertyId' => $property
                );
            }

            $this->db->insert_batch('advertise_properties', $properties);
        }

        $this->db->update('estate_inserts', array('moved'=> 1), array('id' => $this->uri->segment(3)));

        // Ftp Connection..



        return $id;

    }


    public function update($record, $data = array())
    {


        $this->db
            ->where('id', $record->id)
            ->update($this->table, array(
                'estateNo'        => $this->input->post('estateNo'),
                'title'           => $this->input->post('title'),
                //                'consultantId' => ! empty($this->input->post('consultantId')) ? $this->input->post('consultantId') : $record->consultantId,
                'slug'            => $this->input->post('autoSlug') === 'true' ? makeSlug($this->input->post('title')) : makeSlug($this->input->post('slug')),
                'image'           => $data['image']['name'],
                'cityId'          => $this->input->post('cityId'),
                'townId'          => $this->input->post('townId'),
                'localeId'        => $this->input->post('localeId'),
                'estateKindId'    => $this->input->post('kind'),
                'estateTypeId'    => $this->input->post('type'),
                'video'           => $this->input->post('video'),
                'detail'          => $this->input->post('detail'),
                'price'           => $this->input->post('price'),
                'squareMeter'     => $this->input->post('squareMeter'),
                'roomId'          => $this->input->post('roomId'),
                'bathCount'       => $this->input->post('bathCount'),
                'floorId'         => $this->input->post('floor'),
                'floorCount'      => $this->input->post('floorCount'),
                'ageId'           => $this->input->post('buildingAge'),
                'heatId'          => $this->input->post('heating'),
                'usageId'         => $this->input->post('usageStatus'),
                'statusId'        => $this->input->post('statusId'),
                'map'             => $this->input->post('map'),
                'updateDate'      => $this->date->set()->mysqlDatetime(),
                'emergency'       => $this->input->post('emergency'),
                'metaTitle'       => $this->input->post('metaTitle'),
                'metaDescription' => $this->input->post('metaDescription'),
                'metaKeywords'    => $this->input->post('metaKeywords'),
                'imar'            => $this->input->post('imar'),
                'ada'             => $this->input->post('ada'),
                'parsel'          => $this->input->post('parsel'),
                'pafta'           => $this->input->post('pafta'),
                'kaks'            => $this->input->post('kaks'),
                'gabari'          => $this->input->post('gabari'),
                'tapu'            => $this->input->post('tapu'),
                'enlem'            => $this->input->post('enlem'),
                'boylam'            => $this->input->post('boylam'),
                'cephee'            => $this->input->post('cephe'),
                'sitein'            => $this->input->post('sitein'),
                'durumuyapi'            => $this->input->post('durumuyapi'),
                'videoo'            => $this->input->post('videoo'),
                'kat'             => $this->input->post('kat'),
                'kredi'           => $this->input->post('kredi'),
                'takas'           => $this->input->post('takas'),
                'investor'        => $this->input->post('investor'),
                'new_build'       => $this->input->post('new_build'),
                'cepheGen'        => $this->input->post('cepheGen'),
                'tavanYuk'        => $this->input->post('tavanYuk'),
                'bolumSay'        => $this->input->post('bolumSay'),
                'kiraGetirisi'    => $this->input->post('kiraGetirisi'),
                'kiraGetirisiMoneyType'    => $this->input->post('kiraGetirisiMoneyType'),
                'depozito'        => $this->input->post('depozito'),
                'depozitoMoneyType'    => $this->input->post('depozitoMoneyType'),
                'aidat'           => $this->input->post('aidat'),
                'aidatMoneyType'    => $this->input->post('aidatMoneyType'),
                'month'           => $this->input->post('month'),
                'showcase'        => $this->input->post('showcase'),
                'moneyType'       => $this->input->post('moneyType'),
                'adresss'       => $this->input->post('adresss'),

            ));

        $rows = $this->db->affected_rows();

        $this->db->where('estateId', $record->id)->delete('advertise_properties');
        $this->db->where('estateId', $record->id)->delete('advertise_properties2');
        $this->db->where('estateId', $record->id)->delete('advertise_properties3');
        $properties = $this->input->post('properties');
        $properties2 = $this->input->post('properties2');
        $properties3 = $this->input->post('properties3');
        if ($properties) {
            foreach ($properties as &$property) {
                $property = array(
                    'estateId'   => $record->id,
                    'propertyId' => $property
                );
            }

            $this->db->insert_batch('advertise_properties', $properties);
        }
        if ($properties2) {
            foreach ($properties2 as &$property) {
                $property = array(
                    'estateId'   => $record->id,
                    'propertyId' => $property
                );
            }

            $this->db->insert_batch('advertise_properties2', $properties2);
        }
        if ($properties3) {
            foreach ($properties3 as &$property) {
                $property = array(
                    'estateId'   => $record->id,
                    'propertyId' => $property
                );
            }

            $this->db->insert_batch('advertise_properties3', $properties3);
        }

        return $rows;
    }


    public function delete($data)
    {
        if (is_array($data)) {
            $records = $this->db
                ->from($this->table)
                ->where_in('id', $data)
                ->get()
                ->result();

            $success = $this->db
                ->where_in('id', $data)
                ->delete($this->table);

            if ($success) {
                foreach ($records as $record) {
                    @unlink("../public/upload/ilanlar/{$record->image}");
                    @unlink("../public/upload/ilanlar/thumb/{$record->image}");
                }

                $images = $this->db
                    ->from('advertising_images')
                    ->where_in('advertisingId', $data)
                    ->get()
                    ->result();

                $delete = $this->db
                    ->where_in('advertisingId', $data)
                    ->delete('advertising_images');

                if ($delete) {
                    foreach ($images as $image) {
                        @unlink("../public/upload/ilanlar/thumb/{$image->image}");
                        @unlink("../public/upload/ilanlar/normal/{$image->image}");
                    }
                }
            }

            return $success;
        }

        $success = $this->db
            ->where('id', $data->id)
            ->delete($this->table);

        if ($success) {
            @unlink("../public/upload/ilanlar/{$data->image}");
            @unlink("../public/upload/ilanlar/thumb/{$data->image}");

            $images = $this->db
                ->from('advertising_images')
                ->where('advertisingId', $data->id)
                ->get()
                ->result();

            $delete = $this->db
                ->where('advertisingId', $data->id)
                ->delete('advertising_images');

            if ($delete) {
                foreach ($images as $image) {
                    @unlink("../public/upload/ilanlar/normal/{$image->image}");
                    @unlink("../public/upload/ilanlar/thumb/{$image->image}");
                }
            }
        }

        return $success;
    }

    public function alldateupdate()
    {
        $this->db
            ->update($this->table, array(
                'updateDate'      => $this->date->set()->mysqlDatetime(),
            ));
    }


    public function order($ids = null)
    {
        if (is_array($ids)) {
            $records = $this->db
                ->from($this->table)
                ->where_in('id', $ids)
                //                ->where('language', $this->language)
                ->order_by('order', 'asc')
                ->order_by('id', 'desc')
                ->get()
                ->result();

            $firstOrder = 0;
            $affected = 0;

            foreach ($records as $record) {
                if ($firstOrder === 0) {
                    $firstOrder = $record->order;
                }

                $order = array_search($record->id, $ids) + $firstOrder;

                if ($record->order != $order) {
                    $this->db
                        ->where('id', $record->id)
                        ->update($this->table, array('order' => $order));

                    if ($this->db->affected_rows() > 0) {
                        $affected++;
                    }
                }

            }

            return $affected;
        }
    }


    public function image($id)
    {
        return $this->db
            ->from('advertising_images')
            ->where('id', $id)
            ->where('language', $this->language)
            ->get()
            ->row();
    }


    public function imageAll($gallery, $limit = null, $offset = null)
    {
        $this->utils->filter();


        if ($limit != null) {
            $this->db->limit($limit, $offset);
        }

        return $this->db
            ->from('advertising_images')
            ->where('advertisingId', $gallery->id)
            ->where('language', $this->language)
            ->order_by('order', 'asc')
            ->order_by('id', 'asc')
            ->get()
            ->result();
    }


    public function imageCount($gallery)
    {
        $this->utils->filter();

        return $this->db
            ->from('advertising_images')
            ->where('advertisingId', $gallery->id)
            ->where('language', $this->language)
            ->count_all_results();
    }


    public function imageInsert($gallery, $data = array())
    {
        $order = 1;
        $lastOrderRecord = $this->db
            ->from('advertising_images')
            ->where('advertisingId', $gallery->id)
            ->where('language', $this->language)
            ->order_by('order', 'desc')
            ->limit(1)
            ->get()
            ->row();


        if ($lastOrderRecord) {
            $order = $lastOrderRecord->order + 1;
        }


        $this->db->insert('advertising_images', array(
            'advertisingId' => $gallery->id,
            'image'         => $data['image']['name'],
            'order'         => $order,
            'language'      => $this->language,
        ));

        return $this->db->insert_id();
    }


    public function imageUpdate($record, $data = array())
    {
        $this->db
            ->where('id', $record->id)
            ->update('advertising_images', array(
                'image' => $data['image']['name'],
            ));

        return $this->db->affected_rows();
    }


    public function imageDelete($data)
    {
        if (is_array($data)) {
            $records = $this->db
                ->from('advertising_images')
                ->where_in('id', $data)
                ->get()
                ->result();

            $success = $this->db
                ->where_in('id', $data)
                ->delete('advertising_images');

            if ($success) {
                foreach ($records as $record) {
                    @unlink("../public/upload/advertising/thumb/{$record->image}");
                    @unlink("../public/upload/advertising/normal/{$record->image}");
                }
            }

            return $success;
        }

        $success = $this->db
            ->where('id', $data->id)
            ->delete('advertising_images');

        @unlink("../public/upload/advertising/thumb/{$data->image}");
        @unlink("../public/upload/advertising/normal/{$data->image}");


        return $success;
    }


    public function imageOrder($ids = null)
    {
        if (is_array($ids)) {
            $records = $this->db
                ->from('advertising_images')
                ->where_in('id', $ids)
                ->where('language', $this->language)
                ->order_by('order', 'asc')
                ->order_by('id', 'desc')
                ->get()
                ->result();

            $firstOrder = 0;
            $affected = 0;

            foreach ($records as $record) {
                if ($firstOrder === 0) {
                    $firstOrder = $record->order;
                }

                $order = array_search($record->id, $ids) + $firstOrder;

                if ($record->order != $order) {
                    $this->db
                        ->where('id', $record->id)
                        ->update('advertising_images', array('order' => $order));

                    if ($this->db->affected_rows() > 0) {
                        $affected++;
                    }
                }

            }

            return $affected;
        }

    }

} 