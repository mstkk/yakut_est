<?php

class Estateinsert extends CI_Model
{

    public function isEmptyNo($no, $record = null)
    {
        if ($record) {
            $this->db->where('id !=', $record->id);
        }

        $count = $this->db
            ->from('estates')
            ->where('no', $no)
            ->count_all_results();

        if ($count == 0) {
            return true;
        }

        return false;
    }



    public function getCities()
    {
        return $this->db
            ->from('cities')
            ->order_by('order', 'asc')
            ->order_by('id', 'asc')
            ->get()
            ->result();
    }


    public function getTowns($cityId)
    {
        return $this->db
            ->from('towns')
            ->where('cityId', $cityId)
            ->order_by('order', 'asc')
            ->order_by('id', 'asc')
            ->get()
            ->result();
    }


    public function getDistricts($townId)
    {
        return $this->db
            ->from('districts')
            ->where('townId', $townId)
            ->order_by('order', 'asc')
            ->order_by('id', 'asc')
            ->get()
            ->result();
    }

    public function getKinds()
    {
        return $this->db
            ->from('kinds')
            ->order_by('order', 'asc')
            ->order_by('id', 'asc')
            ->get()
            ->result();
    }


    public function getTypes($kindId)
    {
        return $this->db
            ->from('types')
            ->where('kindId', $kindId)
            ->order_by('order', 'asc')
            ->order_by('id', 'asc')
            ->get()
            ->result();
    }

    public function getHeatings()
    {
        return $this->db
            ->from('heatings')
            ->order_by('order', 'asc')
            ->order_by('id', 'asc')
            ->get()
            ->result();
    }

    public function getProperties()
    {
        return $this->db
            ->from('properties')
            ->order_by('order', 'asc')
            ->order_by('id', 'asc')
            ->get()
            ->result();
    }
    public function id($id)
    {
        return $this->db
            ->select("{$this->table}.*, rooms.title room, floors.title storey, building_ages.title buildingAge,
            usage_statuses.title usageStatus", false)
            ->from($this->table)
            ->join("rooms", "rooms.id = {$this->table}.roomId", "left")
            ->join("floors", "floors.id = {$this->table}.floorId", "left")
            ->join("usage_statuses", "usage_statuses.id = {$this->table}.usageStatusId", "left")
            ->join("building_ages", "building_ages.id = {$this->table}.buildingAgeId", "left")
            ->where("{$this->table}.id", $id)
            ->get()
            ->row();
    }
    public function find($id)
    {
        return $this->db
            ->select("estate_inserts.*, rooms.title room, floors.title storey, building_ages.title buildingAge,
            usage_statuses.title usageStatus", false)
            ->from('estate_inserts')
            ->join("rooms", "rooms.id = estate_inserts.roomId", "left")
            ->join("floors", "floors.id = estate_inserts.floorId", "left")
            ->join("usage_statuses", "usage_statuses.id = estate_inserts.usageStatusId", "left")
            ->join("building_ages", "building_ages.id = estate_inserts.buildingAgeId", "left")
            ->where("estate_inserts.id", $id)
            ->get()
            ->row();
    }


    public function all($limit = null, $offset = null)
    {
        $this->utils->filter();


        if ($limit != null) {
            $this->db->limit($limit, $offset);
        }

        return $this->db
            ->select("{$this->table}.*,
                types.title typeTitle, kinds.title kindTitle, districts.title districtTitle, towns.title townTitle, cities.title cityTitle", false)
            ->from($this->table)
            ->join('cities', 'cities.id = estate_inserts.cityId')
            ->join('towns', 'towns.id = estate_inserts.townId')
            ->join('districts', 'districts.id = estate_inserts.districtId')
            ->join('kinds', 'kinds.id = estate_inserts.kindId')
            ->join('types', 'types.id = estate_inserts.typeId')
            ->order_by("{$this->table}.id", 'desc')
            ->get()
            ->result();
    }


    public function count()
    {
        $this->utils->filter();

        return $this->db
            ->from($this->table)
            ->count_all_results();
    }



    public function move($record, $data = array())
    {
        $this->db->insert('estates', array(
            'no' => $this->input->post('no'),
            'cityId' => $this->input->post('city'),
            'townId' => $this->input->post('town'),
            'districtId' => $this->input->post('district'),
            'estateStatus' => $this->input->post('status'),
            'kindId' => $this->input->post('kind'),
            'typeId' => $this->input->post('type'),
            'price' => $this->input->post('price'),
            'image' => $data['image']['name'],
            'description' => $this->input->post('description'),
            'room' => $this->input->post('room'),
            'saloon' => $this->input->post('saloon'),
            'bath' => $this->input->post('bath'),
            'squaremeter' => $this->input->post('squaremeter'),
            'storey' => $this->input->post('storey'),
            'storeyCount' => $this->input->post('storeyCount'),
            'buildingAge' => $this->input->post('buildingAge'),
            'heatingId' => $this->input->post('heating'),
            'usageStatus' => $this->input->post('usageStatus'),
            'mapCoordinate' => $this->input->post('mapCoordinate'),
            'date' => $this->date->set()->mysqlDatetime()
        ));

        $insertId = $this->db->insert_id();


        $this->db
            ->where('estateId', $insertId)
            ->delete('estate_properties');

        if ($properties = $this->input->post('properties')) {
            foreach ($properties as &$property) {
                $property = array(
                    'estateId' => $insertId,
                    'propertyId' => $property
                );
            }

            $this->db->insert_batch('estate_properties', $properties);
        }

        $this->db
            ->where('id', $record->id)
            ->update($this->table, array(
                'moved' => 1
            ));

        return $insertId;
    }



    public function delete($data)
    {
        if (is_array($data)) {
            $records = $this->db
                ->from($this->table)
                ->where_in('id', $data)
                ->get()
                ->result();

            $success = $this->db
                ->where_in('id', $data)
                ->delete($this->table);

            if ($success) {
                foreach ($records as $record) {
                    @unlink("../public/upload/ilanlar/insert/{$record->image}");
                }
            }

            return $success;
        }

        $success = $this->db
            ->where('id', $data->id)
            ->delete($this->table);

        @unlink("../public/upload/ilanlar/insert/{$data->image}");

        return $success;
    }

} 