<?php

class Consultant extends CI_Model
{

    public function id($id)
    {
        return $this->db
            ->from($this->table)
            ->where('id', $id)
            ->where('language', $this->language)
            ->get()
            ->row();
    }


    public function all($limit = null, $offset = null)
    {
        $this->utils->filter();

        $user = $this->session->userdata('adminuser');

        if ($limit != null) {
            $this->db->limit($limit, $offset);
        }

        if ($user->admin == 1) {
            return $this->db
                ->from($this->table)
                ->where('language', $this->language)
                ->order_by('order', 'asc')
                ->order_by('id', 'asc')
                ->get()
                ->result();
        } else {
            return $this->db
                ->from($this->table)
                ->where('userId', $user->id)
                ->get()
                ->row();
        }
    }


    public function count()
    {
        $this->utils->filter();

        return $this->db
            ->from($this->table)
            ->where('language', $this->language)
            ->count_all_results();
    }





    public function insert($data = array())
    {
        $order = 1;
        $lastOrderRecord = $this->db
            ->from($this->table)
            ->where('language', $this->language)
            ->order_by('order', 'desc')
            ->limit(1)
            ->get()
            ->row();

        if ($lastOrderRecord) {
            $order = $lastOrderRecord->order + 1;
        }

        $user = $this->session->userdata('adminuser');

        $this->db->insert($this->table, array(
            'userId' => $user->id,
            'name' => $this->input->post('name'),
            'slug' => $this->input->post('autoSlug') === 'true' ? makeSlug($this->input->post('name')) : makeSlug($this->input->post('slug')),
            'image' => $data['image']['name'],
            'phone' => $this->input->post('phone'),
            'mobile' => $this->input->post('mobile'),
            'email' => $this->input->post('email'),
            'address' => $this->input->post('address'),
            'facebook' => $this->input->post('facebook'),
            'twitter' => $this->input->post('twitter'),
            'google' => $this->input->post('google'),
            'degree' => $this->input->post('degree'),
            'order' => $order,
            'language' =>$this->language
        ));

        return $this->db->insert_id();
    }



    public function update($record, $data = array())
    {
        $this->db
            ->where('id', $record->id)
            ->update($this->table, array(
                'name' => $this->input->post('name'),
                'slug' => $this->input->post('autoSlug') === 'true' ? makeSlug($this->input->post('name')) : makeSlug
                ($this->input->post('slug')),
                'image' => $data['image']['name'],
                'phone' => $this->input->post('phone'),
                'mobile' => $this->input->post('mobile'),
                'email' => $this->input->post('email'),
                'address' => $this->input->post('address'),
                'facebook' => $this->input->post('facebook'),
                'twitter' => $this->input->post('twitter'),
                'google' => $this->input->post('google'),
                'degree' => $this->input->post('degree'),
            ));

        return $this->db->affected_rows();
    }






    public function delete($data)
    {
        if (is_array($data)) {
            $records = $this->db
                ->from($this->table)
                ->where_in('id', $data)
                ->get()
                ->result();

            $success = $this->db
                ->where_in('id', $data)
                ->delete($this->table);

            if ($success) {
                foreach ($records as $record) {
                    @unlink("../public/upload/consultant/{$record->image}");
                }
            }

            return $success;
        }

        $success = $this->db
            ->where('id', $data->id)
            ->delete($this->table);

        @unlink("../public/upload/consultant/{$data->image}");


        return $success;
    }




    public function order($ids = null)
    {
        if (is_array($ids)) {
            $records = $this->db
                ->from($this->table)
                ->where_in('id', $ids)
                ->where('language', $this->language)
                ->order_by('order', 'asc')
                ->order_by('id', 'desc')
                ->get()
                ->result();

            $firstOrder = 0;
            $affected = 0;

            foreach ($records as $record) {
                if ($firstOrder === 0) {
                    $firstOrder = $record->order;
                }

                $order = array_search($record->id, $ids) + $firstOrder;

                if ($record->order != $order) {
                    $this->db
                        ->where('id', $record->id)
                        ->update($this->table, array('order' => $order));

                    if ($this->db->affected_rows() > 0) {
                        $affected++;
                    }
                }

            }

            return $affected;
        }
    }


} 