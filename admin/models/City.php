<?php

class City extends CI_Model
{

    public function id($id)
    {
        return $this->db
            ->from($this->table)
            ->where('id', $id)
            ->get()
            ->row();
    }


    public function all($limit = null, $offset = null)
    {
        $this->utils->filter();


        if ($limit != null) {
            $this->db->limit($limit, $offset);
        }

        return $this->db
            ->select("{$this->table}.*, (SELECT COUNT(id) FROM towns WHERE towns.cityId = {$this->table}.id) towns", false)
            ->from($this->table)
            ->order_by('order', 'asc')
            ->order_by('id', 'asc')
            ->get()
            ->result();
    }


    public function count()
    {
        $this->utils->filter();

        return $this->db
            ->from($this->table)
            ->count_all_results();
    }





    public function insert($data = array())
    {
        $order = 1;
        $lastOrderRecord = $this->db->from($this->table)->order_by('order', 'desc')->limit(1)->get()->row();

        if ($lastOrderRecord) {
            $order = $lastOrderRecord->order + 1;
        }

        $this->db->insert($this->table, array(
            'title' => $this->input->post('title'),
            'slug' => $this->input->post('autoSlug') === 'true' ? makeSlug($this->input->post('title')) : makeSlug($this->input->post('slug')),
            'order' => $order,
        ));

        return $this->db->insert_id();
    }



    public function update($record, $data = array())
    {
        $this->db
            ->where('id', $record->id)
            ->update($this->table, array(
                'title' => $this->input->post('title'),
                'slug' => $this->input->post('autoSlug') === 'true' ? makeSlug($this->input->post('title')) : makeSlug($this->input->post('slug')),
            ));


        return $this->db->affected_rows();
    }




    public function delete($data)
    {
        if (is_array($data)) {
            $success = $this->db
                ->where_in('id', $data)
                ->delete($this->table);

            return $success;
        }

        $success = $this->db
            ->where('id', $data->id)
            ->delete($this->table);

        return $success;
    }



    public function order($ids = null)
    {
        if (is_array($ids)) {
            $records = $this->db
                ->from($this->table)
                ->where_in('id', $ids)
                ->order_by('order', 'asc')
                ->order_by('id', 'desc')
                ->get()
                ->result();

            $firstOrder = 0;
            $affected = 0;

            foreach ($records as $record) {
                if ($firstOrder === 0) {
                    $firstOrder = $record->order;
                }

                $order = array_search($record->id, $ids) + $firstOrder;

                if ($record->order != $order) {
                    $this->db
                        ->where('id', $record->id)
                        ->update($this->table, array('order' => $order));

                    if ($this->db->affected_rows() > 0) {
                        $affected++;
                    }
                }

            }

            return $affected;
        }

    }






    public function town($id)
    {
        return $this->db
            ->from('towns')
            ->where('id', $id)
            ->get()
            ->row();
    }


    public function townAll($parent, $limit = null, $offset = null)
    {
        $this->utils->filter();


        if ($limit != null) {
            $this->db->limit($limit, $offset);
        }

        return $this->db
            ->select("towns.*, (SELECT COUNT(id) FROM districts WHERE districts.townId = towns.id) districts", false)
            ->from('towns')
            ->where('cityId', $parent->id)
            ->order_by('order', 'asc')
            ->order_by('id', 'asc')
            ->get()
            ->result();
    }


    public function townCount($parent)
    {
        $this->utils->filter();

        return $this->db
            ->from('towns')
            ->where('cityId', $parent->id)
            ->count_all_results();
    }


    public function townInsert($parent, $data = array())
    {
        $order = 1;
        $lastOrderRecord = $this->db
            ->from('towns')
            ->where('cityId', $parent->id)
            ->order_by('order', 'desc')
            ->limit(1)
            ->get()
            ->row();


        if ($lastOrderRecord) {
            $order = $lastOrderRecord->order + 1;
        }


        $this->db->insert('towns', array(
            'cityId' => $parent->id,
            'title' => $this->input->post('title'),
            'slug' => $this->input->post('autoSlug') === 'true' ? makeSlug($this->input->post('title')) : makeSlug($this->input->post('slug')),
            'order' => $order,
        ));

        return $this->db->insert_id();
    }



    public function townUpdate($record, $data = array())
    {
        $this->db
            ->where('id', $record->id)
            ->update('towns', array(
                'title' => $this->input->post('title'),
                'slug' => $this->input->post('autoSlug') === 'true' ? makeSlug($this->input->post('title')) : makeSlug($this->input->post('slug'))
            ));

        return $this->db->affected_rows();
    }



    public function townDelete($data)
    {
        if (is_array($data)) {
            $success = $this->db
                ->where_in('id', $data)
                ->delete('towns');

            return $success;
        }

        $success = $this->db
            ->where('id', $data->id)
            ->delete('towns');

        return $success;
    }


    public function townOrder($ids = null)
    {
        if (is_array($ids)) {
            $records = $this->db
                ->from('towns')
                ->where_in('id', $ids)
                ->order_by('order', 'asc')
                ->order_by('id', 'desc')
                ->get()
                ->result();

            $firstOrder = 0;
            $affected = 0;

            foreach ($records as $record) {
                if ($firstOrder === 0) {
                    $firstOrder = $record->order;
                }

                $order = array_search($record->id, $ids) + $firstOrder;

                if ($record->order != $order) {
                    $this->db
                        ->where('id', $record->id)
                        ->update('towns', array('order' => $order));

                    if ($this->db->affected_rows() > 0) {
                        $affected++;
                    }
                }

            }

            return $affected;
        }

    }










    public function district($id)
    {
        return $this->db
            ->from('districts')
            ->where('id', $id)
            ->get()
            ->row();
    }


    public function districtAll($parent, $limit = null, $offset = null)
    {
        $this->utils->filter();


        if ($limit != null) {
            $this->db->limit($limit, $offset);
        }

        return $this->db
            ->from('districts')
            ->where('townId', $parent->id)
            ->order_by('order', 'asc')
            ->order_by('id', 'asc')
            ->get()
            ->result();
    }


    public function districtCount($parent)
    {
        $this->utils->filter();

        return $this->db
            ->from('districts')
            ->where('townId', $parent->id)
            ->count_all_results();
    }


    public function districtInsert($parent, $data = array())
    {
        $order = 1;
        $lastOrderRecord = $this->db
            ->from('districts')
            ->where('townId', $parent->id)
            ->order_by('order', 'desc')
            ->limit(1)
            ->get()
            ->row();


        if ($lastOrderRecord) {
            $order = $lastOrderRecord->order + 1;
        }


        $this->db->insert('districts', array(
            'townId' => $parent->id,
            'title' => $this->input->post('title'),
            'slug' => $this->input->post('autoSlug') === 'true' ? makeSlug($this->input->post('title')) : makeSlug($this->input->post('slug')),
            'order' => $order,
        ));

        return $this->db->insert_id();
    }



    public function districtUpdate($record, $data = array())
    {
        $this->db
            ->where('id', $record->id)
            ->update('districts', array(
                'title' => $this->input->post('title'),
                'slug' => $this->input->post('autoSlug') === 'true' ? makeSlug($this->input->post('title')) : makeSlug($this->input->post('slug'))
            ));

        return $this->db->affected_rows();
    }



    public function districtDelete($data)
    {
        if (is_array($data)) {
            $success = $this->db
                ->where_in('id', $data)
                ->delete('districts');

            return $success;
        }

        $success = $this->db
            ->where('id', $data->id)
            ->delete('districts');

        return $success;
    }


    public function districtOrder($ids = null)
    {
        if (is_array($ids)) {
            $records = $this->db
                ->from('districts')
                ->where_in('id', $ids)
                ->order_by('order', 'asc')
                ->order_by('id', 'desc')
                ->get()
                ->result();

            $firstOrder = 0;
            $affected = 0;

            foreach ($records as $record) {
                if ($firstOrder === 0) {
                    $firstOrder = $record->order;
                }

                $order = array_search($record->id, $ids) + $firstOrder;

                if ($record->order != $order) {
                    $this->db
                        ->where('id', $record->id)
                        ->update('districts', array('order' => $order));

                    if ($this->db->affected_rows() > 0) {
                        $affected++;
                    }
                }

            }

            return $affected;
        }

    }

} 