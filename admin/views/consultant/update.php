
<div class="row">
    <form action="" method="post" enctype="multipart/form-data">
        <div class="col-md-8">
            <?php echo $this->utils->alert(); ?>

            <div class="panel panel-default">
                <div class="panel-heading"><i class="fa fa-edit"></i> Kayıt Düzenle</div>
                <div class="panel-body">

                    <?php echo bsFormText('name', 'İsim Soyisim', array('required' => true, 'value' => $record->name)) ?>
                    <?php echo bsFormText('degree', 'Ünvan', array('required' => true, 'value' => $record->degree)
                    ) ?>
                    <?php echo bsFormSlug('slug', 'Slug', array('value' => $record->slug)) ?>
                    <?php echo bsFormImage('image', 'Resim', array('value' => $record->image, 'path' => '../public/upload/consultant/')) ?>
                    <?php echo bsFormText('phone', 'Telefon (Sabit Hat)', array('value' => $record->phone)) ?>
                    <?php echo bsFormText('mobile', 'Telefon (Mobil)', array('value' => $record->mobile)) ?>
                    <?php echo bsFormText('email', 'E-Posta', array('value' => $record->email)) ?>
                    <?php echo bsFormTextarea('address', 'Adres', array('value' => $record->address)) ?>


                </div>
                <div class="panel-footer">
                    <button class="btn btn-success" type="submit">Kaydet</button>
                    <a class="btn btn-default" href="<?php echo $this->module ?>/records">Vazgeç</a>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-share"></i> Sosyal Paylaşım Linkleri
                </div>
                <div class="panel-body">

                    <?php echo bsFormText('facebook', 'Facebook', array('value' => $record->facebook)) ?>
                    <?php echo bsFormText('twitter', 'Twitter', array('value' => $record->twitter)) ?>
                    <?php echo bsFormText('google', 'Google Plus', array('value' => $record->google)) ?>
                </div>
            </div>
        </div>
    </form>
</div>

