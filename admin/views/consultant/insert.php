
<div class="row">
    <form action="" method="post" enctype="multipart/form-data">
        <div class="col-md-8">
            <?php echo $this->utils->alert(); ?>

            <div class="panel panel-default">
                <div class="panel-heading"><i class="fa fa-plus-square"></i> Yeni Kayıt Ekle</div>
                <div class="panel-body">

                    <?php echo bsFormText('name', 'İsim Soyisim', array('required' => true)) ?>
                    <?php echo bsFormText('degree', 'Ünvan', array('required' => true)) ?>
                    <?php echo bsFormSlug('slug', 'Slug', array('checked' => true)) ?>
                    <?php echo bsFormImage('image', 'Resim', array('required' => true)) ?>
                    <?php echo bsFormText('phone', 'Telefon (Sabit Hat)') ?>
                    <?php echo bsFormText('mobile', 'Telefon (Mobil)') ?>
                    <?php echo bsFormText('email', 'E-Posta', array()) ?>
                    <?php echo bsFormTextarea('address', 'Adres') ?>

                </div>
                <div class="panel-footer">
                    <button class="btn btn-success" type="submit">Kaydet</button>
                    <button class="btn btn-success" type="submit" name="redirect" value="<?php echo $this->module ?>/records">Kaydet ve Listeye Dön</button>
                    <a class="btn btn-default" href="<?php echo $this->module ?>/records">Vazgeç</a>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-share"></i> Sosyal Paylaşım Linkleri
                </div>
                <div class="panel-body">
                    <?php echo bsFormText('facebook', 'Facebook') ?>
                    <?php echo bsFormText('twitter', 'Twitter') ?>
                    <?php echo bsFormText('google', 'Google Plus') ?>
                </div>
            </div>
        </div>
    </form>
</div>