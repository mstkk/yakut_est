<div class="row">
    <?php echo $this->utils->alert(); ?>

    <form action="" method="post" enctype="multipart/form-data">
        <div class="col-md-12">
            <?php
            $record=$this->estateinsert->find($this->uri->segment(3));
            ?>
        </div>

        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-key"></i> Emlak Özellikleri
                </div>


<?php //print_r($record) ?>

                <div class="panel-body">

                    <?php echo bsFormCheckbox('emergency', 'Acil İlan ?', array('value' => 1)) ?>

                    <?php echo bsFormDropdown('cityId', 'Şehir', array(
                        'required' => true,
                        'value' => $record->cityId,
                        'options' => prepareForSelect($this->appmodel->getCities(), 'id', 'title', 'Seçiniz'),
                        'class' => 'city-list'
                    )) ?>
                    <?php echo bsFormDropdown('townId', 'İlçe', array(
                        'value' => $record->townId,
                        'options' => prepareForSelect($this->appmodel->getTowns($record->cityId), 'id',
                            'title', 'Şehir Seçiniz'),
                        'required' => true,
                        'class' => 'town-list'
                    )) ?>

                    <?php echo bsFormDropdown('localeId', 'Semt', array(
                        'required' => true,
                        'value' => $record->districtId,
                        'options' => prepareForSelect($this->appmodel->getDistricts($record->townId),
                            'id', 'title', 'İlçe Seçiniz'),
                        'class' => 'district-list'
                    )) ?>

                    <?php echo bsFormDropdown('statusId', 'Emlak Durumu', array(
                        'required' => true,
                        'value' => $record->estateStatus,
                        'options' => array('' => 'Seçiniz', '1' => 'Kiralık', '2' => 'Satılık'),
                    )) ?>

                    <?php echo bsFormDropdown('kind', 'Emlak Türü', array(
                        'required' => true,
                        'value' => $record->kindId,
                        'options' => prepareForSelect($this->appmodel->getKinds(), 'id', 'title', 'Seçiniz'),
                        'class' => 'kind-list'
                    )) ?>
                    <?php echo bsFormDropdown('type', 'Emlak Tipi', array(
                        'required' => true,
                        'options' => prepareForSelect($this->appmodel->getTypes($record->kindId), 'id', 'title',
                            'Emlak Türü Seçiniz'),
                        'value' => $record->typeId,
                        'class' => 'type-list'
                    )) ?>

                    <?php echo bsFormDropdown('investor', 'Yatırımcı Fırsatları?', array(
                        'options' => array('' => 'Seçiniz', '1' => 'Evet', '0' => 'Hayır'),
                        'class' => 'investor',
                    )) ?>

                    <?php echo bsFormDropdown('new_build', 'Yeni İnşaat', array(
                        'options' => array('' => 'Seçiniz', '1' => 'Evet', '0' => 'Hayır'),
                    )) ?>

                    <?php echo bsFormDropdown('month', 'Ayın Gayrimenkulü', array(
                        'options' => array('' => 'Seçiniz', '1' => 'Evet', '0' => 'Hayır'),
                    )) ?>

                    <?php echo bsFormDropdown('showcase', 'Ana Sayfada Göster', array(
                        'options' => array('' => 'Seçiniz', '1' => 'Evet', '0' => 'Hayır'),
                    )) ?>

                </div>

            </div>


            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-plus"></i> Diğer Özellikler
                </div>

                <div class="panel-body">

                    <div class="form-group">
                        <?php
                        $properties = $this->appmodel->getProperties();
                        $this->viewData['properties'] = $properties;
                        $propertieValidation = $this->appmodel->propertiesValidation($this->uri->segment(3));


                        $propertieValId =  json_decode($record->properties, true);;
                        ?>
                        <?php foreach ($properties as $propertie): ?>
                            <div class="col-sm-2 col-xs-12">
                                <div class="form-group">
                                    <div class="togglebutton">
                                        <label for="extras-<?= $propertie->id ?>" style=" <?= in_array($propertie->id, $propertieValId) ? 'border-left: 1px solid;padding: 5px;"' : 'border-left: 1px solid transparent;padding: 5px;"' ?> ">
                                            <input type="checkbox" name="properties[]" id="extras-<?= $propertie->id ?>" value="<?= $propertie->id ?>"
                                                <?= in_array($propertie->id, $propertieValId) ? 'checked="checked"' : '' ?> />
                                            <?= $propertie->title ?>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>

                    <?= bsFormText('map', 'Google Map Koordinatları') ?>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <?php echo $this->utils->alert(); ?>

            <div class="panel panel-default">
                <div class="panel-heading"><i class="fa fa-plus-square"></i> Yeni Kayıt Ekle</div>
                <div class="panel-body">

                    <?php echo bsFormText('estateNo', 'İlan No', array('required' => true)) ?>
                    <?php echo bsFormText('title', 'İlan Başlığı', array('required' => true)) ?>

                    <?php echo bsFormSlug('slug', 'Slug', array('checked' => true)) ?>

                    <?php echo bsFormImage('image', 'Resim', array('value' => $record->image,
                        'path' => '../public/upload/ilanlar/')) ?>

                    <?php echo bsFormText('video', 'Youtube Video Linki', array()) ?>


                    <div class="row">
                        <div class="col-md-4">
                            <?php echo bsFormText('price', 'Fiyat', array('value'=>$record->price,'required' => true,
                                'class' => 'numeric')) ?>
                        </div>
                        <div class="col-md-3">
                            <?php echo bsFormDropdown('moneyType','Kur Biçimi ',array('value'=>$record->moneyType,'options'=>['TL'=>'Türk Lirası','EU' =>'Euro','USD'=>'Dolar'])) ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4 col-sm-4">
                            <?php echo bsFormText('depozito', 'Depozito', array('class' => 'numeric')) ?>
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <?php echo bsFormDropdown('depozitoMoneyType','Kur Biçimi ',array('options'=>
                                ['TL'=>'Türk Lirası','EU' =>'Euro','USD'=>'Dolar'])) ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4 col-sm-4">
                            <?php echo bsFormText('aidat', 'Aidat', array('class' => 'numeric')) ?>
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <?php echo bsFormDropdown('aidatMoneyType','Kur Biçimi ',array('options'=>
                                ['TL'=>'Türk Lirası','EU' =>'Euro','USD'=>'Dolar'])) ?>
                        </div>
                    </div>

                    <div>
                        <div class="row">
                            <div class="col-md-4">
                                <?= bsFormText('kiraGetirisi', "Kira Getirisi", array('class' => 'numeric')) ?>
                            </div>
                            <div class="col-md-3">
                                <?php echo bsFormDropdown('kiraGetirisiMoneyType','Kur Biçimi ',array('options'=>['TL'=>'Türk Lirası','EU' =>'Euro','USD'=>'Dolar'])) ?>
                            </div>
                        </div>
                    </div>


                    

                    <?php echo bsFormText('squareMeter', 'Metre Kare (m2)',  array('value' => $record->squaremeter,
                        'required' => true, 'class' => 'numeric')) ?>

                    <div id="apart">
                    <?php echo bsFormDropdown('roomId', 'Oda Sayısı', array('value' => $record->roomId,
                        'options' => prepareForSelect($this->appmodel->getRooms(), 'id', 'title', 'Seçiniz'),
                    ))  ?>
                        <p class="help-block">Bina için lütfen Bina seçeneğini seçiniz.</p>

                    <?php echo bsFormText('bathCount', 'Banyo Sayısı', array('value' => $record->bath, 'class' => 'numeric')) ?>

                    <?php echo bsFormText('floorCount', 'Binadaki Kat Sayısı',array('value' => $record->storeyCount,)
                        ) ?>

                    <?php echo bsFormDropdown('floor', 'Bulunduğu Kat', array('value' => $record->floorId,
                        'options' => prepareForSelect($this->appmodel->getFloors(), 'id', 'title', 'Seçiniz'),
                    )) ?>

                    <?php echo bsFormDropdown('buildingAge', 'Binanın Yaşı', array('value' => $record->buildingAgeId,
                        'options' => prepareForSelect($this->appmodel->getBuildingAges(), 'id', 'title', 'Seçiniz'),
                    ))  ?>

                    <?php echo bsFormDropdown('heating', 'Isınma Şekli', array('value' => $record->heatingId,
                        'options' => prepareForSelect($this->estateinsert->getHeatings(), 'id', 'title', 'Seçiniz'),
                    ))?>

                    <?php echo bsFormDropdown('usageStatus', 'Kullanım Durumu',array('value' => $record->usageStatusId,
                        'options' => prepareForSelect($this->appmodel->getUsageStatuses(), 'id', 'title', 'Seçiniz'),
                    ))?>
                    </div>

                    <div id="dukkan" class="hide">
                        <?= bsFormText('cepheGen', "Cephe Genişliği", array('class' => 'numeric')) ?>

                        <?= bsFormText('tavanYuk', "Tavan Yüksekliği", array('class' => 'numeric')) ?>

                        <?= bsFormText('bolumSay', "Bölüm Sayısı", array('class' => 'numeric')) ?>
                    </div>

                    <div id="arsa" class="hide">

<!--                        --><?php //echo bsFormDropdown('imar', 'İmar Durumu', array(
//                            'options' => prepareForSelect($this->appmodel->getImar(), 'id', 'title'),
//                        )) ?>
                        <?php echo bsFormText('ada', 'Ada No', array()) ?>
                        <?php echo bsFormText('parsel', 'Parsel No', array()) ?>
                        <?php echo bsFormText('pafta', 'Pafta No', array()) ?>
                        <?php echo bsFormDropdown('kaks', 'Kaks (Emsal)', array(
                            'options' => prepareForSelect($this->appmodel->getKaks(), 'id', 'title'),
                        )) ?>
                        <?php echo bsFormDropdown('gabari', 'Gabari', array(
                            'options' => prepareForSelect($this->appmodel->getGabari(), 'id', 'title'),
                        )) ?>
                        <?php echo bsFormDropdown('tapu', 'Tapu Durumu', array(
                            'options' => prepareForSelect($this->appmodel->getTapu(), 'id', 'title'),
                        )) ?>
                        <?php echo bsFormDropdown('kat', 'Kat Karşılığı', array(
                            'options' => array(
                                '1' => 'Evet',
                                '2' => 'Hayır'
                            ),
                        )) ?>
                        <?php echo bsFormDropdown('kredi', 'Krediye Uygunluk', array(
                            'options' => array(
                                '3' => 'Bilinmiyor',
                                '1' => 'Evet',
                                '2' => 'Hayır',
                            ),
                        )) ?>
                        <?php echo bsFormDropdown('takas', 'Takaslı', array(
                            'options' => array(
                                '3' => 'Bilinmiyor',
                                '1' => 'Evet',
                                '2' => 'Hayır',
                            ),
                        )) ?>
                    </div>




                </div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-commenting"></i> Detaylar
                </div>
                <div class="panel-body">

                    <?php echo bsFormText('metaTitle', 'Meta Title') ?>
                    <p class="help-block">
                        Temel anlamda paylaşımlar, seo, google arama sonuçlarında gösterilecek olan başlıktır.
                    </p>
                    <?php echo bsFormTextarea('metaDescription', 'Meta Description') ?>
                    <p class="help-block">
                        Arama motorlarında görünmesi için web sayfasının tanımının yapıldığı alandır.
                    </p>
                    <?php echo bsFormTextarea('metaKeywords', 'Meta Keywords') ?>
                    <p class="help-block">
                        Arama motorlarında aramalarda yardımcı olması açısından kullanılan anahtar kelimelerdir.
                        Kelimeleri virgül ile ayırınız.
                    </p>
                    <?php echo bsFormEditor('detail', 'Detaylar', array('required' => true ,'value'=> $record->description)) ?>
                </div>


                <div class="panel-footer">
                    <button class="btn btn-success" type="submit">Kaydet</button>
                    <button class="btn btn-success" type="submit" name="redirect" value="<?php echo $this->module ?>/records">Kaydet ve Listeye Dön</button>
                    <a class="btn btn-default" href="<?php echo $this->module ?>/records">Vazgeç</a>
                </div>
            </div>
        </div>

    </form>
</div>