
<?php echo $this->utils->alert(); ?>

<div class="panel panel-default">
    <div class="panel-heading"><i class="fa fa-table"></i> <?php echo $this->moduleTitle ?></div>
    <div class="panel-toolbar clearfix">
        <div class="row">
            <div class="col-md-4">
                <?php if ($this->permission('delete')): ?>
                    <a class="btn btn-sm btn-info checkall" data-toggle="button"><i class="fa fa-check-square-o"></i> Hepsini Seç</a>
                    <a class="btn btn-sm btn-danger deleteall" href="<?php echo $this->module ?>/delete"><i class="fa fa-trash-o"></i></a>
                <?php endif; ?>
                <?php if ($this->permission('insert')): ?>
                    <a class="btn btn-sm btn-success" href="<?php echo $this->module ?>/insert"><i class="fa fa-plus"></i> Yeni Kayıt</a>
                <?php endif; ?>
                <?php if ($this->permission('update')): ?>
                    <a class="btn btn-sm btn-primary" href="<?php echo $this->module ?>/alldateupdate"><i class="fa fa-refresh"></i> Tüm Tarihleri Güncelle</a>
                <?php endif; ?>
                 <a class="btn btn-sm btn-primary" href="<?php echo $this->module ?>/records?isActive=0"><i class="fa fa-eye-slash"></i> Pasif İlanlar</a>
            </div>
            <div class="col-md-8 text-right">
                <form class="form-inline" action="" method="get" id="filter" accept-charset="utf-8" style="display: inline-block;">
                    <?php echo bsFormDropdown('statusId', 'Emlak Durumu : ', array(
                        'value' => $this->input->get('statusId'),
                        'options' => array('' => 'Hepsi', '1' => 'Kiralık', '2' => 'Satılık'),
                        'class' => 'input-sm'
                    )); ?>
                    <?php $this->view('filter') ?>
                </form>
            </div>
        </div>
    </div>
    <?php

    $sortType = $this->input->get('sortType') == 'desc' ? 'desc':'asc';
    $sortBy = $this->input->get('sortBy') ? $this->input->get('sortBy') : 'id';

    if (! $this->input->get('sortBy')) {
        $sortType = 'desc';
    }
    $sortTypeOptions = array(
        'desc' => array('reverse' => 'asc', 'icon' => 'up'),
        'asc' => array('reverse' => 'desc', 'icon' => 'down')
    );
    ?>
    <table class="table table-bordered table-hover">
        <thead>
        <tr>
            <th width="40" class="text-center"><i class="fa fa-ellipsis-v"></i></th>
            <th style="display: none" width="50">
                #
                <?php if ($sortBy == 'id'): ?>
                    <a href="<?php echo clink(array($this->module, 'records'), array('sortBy' => 'id', 'sortType' => $sortTypeOptions[$sortType]['reverse']), true) ?>"><i class="fa fa-chevron-<?php echo $sortTypeOptions[$sortType]['icon'] ?> text-primary"></i></a>
                <?php else: ?>
                    <a href="<?php echo clink(array($this->module, 'records'), array('sortBy' => 'id', 'sortType' => 'asc'), true) ?>"><i class="fa fa-chevron-down text-muted"></i></a>
                <?php endif; ?>
            </th>
            <th>
                Emlak No
                <?php if ($sortBy == 'estateNo'): ?>
                    <a href="<?php echo clink(array($this->module, 'records'), array('sortBy' => 'estateNo', 'sortType' => $sortTypeOptions[$sortType]['reverse']), true) ?>"><i class="fa fa-chevron-<?php echo $sortTypeOptions[$sortType]['icon'] ?> text-primary"></i></a>
                <?php else: ?>
                    <a href="<?php echo clink(array($this->module, 'records'), array('sortBy' => 'estateNo', 'sortType' => 'asc'), true) ?>"><i class="fa fa-chevron-down text-muted"></i></a>
                <?php endif; ?>
            </th>
            <th>Emlak Durumu</th>
            <th>Konum</th>
            <th>Emlak</th>
            <th>
                Fiyat
                <?php if ($sortBy == 'price'): ?>
                    <a href="<?php echo clink(array($this->module, 'records'), array('sortBy' => 'price', 'sortType' => $sortTypeOptions[$sortType]['reverse']), true) ?>"><i class="fa fa-chevron-<?php echo $sortTypeOptions[$sortType]['icon'] ?> text-primary"></i></a>
                <?php else: ?>
                    <a href="<?php echo clink(array($this->module, 'records'), array('sortBy' => 'price', 'sortType' => 'asc'), true) ?>"><i class="fa fa-chevron-down text-muted"></i></a>
                <?php endif; ?>
            </th>
            <th>
                Güncelleme Tarihi
                <?php if ($sortBy == 'updateDate'): ?>
                    <a href="<?php echo clink(array($this->module, 'records'), array('sortBy' => 'updateDate', 'sortType' => $sortTypeOptions[$sortType]['reverse']), true) ?>"><i class="fa fa-chevron-<?php echo $sortTypeOptions[$sortType]['icon'] ?> text-primary"></i></a>
                <?php else: ?>
                    <a href="<?php echo clink(array($this->module, 'records'), array('sortBy' => 'updateDate', 'sortType' => 'asc'), true) ?>"><i class="fa fa-chevron-down text-muted"></i></a>
                <?php endif; ?>
            </th>
            <th class="text-center">
                Gösterim
                <?php if ($sortBy == 'views'): ?>
                    <a href="<?php echo clink(array($this->module, 'records'), array('sortBy' => 'views', 'sortType' => $sortTypeOptions[$sortType]['reverse']), true) ?>"><i class="fa fa-chevron-<?php echo $sortTypeOptions[$sortType]['icon'] ?> text-primary"></i></a>
                <?php else: ?>
                    <a href="<?php echo clink(array($this->module, 'records'), array('sortBy' => 'views', 'sortType' => 'asc'), true) ?>"><i class="fa fa-chevron-down text-muted"></i></a>
                <?php endif; ?>
            </th>
            <th width="100" class="text-center">Resimler</th>
            <th width="100" class="text-center">Özellikler</th>
            <th width="100" class="text-center hide">Sıra</th>
            <th width="100" class="text-right">İşlem</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($records as $item): ?>
            <tr data-id="<?php echo $item->id ?>">
                <td class="text-center">
                    <input type="checkbox" class="checkall-item" value="<?php echo $item->id ?>" />
                </td>
                <td class="hidden"><?php echo $item->id ?></td>
                <td><?php echo $item->id ?></td>
                <td><?php echo $item->statusId == 1 ? 'Kiralık' : 'Satılık' ?></td>
                <td><?php echo $item->adresss ?></td>
                <td><?php echo $item->kindTitle ?> - <?php echo $item->typeTitle ?></td>
                <td><?php echo money($item->price) ?> <?php echo $item->moneyType ?></td>
                <td><?php echo $this->date->set($item->updateDate)->datetimeWithName() ?></td>
                <td class="text-center"><?php echo $item->views ?></td>
                <td class="text-center">
                    <a class="btn btn-success btn-xs" data-toggle="tooltip" title="Resim Ekle / Düzenle" href="<?php echo $this->module ?>/images/<?php echo $item->id ?>"><i class="fa fa-picture-o"></i> <?php echo $item->images ?></a>
                </td>
                <td class="text-center">
                    <span class="btn btn-success btn-xs"><i class="fa fa-file-text-o"></i> <?php echo $item->properties ?></span>
                </td>
                <td class="text-center hide">
                    <div class="btn-group">
                        <a class="btn btn-xs btn-info disabled" data-toggle="tooltip" title="gg!"><?php echo $item->order ?></a>
                        <?php if (! $this->input->get() || $this->input->get('page')): ?>
                            <a class="btn btn-xs btn-default sortable-handle"><i class="fa fa-arrows"></i></a>
                        <?php endif; ?>
                    </div>
                </td>
                <td class="text-right">
                    <?php if ($item->isActive == 1): ?>
                    <a class="btn btn-<?php echo $item->vitrinIlan == 1 ? 'success':'default' ?> btn-xs"  data-toggle="tooltip" title="<?php echo $item->vitrinIlan == 1 ? 'Vitrinden Çıkar':'Vitrine Ekle' ?>" href="<?= $this->module.'/approve/'.$item->id ?>">
                        <i class="fa fa-check"></i></a>
                    <?php endif; ?>
                    <a class="btn btn-xs btn-warning" data-toggle="tooltip" title="İlan'a git!" href="../<?php echo clink(array('@advertising', $item->slug,
                    $item->id)) ?>"
                       target="_blank"><i class="fa fa-external-link-square "></i></a>
                    <?php if ($this->permission('update')): ?>
                        <a class="btn btn-xs btn-primary" data-toggle="tooltip" title="Düzenle" href="<?php echo $this->module ?>/update/<?php echo $item->id ?>"><i class="fa fa-edit"></i></a>
                    <?php endif; ?>
                    <?php if ($item->vitrinIlan == 0): ?>
                        <a class="btn btn-<?php echo $item->isActive == 1 ? 'default':'danger' ?> btn-xs"  data-toggle="tooltip" title="<?php echo $item->isActive == 1 ? 'Pasife Çevir':'Aktif Et' ?>" href="<?= $this->module.'/isActive/'.$item->id ?>">
                            <i class="fa fa-check"></i></a>
                    <?php endif; ?>
                    <?php if ($this->permission('delete')): ?>
                        <a class="btn btn-xs btn-danger confirm-delete" data-toggle="tooltip" title="Sil!"
                           href="<?= base_url().$this->module ?>/delete/<?php echo $item->id ?>">
                            <i class="fa fa-trash-o"></i>
                        </a>
                    <?php endif; ?>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>

    <?php if (! empty($pagination)): ?>
        <div class="panel-footer">
            <?php echo $pagination ?>
        </div>
    <?php endif; ?>
</div>