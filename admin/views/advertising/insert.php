
<script type="text/javascript" src='https://maps.google.com/maps/api/js?key=AIzaSyAjqeyww0ss-1ckf32JttCaFqhwTi2r8CM&sensor=false&libraries=places'></script>
<script type="text/javascript" src='../public/js/locationpicker.jquery.min.js'></script>
<div class="row">
    <form action="" method="post" enctype="multipart/form-data">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-key"></i> Emlak Özellikleri
                </div>

                <div class="panel-body">

                    <?php echo bsFormDropdown('cityId', 'Şehir', array(
                        'required' => true,
                        'options' => prepareForSelect($this->appmodel->getCities(), 'id', 'title', 'Seçiniz'),
                        'class' => 'city-list'
                    )) ?>
                    <?php echo bsFormDropdown('townId', 'İlçe', array(
                        'required' => true,
                        'class' => 'town-list'
                    )) ?>

                    <?php echo bsFormDropdown('localeId', 'Semt', array(
                        'required' => true,
                        'class' => 'district-list'
                    )) ?>

                    <?php echo bsFormDropdown('statusId', 'Emlak Durumu', array(
                        'required' => true,
                        'options' => array('' => 'Seçiniz', '1' => 'Kiralık', '2' => 'Satılık'),
                    )) ?>

                    <?php echo bsFormDropdown('kind', 'Emlak Türü', array(
                        'required' => true,
                        'options' => prepareForSelect($this->appmodel->getKinds(), 'id', 'title', 'Seçiniz'),
                        'class' => 'kind-list',
                    )) ?>

                    <?php echo bsFormDropdown('type', 'Emlak Tipi', array(
                        'required' => true,
                        'options' => prepareForSelect($this->appmodel->getTypes($this->input->post('kind')), 'id', 'title', 'Emlak Türü Seçiniz'),
                        'class' => 'type-list'
                    )) ?>

                    <?php echo bsFormDropdown('investor', 'Yatırımcı Fırsatları?', array(
                        'options' => array('' => 'Seçiniz', '1' => 'Evet', '0' => 'Hayır'),
                        'class' => 'investor',
                    )) ?>
                    <?php echo bsFormDropdown('tapu', 'Tapu Durumu', array(
                        'options' => array('' => 'Seçiniz', 'Arsa' => 'Arsa', 'Kat İrtifakı' => 'Kat İrtifakı', 'Kat Mülkiyeti' => 'Kat Mülkiyeti'),
                    )) ?>
                    <?php echo bsFormDropdown('cephe', 'Cephe Seçenekleri', array(
                        'options' => array('' => 'Seçiniz', 'Kuzey' => 'Kuzey', 'Güney' => 'Güney', 'Doğu' => 'Doğu', 'Batı' => 'Batı'),
                    )) ?>
                    <?php echo bsFormDropdown('sitein', 'Site İçerisinde', array(
                        'options' => array('' => 'Seçiniz', 'Evet' => 'Evet', 'Hayır' => 'Hayır'),
                    )) ?>
                    <?php echo bsFormDropdown('durumuyapi', 'Yapının Durumu', array(
                        'options' => array('' => 'Seçiniz', 'Sıfır' => 'Sıfır', '2. El' => '2. El', 'Yapım Aşamasında' => 'Yapım Aşamasında'),
                    )) ?>
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <?php echo $this->utils->alert(); ?>

            <div class="panel panel-default">
                <div class="panel-heading"><i class="fa fa-plus-square"></i> Yeni Kayıt Ekle</div>
                <div class="panel-body">

                    <?php echo bsFormText('title', 'İlan Başlığı', array('required' => true)) ?>

                    <?php echo bsFormSlug1('slug', '', array('checked' => true, 'class' => 'hidden')) ?>

                    <?php echo bsFormImage1('image', 'Resim') ?>

                    <div class="row">
                        <div class="col-md-4">
                            <?php echo bsFormText('price', 'Fiyat', array('required' => true,
                                'class' => 'numeric')) ?>
                        </div>
                        <div class="col-md-3">
                            <?php echo bsFormDropdown('moneyType','Kur Biçimi ',array('options'=>['TL'=>'Türk Lirası','EU' =>'Euro','USD'=>'Dolar'])) ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4 col-sm-4">
                            <?php echo bsFormText('depozito', 'Depozito', array('class' => 'numeric')) ?>
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <?php echo bsFormDropdown('depozitoMoneyType','Kur Biçimi ',array('options'=>
                                ['TL'=>'Türk Lirası','EU' =>'Euro','USD'=>'Dolar'])) ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4 col-sm-4">
                            <?php echo bsFormText('aidat', 'Aidat', array('class' => 'numeric')) ?>
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <?php echo bsFormDropdown('aidatMoneyType','Kur Biçimi ',array('options'=>
                                ['TL'=>'Türk Lirası','EU' =>'Euro','USD'=>'Dolar'])) ?>
                        </div>
                    </div>

                    <div>
                        <div class="row">
                            <div class="col-md-4">
                                <?= bsFormText('kiraGetirisi', "Kira Getirisi", array('class' => 'numeric')) ?>
                            </div>
                            <div class="col-md-3">
                                <?php echo bsFormDropdown('kiraGetirisiMoneyType','Kur Biçimi ',array('options'=>['TL'=>'Türk Lirası','EU' =>'Euro','USD'=>'Dolar'])) ?>
                            </div>
                        </div>
                    </div>

                    <?php echo bsFormText('squareMeter', 'Metre Kare (m2)', array('required' => true, 'class' =>
                        'numeric')) ?>

                    <div id="dukkan" class="hide">
                        <?= bsFormText('cepheGen', "Cephe Genişliği", array('class' => 'numeric')) ?>

                        <?= bsFormText('tavanYuk', "Tavan Yüksekliği", array('class' => 'numeric')) ?>

                        <?= bsFormText('bolumSay', "Bölüm Sayısı", array('class' => 'numeric')) ?>
                    </div>
                    <div id="apart">
                    <?php echo bsFormDropdown('roomId', 'Oda Sayısı', array(
                        'options' => prepareForSelect($this->appmodel->getRooms(), 'id', 'title', 'Seçiniz'),
                    )) ?>
                        <p class="help-block">Bina için lütfen Bina seçeneğini seçiniz.</p>

                    <?php echo bsFormText('bathCount', 'Banyo Sayısı', array('class' =>
                        'numeric')) ?>

                    <?php echo bsFormText('floorCount', 'Binadaki Kat Sayısı') ?>

                    <?php echo bsFormDropdown('floor', 'Bulunduğu Kat', array(
                        'options' => prepareForSelect($this->appmodel->getFloors(), 'id', 'title', 'Seçiniz'),
                    )) ?>

                    <?php echo bsFormDropdown('buildingAge', 'Binanın Yaşı', array(
                        'options' => prepareForSelect($this->appmodel->getBuildingAges(), 'id', 'title', 'Seçiniz'),
                    )) ?>

                    <?php echo bsFormDropdown('heating', 'Isınma Şekli', array(
                        'options' => prepareForSelect($this->appmodel->getHeatings(), 'id', 'title', 'Seçiniz'),
                    )) ?>

                    <?php echo bsFormDropdown('usageStatus', 'Kullanım Durumu', array(
                        'options' => prepareForSelect($this->appmodel->getUsageStatuses(), 'id', 'title', 'Seçiniz'),
                    )) ?>
                        <?php echo bsFormDropdown('krediuygun', 'Krediye Uygunluk', array(
                            'options' => array('' => 'Seçiniz', 'Evet' => 'Evet', 'Hayır' => 'Hayır'),
                        )) ?>
                    </div>

                    <div id="arsa" class="hide">

<!--                        --><?php //echo bsFormDropdown('imar', 'İmar Durumu', array(
//                            'options' => prepareForSelect($this->appmodel->getImar(), 'id', 'title'),
//                        )) ?>
                        <?php echo bsFormText('ada', 'Ada No', array()) ?>
                        <?php echo bsFormText('parsel', 'Parsel No', array()) ?>
                        <?php echo bsFormText('pafta', 'Pafta No', array()) ?>
                        <?php echo bsFormDropdown('kaks', 'Kaks (Emsal)', array(
                            'options' => prepareForSelect($this->appmodel->getKaks(), 'id', 'title'),
                        )) ?>
                        <?php echo bsFormDropdown('gabari', 'Gabari', array(
                            'options' => prepareForSelect($this->appmodel->getGabari(), 'id', 'title'),
                        )) ?>
                        <?php echo bsFormDropdown('tapu', 'Tapu Durumu', array(
                            'options' => prepareForSelect($this->appmodel->getTapu(), 'id', 'title'),
                        )) ?>
                        <?php echo bsFormDropdown('kat', 'Kat Karşılığı', array(
                            'options' => array(
                                '1' => 'Evet',
                                '2' => 'Hayır'
                            ),
                        )) ?>
                        <?php echo bsFormDropdown('kredi', 'Krediye Uygunluk', array(
                            'options' => array(
                                '3' => 'Bilinmiyor',
                                '1' => 'Evet',
                                '2' => 'Hayır',
                            ),
                        )) ?>
                        <?php echo bsFormDropdown('takas', 'Takaslı', array(
                            'options' => array(
                                '3' => 'Bilinmiyor',
                                '1' => 'Evet',
                                '2' => 'Hayır',
                            ),
                        )) ?>
                    </div>




                </div>
            </div>
        </div>
        <div class="col-md-12">

            <div class="panel panel-default">

                <div class="panel-body" style="    padding: 0;">
                    <div class="row">
                    <!--<div class="col-md-12">
                        <div class="panel-heading" style="background: #FFC107;">
                            <i data-toggle="collapse" data-target="#inproperty" class="fa fa-plus"></i> İç Özellikler
                        </div>
                        <?php
                        $properties2 = $this->appmodel->getProperties2();
                        $this->viewData['properties'] = $properties2;
                        ?>
                        <div class="col-md-12 collapse in" id="inproperty">
                        <?php foreach ($properties2 as $propertie2): ?>
                            <div class="col-sm-2 col-xs-12">
                                <div class="form-group">
                                    <div class="togglebutton">
                                        <label for="extras2-<?= $propertie2->id ?>" style=" border-left: 1px solid transparent;padding: 5px;">
                                            <input type="checkbox" name="properties2[]" id="extras2-<?= $propertie2->id ?>" value="<?= $propertie2->id ?>" />
                                            <?= $propertie2->title ?>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="panel-heading" style="background: #FFC107;">
                            <i data-toggle="collapse" data-target="#outproperty" class="fa fa-plus"></i> Dış Özellikler
                        </div>
                        <?php
                        $properties3 = $this->appmodel->getProperties3();
                        $this->viewData['properties'] = $properties3;
                        ?>
                        <div class="col-md-12 collapse" id="outproperty">
                        <?php foreach ($properties3 as $propertie3): ?>
                            <div class="col-sm-2 col-xs-12">
                                <div class="form-group">
                                    <div class="togglebutton">
                                        <label for="extras3-<?= $propertie3->id ?>" style=" border-left: 1px solid transparent;padding: 5px;">
                                            <input type="checkbox" name="properties3[]" id="extras3-<?= $propertie3->id ?>" value="<?= $propertie3->id ?>" />
                                            <?= $propertie3->title ?>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                        </div>
                    </div>-->
                    <div class="col-md-12">
                        <div class="panel-heading" style="background: #FFC107;">
                            <i data-toggle="collapse" data-target="#locationprop" class="fa fa-plus"></i> Diğer Özellikler
                        </div>
                        <?php
                        $properties = $this->appmodel->getProperties();
                        $this->viewData['properties'] = $properties;
                        ?>
                        <div class="col-md-12 collapse in" id="locationprop">
                        <?php foreach ($properties as $propertie): ?>
                            <div class="col-sm-2 col-xs-12" >
                                <div class="form-group">
                                    <div class="togglebutton">
                                        <label for="extras-<?= $propertie->id ?>" style=" border-left: 1px solid transparent;padding: 5px;">
                                            <input type="checkbox" name="properties[]" id="extras-<?= $propertie->id ?>" value="<?= $propertie->id ?>" />
                                            <?= $propertie->title ?>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                        </div>
                    </div>
                </div>
                </div>
            </div>

        </div>
        <div class="col-md-12">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-plus"></i> Harita Konum Seçimi
                </div>

                <div class="panel-body">

                    <div class="form-group">
                        <div class="form-horizontal" style="">
                            <div class="form-group">
                                <label class="col-sm-1 control-label">Adres:</label>

                                <div class="col-sm-11">
                                    <input type="text" class="form-control" id="us3-address" />
                                </div>
                            </div>
                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label">Radius:</label>

                                <div class="col-sm-5">
                                    <input type="text" class="form-control" id="us3-radius" />
                                </div>
                            </div>
                            <div id="us3" style="width: 100%; height: 400px;"></div>
                            <div class="m-t-small hidden">
                                <label class="p-r-small col-sm-1 control-label">Lat.:</label>
                                <div class="col-sm-3">
                                    <input type="text" name="enlem" class="form-control" style="width: 110px" id="us3-lat" />
                                </div>
                                <label class="p-r-small col-sm-2 control-label">Long.:</label>
                                <div class="col-sm-3">
                                    <input type="text" name="boylam" class="form-control" style="width: 110px" id="us3-lon" />
                                </div>
                            </div>
                            <script>
                                $('#us3').locationpicker({
                                    location: {
                                        latitude: 38.423734,
                                        longitude: 27.142826000000014
                                    },
                                    radius: 100,
                                    inputBinding: {
                                        latitudeInput: $('#us3-lat'),
                                        longitudeInput: $('#us3-lon'),
                                        radiusInput: $('#us3-radius'),
                                        locationNameInput: $('#us3-address')
                                    },
                                    enableAutocomplete: true,
                                    onchanged: function (currentLocation, radius, isMarkerDropped) {
                                        // Uncomment line below to show alert on each Location Changed event
                                        //alert("Location changed. New location (" + currentLocation.latitude + ", " + currentLocation.longitude + ")");
                                    }
                                });
                            </script>
                        </div>
                    </div>

                </div>
            </div>

        </div>
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-commenting"></i> Detaylar
                </div>
                <div class="panel-body">
                    <?php echo bsFormText('adresss', 'Açık Adres', array('required' => true)) ?>
                    <?php echo bsFormTextarea('metaKeywords', '', array('class' => "hidden",'value' => "konfor gayrimenkul, konforemlak, konfor emlak, izmir emlak, konforgayrimenkul, konfor ilanları, izmir urla emlakçı, izmir urla emlak, emlak izmir, konfor gayrimenkul emlak")) ?>
                    <?php echo bsFormEditor('detail', 'Detaylar', array('required' => true)) ?>
                </div>


                <div class="panel-footer">
                    <button class="btn btn-success" type="submit">Kaydet</button>
                    <button class="btn btn-success" type="submit" name="redirect" value="<?php echo $this->module ?>/records">Kaydet ve Listeye Dön</button>
                    <a class="btn btn-default" href="<?php echo $this->module ?>/records">Vazgeç</a>
                </div>
            </div>
        </div>

    </form>
</div>