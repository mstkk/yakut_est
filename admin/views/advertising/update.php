<script type="text/javascript" src='https://maps.google.com/maps/api/js?key=AIzaSyAjqeyww0ss-1ckf32JttCaFqhwTi2r8CM&sensor=false&libraries=places'></script>
<script type="text/javascript" src='../public/js/locationpicker.jquery.min.js'></script>
<div class="row">
    <form action="" method="post" enctype="multipart/form-data">
        <div class="col-md-12">
            <?php echo $this->utils->alert(); ?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-key"></i> Emlak Özellikleri
                </div>

                <div class="panel-body">

                    <?php echo bsFormDropdown('cityId', 'Şehir', array(
                        'required' => true,
                        'value' => $record->cityId,
                        'options' => prepareForSelect($this->appmodel->getCities(), 'id', 'title', 'Seçiniz'),
                        'class' => 'city-list'
                    )) ?>
                    <?php echo bsFormDropdown('townId', 'İlçe', array(
                        'required' => true,
                        'value' => $record->townId,
                        'options' => prepareForSelect($this->appmodel->getTowns($record->cityId), 'id',
                            'title', 'Şehir Seçiniz'),
                        'class' => 'town-list'
                    )) ?>

                    <?php echo bsFormDropdown('localeId', 'Semt', array(
                        'required' => true,
                        'value' => $record->localeId,
                        'options' => prepareForSelect($this->appmodel->getDistricts($record->townId),
                            'id', 'title', 'İlçe Seçiniz'),
                        'class' => 'district-list'
                    )) ?>

                    <?php echo bsFormDropdown('statusId', 'Emlak Durumu', array(
                        'required' => true,
                        'value' => $record->statusId,
                        'options' => array('' => 'Seçiniz', '1' => 'Kiralık', '2' => 'Satılık'),
                    )) ?>

                    <?php echo bsFormDropdown('kind', 'Emlak Türü', array(
                        'required' => true,
                        'value' => $record->estateKindId,
                        'options' => prepareForSelect($this->appmodel->getKinds(), 'id', 'title', 'Seçiniz'),
                        'class' => 'kind-list'
                    )) ?>

                    <?php echo bsFormDropdown('type', 'Emlak Tipi', array(
                        'required' => true,
                        'value' => $record->estateTypeId,
                        'options' => prepareForSelect($this->appmodel->getTypes($record->estateKindId), 'id', 'title',
                            'Emlak Türü Seçiniz'),
                        'class' => 'type-list'
                    )) ?>

                    <?php echo bsFormDropdown('investor', 'Yatırımcı Fırsatları?', array(
                        'value' => $record->investor,
                        'options' => array('' => 'Seçiniz', '1' => 'Evet', '0' => 'Hayır'),
                    )) ?>

                    <?php echo bsFormDropdown('tapu', 'Tapu Durumu', array(
                        'value' => $record->tapu,
                        'options' => array('' => 'Seçiniz', 'Arsa' => 'Arsa', 'Kat İrtifakı' => 'Kat İrtifakı', 'Kat Mülkiyeti' => 'Kat Mülkiyeti'),
                    )) ?>
                    <?php echo bsFormDropdown('cephe', 'Cephe Seçenekleri', array(
                        'value' => $record->cephee,
                        'options' => array('' => 'Seçiniz', 'Kuzey' => 'Kuzey', 'Güney' => 'Güney', 'Doğu' => 'Doğu', 'Batı' => 'Batı'),
                    )) ?>
                    <?php echo bsFormDropdown('sitein', 'Site İçerisi', array(
                        'value' => $record->sitein,
                        'options' => array('' => 'Seçiniz', 'Evet' => 'Evet', 'Hayır' => 'Hayır'),
                    )) ?>
                    <?php echo bsFormDropdown('durumuyapi', 'Yapının Durumu', array(
                        'value' => $record->durumuyapi,
                        'options' => array('' => 'Seçiniz', 'Sıfır' => 'Sıfır', '2. El' => '2. El', 'Yapım Aşamasında' => 'Yapım Aşamasında'),
                    )) ?>

                </div>

            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-plus"></i> Emlak Özellikler
                </div>

                <div class="panel-body">
                         <!--<div class="col-md-12">
                             <div class="panel-heading" style="background: #FFC107;">
                                 <i data-toggle="collapse" data-target="#inproperty" class="fa fa-plus"></i> İç Özellikler
                             </div>
                            <?php
                            $properties2 = $this->appmodel->getProperties2();
                            $this->viewData['properties2'] = $properties2;
                            $propertieValidation2 = $this->appmodel->propertiesValidation2($this->uri->segment(3));

                            $propertieValId2 = [];
                            foreach ($propertieValidation2 as $val) {
                                $propertieValId2[] .= $val->propertyId;
                            }
                            $this->viewData['propertieValidation2'] = $propertieValId2;
                            ?>
                            <?php foreach ($properties2 as $propertie): ?>
                                <div class="col-sm-2 col-xs-12">
                                    <div class="form-group">
                                        <div class="togglebutton">
                                            <label for="extras2-<?= $propertie->id ?>" style=" <?= in_array($propertie->id, $propertieValId2) ? 'border-left: 1px solid;padding: 5px;"' : 'border-left: 1px solid transparent;padding: 5px;"' ?> ">
                                                <input type="checkbox" name="properties2[]" id="extras2-<?= $propertie->id ?>" value="<?= $propertie->id ?>"
                                                    <?= in_array($propertie->id, $propertieValId2) ? 'checked="checked"' : '' ?> />
                                                <?= $propertie->title ?>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                         </div>-->
                        <!--<div class="col-md-12">
                            <div class="panel-heading" style="background: #FFC107;">
                                <i data-toggle="collapse" data-target="#inproperty" class="fa fa-plus"></i> Dış Özellikler
                            </div>
                        <?php
                        $properties3 = $this->appmodel->getProperties3();
                        $this->viewData['properties3'] = $properties3;
                        $propertieValidation3 = $this->appmodel->propertiesValidation3($this->uri->segment(3));

                        $propertieValId3 = [];
                        foreach ($propertieValidation3 as $val) {
                            $propertieValId3[] .= $val->propertyId;
                        }
                        $this->viewData['propertieValidation3'] = $propertieValId3;
                        ?>
                        <?php foreach ($properties3 as $propertie): ?>
                            <div class="col-sm-2 col-xs-12">
                                <div class="form-group">
                                    <div class="togglebutton">
                                        <label for="extras3-<?= $propertie->id ?>" style=" <?= in_array($propertie->id, $propertieValId3) ? 'border-left: 1px solid;padding: 5px;"' : 'border-left: 1px solid transparent;padding: 5px;"' ?> ">
                                            <input type="checkbox" name="properties3[]" id="extras3-<?= $propertie->id ?>" value="<?= $propertie->id ?>"
                                                <?= in_array($propertie->id, $propertieValId3) ? 'checked="checked"' : '' ?> />
                                            <?= $propertie->title ?>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                        </div>-->
                        <div class="col-md-12">
                            <div class="panel-heading" style="background: #FFC107;">
                                <i data-toggle="collapse" data-target="#inproperty" class="fa fa-plus"></i> Konum
                            </div>
                        <?php
                        $properties = $this->appmodel->getProperties();
                        $this->viewData['properties'] = $properties;
                        $propertieValidation = $this->appmodel->propertiesValidation($this->uri->segment(3));

                        $propertieValId = [];
                        foreach ($propertieValidation as $val) {
                            $propertieValId[] .= $val->propertyId;
                        }
                        $this->viewData['propertieValidation'] = $propertieValId;
                        ?>
                        <?php foreach ($properties as $propertie): ?>
                            <div class="col-sm-2 col-xs-12">
                                <div class="form-group">
                                    <div class="togglebutton">
                                        <label for="extras-<?= $propertie->id ?>" style=" <?= in_array($propertie->id, $propertieValId) ? 'border-left: 1px solid;padding: 5px;"' : 'border-left: 1px solid transparent;padding: 5px;"' ?> ">
                                            <input type="checkbox" name="properties[]" id="extras-<?= $propertie->id ?>" value="<?= $propertie->id ?>"
                                                <?= in_array($propertie->id, $propertieValId) ? 'checked="checked"' : '' ?> />
                                            <?= $propertie->title ?>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                        </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-plus"></i> Harita Konum Seçimi
                </div>

                <div class="panel-body">

                    <div class="form-group">
                        <div class="form-horizontal" style="">
                            <div class="form-group">
                                <label class="col-sm-1 control-label">Adres:</label>

                                <div class="col-sm-11">
                                    <input type="text" class="form-control" id="us3-address" />
                                </div>
                            </div>
                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label">Radius:</label>

                                <div class="col-sm-5">
                                    <input type="text" class="form-control" id="us3-radius" />
                                </div>
                            </div>
                            <div id="us3" style="width: 100%; height: 400px;"></div>
                            <div class="m-t-small hidden">
                                <label class="p-r-small col-sm-1 control-label">Lat.:</label>
                                <div class="col-sm-3">
                                    <input type="text" name="enlem" class="form-control" style="width: 110px" id="us3-lat" />
                                </div>
                                <label class="p-r-small col-sm-2 control-label">Long.:</label>
                                <div class="col-sm-3">
                                    <input type="text" name="boylam" class="form-control" style="width: 110px" id="us3-lon" />
                                </div>
                            </div>
                            <script>
                                $('#us3').locationpicker({
                                    location: {
                                        latitude: <?= $record->enlem ?>,
                                        longitude: <?= $record->boylam ?>
                                    },
                                    radius: 100,
                                    inputBinding: {
                                        latitudeInput: $('#us3-lat'),
                                        longitudeInput: $('#us3-lon'),
                                        radiusInput: $('#us3-radius'),
                                        locationNameInput: $('#us3-address')
                                    },
                                    enableAutocomplete: true,
                                    onchanged: function (currentLocation, radius, isMarkerDropped) {
                                        // Uncomment line below to show alert on each Location Changed event
                                        //alert("Location changed. New location (" + currentLocation.latitude + ", " + currentLocation.longitude + ")");
                                    }
                                });
                            </script>
                        </div>
                    </div>

                </div>
            </div>

        </div>
        <div class="col-md-12">

            <div class="panel panel-default">
                <div class="panel-heading"><i class="fa fa-edit"></i> Kayıt Düzenle</div>
                <div class="panel-body">


                    <?php echo bsFormText('title', 'Başlık', array('required' => true, 'value' => $record->title)) ?>

                    <?php echo bsFormSlug1('slug', '', array('required' => true, 'value' => $record->slug, 'class' => 'hidden')) ?>

                    <?php echo bsFormImage1('image', 'Resim', array('value' => $record->image,
                        'path' => '../public/upload/ilanlar/')) ?>




                    <div class="row">
                        <div class="col-md-4 col-sm-4">
                            <?php echo bsFormText('price', 'Fiyat', array('value' => $record->price,'required' => true,
                                'class' => 'numeric')) ?>
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <?php echo bsFormDropdown('moneyType','Kur Biçimi ',array('options'=>
                                ['TL'=>'Türk Lirası','EU' =>'Euro','USD'=>'Dolar'],'value'=>$record->moneyType)) ?>
                        </div>
                    </div>



                    <div class="row">
                        <div class="col-md-4 col-sm-4">
                            <?php echo bsFormText('depozito', 'Depozito', array('class' => 'numeric',
                                'value' => $record->depozito)) ?>
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <?php echo bsFormDropdown('depozitoMoneyType','Kur Biçimi ',array('options'=>
                                ['TL'=>'Türk Lirası','EU' =>'Euro','USD'=>'Dolar'],'value'=>$record->depozitoMoneyType)) ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4 col-sm-4">
                            <?php echo bsFormText('aidat', 'Aidat', array(
                                'class' => 'numeric',
                                'value' => $record->aidat)) ?>
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <?php echo bsFormDropdown('aidatMoneyType','Kur Biçimi ',array('options'=>
                                ['TL'=>'Türk Lirası','EU' =>'Euro','USD'=>'Dolar'],'value'=>$record->aidatMoneyType)) ?>
                        </div>
                    </div>


                    <div>
                        <div class="row">
                            <div class="col-md-4">
                                <?= bsFormText('kiraGetirisi', "Kira Getirisi", array('class' => 'numeric',
                                    'value' => $record->kiraGetirisi)) ?>
                            </div>
                            <div class="col-md-3">
                                <?php echo bsFormDropdown('kiraGetirisiMoneyType','Kur Biçimi ',array('options'=>['TL'=>'Türk Lirası','EU' =>'Euro','USD'=>'Dolar'] ,
                                    'value' => $record->kiraGetirisiMoneyType)) ?>
                            </div>
                        </div>
                    </div>

                    <?php echo bsFormText('squareMeter', 'Metre Kare (m2)', array('value' => $record->squareMeter,
                        'required' => true, 'class' => 'numeric')) ?>

                    <div id="dukkan" class="<?= $record->estateKindId == 2 ? '' : 'hide' ?>">
                        <?= bsFormText('cepheGen', "Cephe Genişliği", array('value' => $record->cepheGen,
                            'class' => 'numeric')) ?>

                        <?= bsFormText('tavanYuk', "Tavan Yüksekliği", array('value' => $record->tavanYuk,
                            'class' => 'numeric')) ?>

                        <?= bsFormText('bolumSay', "Bölüm Sayısı", array('value' => $record->bolumSay,
                            'class' => 'numeric')) ?>
                    </div>

                    <div id="apart" class="<?= $record->estateKindId == 3 ? 'hide' : '' ?>">

                        <?php echo bsFormDropdown('roomId', 'Oda Sayısı', array('value' => $record->roomId,
                            'options' => prepareForSelect($this->appmodel->getRooms(), 'id', 'title', 'Seçiniz'),
                        )) ?>
                        <p class="help-block">Bina için lütfen Bina seçeneğini seçiniz.</p>

                        <?php echo bsFormText('bathCount', 'Banyo Sayısı', array('value' => $record->bathCount, 'class' => 'numeric')) ?>

                        <?php echo bsFormText('floorCount', 'Binadaki Kat Sayısı', array('value' => $record->floorCount,)
                        ) ?>

                        <?php echo bsFormDropdown('floor', 'Bulunduğu Kat', array('value' => $record->floorId,
                            'options' => prepareForSelect($this->appmodel->getFloors(), 'id', 'title', 'Seçiniz'),
                        )) ?>

                        <?php echo bsFormDropdown('buildingAge', 'Binanın Yaşı', array('value' => $record->ageId,
                            'options' => prepareForSelect($this->appmodel->getBuildingAges(), 'id', 'title', 'Seçiniz'),
                        )) ?>

                        <?php echo bsFormDropdown('heating', 'Isınma Şekli', array('value' => $record->heatId,
                            'options' => prepareForSelect($this->appmodel->getHeatings(), 'id', 'title', 'Seçiniz'),
                        )) ?>

                        <?php echo bsFormDropdown('usageStatus', 'Kullanım Durumu', array('value' => $record->usageId,
                            'options' => prepareForSelect($this->appmodel->getUsageStatuses(), 'id', 'title', 'Seçiniz'),
                        )) ?>
                        <?php echo bsFormDropdown('krediuygun', 'Krediye Uygunluk', array(
                            'options' => array('' => 'Seçiniz', 'Evet' => 'Evet', 'Hayır' => 'Hayır'),
                        )) ?>
                    </div>

                    <div id="arsa" class="<?= $record->estateKindId != 3 ? 'hide' : '' ?>">
<!--                        --><?php //echo bsFormDropdown('imar', 'İmar Durumu', array(
//                            'options' => prepareForSelect($this->appmodel->getImar(), 'id', 'title'),
//                        )) ?>

                        <?php echo bsFormText('ada', 'Ada No', array('value' => $record->ada,)) ?>

                        <?php echo bsFormText('parsel', 'Parsel No', array('value' => $record->parsel,)) ?>

                        <?php echo bsFormText('pafta', 'Pafta No', array('value' => $record->pafta,)) ?>

                        <?php echo bsFormDropdown('kaks', 'Kaks (Emsal)', array(
                            'options' => prepareForSelect($this->appmodel->getKaks(), 'id', 'title'),
                            'value' => $record->kaks,
                        )) ?>

                        <?php echo bsFormDropdown('gabari', 'Gabari', array(
                            'options' => prepareForSelect($this->appmodel->getGabari(), 'id', 'title'),
                            'value' => $record->gabari,
                        )) ?>

                        <?php echo bsFormDropdown('tapu', 'Tapu Durumu', array(
                            'options' => prepareForSelect($this->appmodel->getTapu(), 'id', 'title'),
                            'value' => $record->tapu,
                        )) ?>

                        <?php echo bsFormDropdown('kat', 'Kat Karşılığı', array(
                            'options' => array(
                                '1' => 'Evet',
                                '2' => 'Hayır'
                            ),
                            'value' => $record->kat,
                        )) ?>

                        <?php echo bsFormDropdown('kredi', 'Krediye Uygunluk', array(
                            'options' => array(
                                '3' => 'Bilinmiyor',
                                '1' => 'Evet',
                                '2' => 'Hayır',
                            ),
                            'value' => $record->kredi,
                        )) ?>

                        <?php echo bsFormDropdown('takas', 'Takaslı', array(
                            'options' => array(
                                '3' => 'Bilinmiyor',
                                '1' => 'Evet',
                                '2' => 'Hayır',
                            ),
                            'value' => $record->takas,
                        )) ?>

                    </div>

                </div>
            </div>
        </div>


        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-commenting"></i> Detaylar
                </div>
                <div class="panel-body">
                    <?php echo bsFormText('adresss', 'Açık Adres', array('value' => $record->adresss)) ?>
                    <?php echo bsFormTextarea('metaKeywords', 'Keywords', array('value' => $record->metaKeywords, 'class' => 'hidden')) ?>
                    <?php echo bsFormEditor('detail', 'Detay', array('value' => $record->detail)) ?>
                </div>

                <div class="panel-footer">
                    <button class="btn btn-success" type="submit">Kaydet</button>
                    <a class="btn btn-default" href="<?php echo $this->module ?>/records">Vazgeç</a>
                </div>
            </div>
        </div>
    </form>
</div>

