
<div class="row">
    <form action="" method="post" enctype="multipart/form-data">
        <div class="col-md-4">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-key"></i> Emlak Özellikleri
                </div>

                <div class="panel-body">

                    <?php echo bsFormCheckbox('emergency', 'Acil İlan ?', array('value' => 1, 'returnValue' => $record->emergency)) ?>

                    <?php echo bsFormDropdown('cityId', 'Şehir', array(
                        'required' => true,
                        'value' => $record->cityId,
                        'options' => prepareForSelect($this->appmodel->getCities(), 'id', 'title', 'Seçiniz'),
                        'class' => 'city-list'
                    )) ?>
                    <?php echo bsFormDropdown('townId', 'İlçe', array(
                        'required' => true,
                        'value' => $record->townId,
                        'options' => prepareForSelect($this->appmodel->getTowns($record->cityId), 'id',
                            'title', 'Şehir Seçiniz'),
                        'class' => 'town-list'
                    )) ?>

                    <?php echo bsFormDropdown('localeId', 'Semt', array(
                        'required' => true,
                        'value' => $record->localeId,
                        'options' => prepareForSelect($this->appmodel->getDistricts($record->townId),
                            'id', 'title', 'İlçe Seçiniz'),
                        'class' => 'district-list'
                    )) ?>

                    <?php echo bsFormDropdown('statusId', 'Emlak Durumu', array(
                        'required' => true,
                        'value' => $record->statusId,
                        'options' => array('' => 'Seçiniz', '1' => 'Kiralık', '2' => 'Satılık'),
                    )) ?>

                    <?php echo bsFormDropdown('kind', 'Emlak Türü', array(
                        'required' => true,
                        'value' => $record->estateKindId,
                        'options' => prepareForSelect($this->appmodel->getKinds(), 'id', 'title', 'Seçiniz'),
                        'class' => 'kind-list'
                    )) ?>

                    <?php echo bsFormDropdown('type', 'Emlak Tipi', array(
                        'required' => true,
                        'value' => $record->estateTypeId,
                        'options' => prepareForSelect($this->appmodel->getTypes($record->estateKindId), 'id', 'title',
                            'Emlak Türü Seçiniz'),
                        'class' => 'type-list'
                    )) ?>

                    <?php echo bsFormDropdown('investor', 'Yatırımcı Fırsatları?', array(
                        'value' => $record->investor,
                        'options' => array('' => 'Seçiniz', '1' => 'Evet', '0' => 'Hayır'),
                    )) ?>

                    <?php echo bsFormDropdown('new_build', 'Yeni İnşaat', array(
                        'value' => $record->new_build,
                        'options' => array('' => 'Seçiniz', '1' => 'Evet', '0' => 'Hayır'),
                    )) ?>

                    <?php echo bsFormDropdown('month', 'Ayın Gayrimenkulü', array(
                        'value' => $record->month,
                        'options' => array('' => 'Seçiniz', '1' => 'Evet', '0' => 'Hayır'),
                    )) ?>

                    <?php echo bsFormDropdown('showcase', 'Ana Sayfada Göster', array(
                        'value' => $record->showcase,
                        'options' => array('' => 'Seçiniz', '1' => 'Evet', '0' => 'Hayır'),
                    )) ?>

                </div>

            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-plus"></i> Diğer Özellikler
                </div>

                <div class="panel-body">
                    <div class="form-group">
                        <label>Emlak Özellikleri</label>
                        <?php echo form_multiselect('properties[]', prepareForSelect($this->appmodel->getProperties(), 'id', 'title'), set_value('properties[]', $this->appmodel->getPropertyRelations($record)), 'class="form-control" id="properties" size="15"'); ?>
                        <div class="help-block"><kbd>Ctrl</kbd> tuşuna basılı tutarak birden fazla kategori seçebilirsiniz.</div>
                    </div>

                    <?= bsFormText('map', 'Google Map Koordinatları', array('value' => $record->map)) ?>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <?php echo $this->utils->alert(); ?>

            <div class="panel panel-default">
                <div class="panel-heading"><i class="fa fa-edit"></i> Kayıt Düzenle</div>
                <div class="panel-body">

                    <?php echo bsFormText('estateNo', 'İlan No', array('required' => true, 'value' => $record->estateNo)) ?>

                    <?php echo bsFormText('title', 'Başlık', array('required' => true, 'value' => $record->title)) ?>

                    <?php echo bsFormText('estateNo', 'İlan No', array('required' => true, 'value' => $record->estateNo)) ?>
                    <p class="help-block">
                        Lütfen gerekmedikçe değiştirmeyiniz.
                    </p>
                    <?php echo bsFormSlug('slug', 'Slug', array('required' => true, 'value' => $record->slug)) ?>

                    <?php echo bsFormImage('image', 'Resim', array('value' => $record->image,
                        'path' => '../public/upload/ilanlar/')) ?>

                    <?php echo bsFormText('video', 'Youtube Video Linki', array('value' => $record->video,)) ?>

                    <?php echo bsFormText('price', 'Fiyat', array('value' => $record->price,'required' => true,
                        'class' => 'numeric')) ?>

                    <?php echo bsFormText('depozito', 'Depozito', array('class' => 'numeric',
                        'value' => $record->depozito)) ?>

                    <?php echo bsFormText('aidat', 'Aidat', array(
                        'class' => 'numeric',
                        'value' => $record->aidat)) ?>

                    <div class="">
                        <?= bsFormText('kiraGetirisi', "Kira Getirisi", array('class' => 'numeric',
                            'value' => $record->kiraGetirisi)) ?>
                    </div>

                    <?php echo bsFormText('squareMeter', 'Metre Kare (m2)', array('value' => $record->squareMeter,
                        'required' => true, 'class' => 'numeric')) ?>

                    <div id="dukkan" class="<?= $record->estateKindId == 2 ? '' : 'hide' ?>">
                        <?= bsFormText('cepheGen', "Cephe Genişliği", array('value' => $record->cepheGen,
                            'class' => 'numeric')) ?>

                        <?= bsFormText('tavanYuk', "Tavan Yüksekliği", array('value' => $record->tavanYuk,
                            'class' => 'numeric')) ?>

                        <?= bsFormText('bolumSay', "Bölüm Sayısı", array('value' => $record->bolumSay,
                            'class' => 'numeric')) ?>
                    </div>

                    <div id="apart" class="<?= $record->estateKindId == 3 ? 'hide' : '' ?>">

                        <?php echo bsFormDropdown('roomId', 'Oda Sayısı', array('value' => $record->roomId,
                            'options' => prepareForSelect($this->appmodel->getRooms(), 'id', 'title', 'Seçiniz'),
                        )) ?>
                        <p class="help-block">Bina için lütfen Bina seçeneğini seçiniz.</p>

                        <?php echo bsFormText('bathCount', 'Banyo Sayısı', array('value' => $record->bathCount, 'class' => 'numeric')) ?>

                        <?php echo bsFormText('floorCount', 'Binadaki Kat Sayısı', array('value' => $record->floorCount,)
                        ) ?>

                        <?php echo bsFormDropdown('floor', 'Bulunduğu Kat', array('value' => $record->floorId,
                            'options' => prepareForSelect($this->appmodel->getFloors(), 'id', 'title', 'Seçiniz'),
                        )) ?>

                        <?php echo bsFormDropdown('buildingAge', 'Binanın Yaşı', array('value' => $record->ageId,
                            'options' => prepareForSelect($this->appmodel->getBuildingAges(), 'id', 'title', 'Seçiniz'),
                        )) ?>

                        <?php echo bsFormDropdown('heating', 'Isınma Şekli', array('value' => $record->heatId,
                            'options' => prepareForSelect($this->appmodel->getHeatings(), 'id', 'title', 'Seçiniz'),
                        )) ?>

                        <?php echo bsFormDropdown('usageStatus', 'Kullanım Durumu', array('value' => $record->usageId,
                            'options' => prepareForSelect($this->appmodel->getUsageStatuses(), 'id', 'title', 'Seçiniz'),
                        )) ?>

                    </div>


                    <div id="arsa" class="<?= $record->estateKindId != 3 ? 'hide' : '' ?>">

<!--                        --><?php //echo bsFormDropdown('imar', 'İmar Durumu', array(
//                            'options' => prepareForSelect($this->appmodel->getImar(), 'id', 'title'),
//                        )) ?>

                        <?php echo bsFormText('ada', 'Ada No', array('value' => $record->ada,)) ?>

                        <?php echo bsFormText('parsel', 'Parsel No', array('value' => $record->parsel,)) ?>

                        <?php echo bsFormText('pafta', 'Pafta No', array('value' => $record->pafta,)) ?>

                        <?php echo bsFormDropdown('kaks', 'Kaks (Emsal)', array(
                            'options' => prepareForSelect($this->appmodel->getKaks(), 'id', 'title'),
                            'value' => $record->kaks,
                        )) ?>

                        <?php echo bsFormDropdown('gabari', 'Gabari', array(
                            'options' => prepareForSelect($this->appmodel->getGabari(), 'id', 'title'),
                            'value' => $record->gabari,
                        )) ?>

                        <?php echo bsFormDropdown('tapu', 'Tapu Durumu', array(
                            'options' => prepareForSelect($this->appmodel->getTapu(), 'id', 'title'),
                            'value' => $record->tapu,
                        )) ?>

                        <?php echo bsFormDropdown('kat', 'Kat Karşılığı', array(
                            'options' => array(
                                '1' => 'Evet',
                                '2' => 'Hayır'
                            ),
                            'value' => $record->kat,
                        )) ?>

                        <?php echo bsFormDropdown('kredi', 'Krediye Uygunluk', array(
                            'options' => array(
                                '3' => 'Bilinmiyor',
                                '1' => 'Evet',
                                '2' => 'Hayır',
                            ),
                            'value' => $record->kredi,
                        )) ?>

                        <?php echo bsFormDropdown('takas', 'Takaslı', array(
                            'options' => array(
                                '3' => 'Bilinmiyor',
                                '1' => 'Evet',
                                '2' => 'Hayır',
                            ),
                            'value' => $record->takas,
                        )) ?>

                    </div>

                </div>
            </div>
        </div>


        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-commenting"></i> Detaylar
                </div>
                <div class="panel-body">
                    <?php echo bsFormText('metaTitle', 'Title', array('value' => $record->metaTitle)) ?>
                    <p class="help-block">
                        Temel anlamda paylaşımlar, seo, google arama sonuçlarında gösterilecek olan başlıktır.
                    </p>
                    <?php echo bsFormTextarea('metaDescription', 'Description', array('value' => $record->metaDescription)) ?>
                    <p class="help-block">
                        Arama motorlarında görünmesi için web sayfasının tanımının yapıldığı alandır.
                    </p>
                    <?php echo bsFormTextarea('metaKeywords', 'Keywords', array('value' => $record->metaKeywords)) ?>
                    <p class="help-block">
                        Arama motorlarında aramalarda yardımcı olması açısından kullanılan anahtar kelimelerdir.
                        Kelimeleri virgül ile ayırınız.
                    </p>
                    <?php echo bsFormEditor('detail', 'Detay', array('value' => $record->detail)) ?>
                </div>

                <div class="panel-footer">
                    <button class="btn btn-success" type="submit">Kaydet</button>
                    <a class="btn btn-default" href="<?php echo $this->module ?>/records">Vazgeç</a>
                </div>
            </div>
        </div>
    </form>
</div>

