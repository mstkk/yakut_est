
<div class="row">
    <form action="" method="post" enctype="multipart/form-data">
        <div class="col-md-8">
            <?php echo $this->utils->alert(); ?>

            <div class="panel panel-default">
                <div class="panel-heading"><i class="fa fa-edit"></i> Kayıt Aktar</div>
                <div class="panel-body">


                    <?php echo bsFormText('no', 'No', array('required' => true)) ?>
                    <?php echo bsFormDropdown('city', 'Şehir', array(
                        'required' => true,
                        'value' => $record->cityId,
                        'options' => prepareForSelect($this->appmodel->getCities(), 'id', 'title', 'Seçiniz'),
                        'class' => 'city-list'
                    )) ?>
                    <?php echo bsFormDropdown('town', 'İlçe', array(
                        'required' => true,
                        'value' => $record->townId,
                        'options' => prepareForSelect($this->appmodel->getTowns($record->cityId), 'id', 'title', 'Şehir Seçiniz'),
                        'class' => 'town-list'
                    )) ?>

                    <?php echo bsFormDropdown('district', 'Semt', array(
                        'required' => true,
                        'value' => $record->districtId,
                        'options' => prepareForSelect($this->appmodel->getDistricts($record->townId), 'id', 'title', 'İlçe Seçiniz'),
                        'class' => 'district-list'
                    )) ?>

                    <?php echo bsFormDropdown('status', 'Emlak Durumu', array(
                        'required' => true,
                        'value' => $record->estateStatus,
                        'options' => array('' => 'Seçiniz', 'Kiralık' => 'Kiralık', 'Satılık' => 'Satılık'),
                    )) ?>

                    <?php echo bsFormDropdown('kind', 'Emlak Türü', array(
                        'required' => true,
                        'value' => $record->kindId,
                        'options' => prepareForSelect($this->appmodel->getKinds(), 'id', 'title', 'Seçiniz'),
                        'class' => 'kind-list'
                    )) ?>

                    <?php echo bsFormDropdown('type', 'Emlak Tipi', array(
                        'required' => true,
                        'value' => $record->typeId,
                        'options' => prepareForSelect($this->appmodel->getTypes($record->kindId), 'id', 'title', 'Emlak Türü Seçiniz'),
                        'class' => 'type-list'
                    )) ?>

                    <?php echo bsFormText('price', 'Fiyat', array('required' => true, 'value' => $record->price)) ?>

                    <?php echo bsFormImage('image', 'Resim', array('value' => $record->image, 'path' => '../public/upload/ilanlar/insert/')) ?>
                    <?php echo bsFormEditor('description', 'Açıklama', array('value' => $record->description)) ?>

                    <?php echo bsFormText('room', 'Oda Sayısı', array('value' => $record->room)) ?>
                    <?php echo bsFormText('bath', 'Banyo Sayısı', array('value' => $record->bath)) ?>
                    <?php echo bsFormText('squaremeter', 'Metrekare (m2)', array('value' => $record->squaremeter)) ?>
                    <?php echo bsFormText('storey', 'Bulunduğu Kat', array('value' => $record->storey)) ?>
                    <?php echo bsFormText('storeyCount', 'Binadaki Kat Sayısı', array('value' => $record->storeyCount)) ?>
                    <?php echo bsFormText('buildingAge', 'Binanın Yaşı', array('value' => $record->buildingAge)) ?>

                    <?php echo bsFormDropdown('heating', 'Isınma Şekli', array(
                        'value' => $record->heatingId,
                        'options' => prepareForSelect($this->appmodel->getHeatings(), 'id', 'title', 'Seçiniz'),
                    )) ?>

                    <?php echo bsFormText('usageStatus', 'Kullanım Durumu', array('value' => $record->usageStatus)) ?>
                    <?php echo bsFormText('mapCoordinate', 'Google Map Kordinatlatı') ?>


                </div>
                <div class="panel-footer">
<!--                    <button class="btn btn-success" type="submit">Aktar</button>-->
                    <a class="btn btn-default" href="<?php echo $this->module ?>/records">Geri Dön</a>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading"><i class="fa fa-plus-square"></i> Bilgiler</div>

                <div class="panel-body">
                    <div class="form-group">
                        <label>Emlak Özellikleri</label>
                        <?php echo form_multiselect('properties[]', prepareForSelect($this->appmodel->getProperties(), 'id', 'title'), set_value('properties[]', json_decode($record->properties)), 'class="form-control" id="properties" size="10"'); ?>
                        <div class="help-block"><kbd>Ctrl</kbd> tuşuna basılı tutarak birden fazla kategori seçebilirsiniz.</div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>

