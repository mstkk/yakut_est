
<?php echo $this->utils->alert(); ?>

<div class="panel panel-default">
    <div class="panel-heading"><i class="fa fa-table"></i> İlçeler</div>
    <div class="panel-toolbar clearfix">
        <div class="row">
            <div class="col-md-4">
                <?php if ($this->permission('type-delete')): ?>
                    <a class="btn btn-sm btn-info checkall" data-toggle="button"><i class="fa fa-check-square-o"></i> Hepsini Seç</a>
                    <a class="btn btn-sm btn-danger deleteall" href="<?php echo $this->module ?>/typeDelete"><i class="fa fa-trash-o"></i></a>
                <?php endif; ?>
                <?php if ($this->permission('type-insert')): ?>
                    <a class="btn btn-sm btn-success" href="<?php echo $this->module ?>/typeInsert/<?php echo $parent->id ?>"><i class="fa fa-plus"></i> Yeni Kayıt</a>
                <?php endif; ?>

                <a id="order-update" class="btn btn-sm btn-info hide" href="<?php echo $this->module ?>/typeOrder"><i class="fa fa-check-square"></i> Sırayı Güncelle</a>
            </div>

        </div>
    </div>
    <table class="table table-bordered table-hover">
        <thead>
        <tr>
            <th width="40" class="text-center"><i class="fa fa-ellipsis-v"></i></th>
            <th width="50">#</th>
            <th>Başlık</th>
            <th width="100" class="text-center">Sıra</th>
            <th width="100" class="text-right">İşlem</th>
        </tr>
        </thead>
        <tbody class="sortable">
        <?php foreach ($records as $item): ?>
            <tr data-id="<?php echo $item->id ?>">
                <td class="text-center"><input type="checkbox" class="checkall-item" value="<?php echo $item->id ?>" /></td>
                <td><?php echo $item->id ?></td>
                <td><?php echo $item->title ?></td>
                <td class="text-center">
                    <div class="btn-group">
                        <a class="btn btn-xs btn-info disabled"><?php echo $item->order ?></a>
                        <?php if (! $this->input->get() || $this->input->get('page')): ?>
                            <a class="btn btn-xs btn-default sortable-handle"><i class="fa fa-arrows"></i></a>
                        <?php endif; ?>
                    </div>
                </td>
                <td class="text-right">
                    <?php if ($this->permission('type-update')): ?>
                        <a class="btn btn-xs btn-primary" href="<?php echo $this->module ?>/typeUpdate/<?php echo $item->id ?>"><i class="fa fa-edit"></i></a>
                    <?php endif; ?>
                    <?php if ($this->permission('type-delete')): ?>
                        <a class="btn btn-xs btn-danger confirm-delete" href="<?php echo $this->module ?>/typeDelete/<?php echo $item->id ?>"><i class="fa fa-trash-o"></i></a>
                    <?php endif; ?>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>

    <?php if (! empty($pagination)): ?>
        <div class="panel-footer">
            <?php echo $pagination ?>
        </div>
    <?php endif; ?>
</div>