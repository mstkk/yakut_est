-- ----------------------------
-- Table structure for `advertisings`
-- ----------------------------
CREATE TABLE `advertisings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `consultantId` int(10) unsigned NOT NULL,
  `cityId` int(10) unsigned NOT NULL,
  `townId` int(10) unsigned NOT NULL,
  `localId` int(10) unsigned NOT NULL,
  `estateNo` varchar(100) NOT NULL,
  `estateKindId` int(10) unsigned NOT NULL,
  `estateTypeId` int(10) unsigned NOT NULL,
  `title` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `video` varchar(255) NOT NULL,
  `detail` text NOT NULL,
  `price` DOUBLE(10,2) NOT NULL,
  `squareMeter` int(10) unsigned NOT NULL,
  `roomId` int(10) unsigned NOT NULL,
  `roomCount` int(5) unsigned NOT NULL,
  `bathCount` int(5) unsigned NOT NULL,
  `floorId` int(10) unsigned NOT NULL,
  `floorName` varchar(255) NOT NULL,
  `floorCount` int(5) unsigned NOT NULL,
  `ageId` int(10) unsigned NOT NULL,
  `heatId` int(10) unsigned NOT NULL,
  `statusId` int(10) unsigned NOT NULL,
  `date` DATETIME NOT NULL ,
  `updateDate` DATETIME NOT NULL ,
  `views` int(10) unsigned NOT NULL,
  `metaTitle` varchar(255) DEFAULT NULL,
  `metaDescription` text,
  `metaKeywords` text,
  `order` int(10) unsigned NOT NULL DEFAULT '0',
  `language` varchar(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for `advertising_images`
-- ----------------------------
CREATE TABLE `advertising_images` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `advertisingId` int(10) unsigned NOT NULL,
  `image` varchar(255) NOT NULL,
  `order` int(10) unsigned NOT NULL DEFAULT '0',
  `language` varchar(5) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_adver_parent` (`advertisingId`),
  CONSTRAINT `fk_contents_parentId` FOREIGN KEY (`advertisingId`) REFERENCES `advertisings` (`id`) ON UPDATE CASCADE
  ON DELETE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

