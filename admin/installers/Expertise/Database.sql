-- ----------------------------
-- Table structure for `expertise`
-- ----------------------------
CREATE TABLE `expertise` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `t_sube` varchar(255) NULL,
  `t_fullname` varchar(255) NOT NULL,
  `t_phone` varchar(255) NOT NULL,
  `t_email` varchar(255) NOT NULL,
  `g_city` varchar(255) NOT NULL,
  `g_town` varchar(255) NOT NULL,
  `g_address` text NULL,
  `g_pafta` varchar(255) NULL,
  `g_ada` varchar(255) NULL,
  `g_parsel` varchar(255) NULL,
  `k_fullname` varchar(255) NOT NULL,
  `k_phone` varchar(255) NOT NULL,
  `m_fullname` varchar(255) NULL,
  `m_vergiD` varchar(255) NULL,
  `m_vergiNo` varchar(255) NULL,
  `m_address` text NULL,
  `m_comment` text NULL,
  `date` datetime NOT NULL,
  `ip` varchar(255) NOT NULL,
  `viewed` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


