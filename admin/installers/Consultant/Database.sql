-- ----------------------------
-- Table structure for `profiles`
-- ----------------------------
CREATE TABLE `profiles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userId` int(10) unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `phoneSt` varchar(255) NULL,
  `phoneMb` varchar(255) NULL,
  `address` varchar(255) NULL,
  `facebook` varchar(255) NULL,
  `twitter` varchar(255) NULL,
  `google` varchar(255) NULL,
  `order` int(10) NOT NULL,
  `language` varchar(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
