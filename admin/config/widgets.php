<?php

$config['widgets'] = array(
    array(
        'module' => 'news',
        'icon' => 'fa-text-width',
        'type' => 'app-full app-success',
    ),
    array(
        'module' => 'contact',
        'icon' => 'fa-envelope',
        'info' => 'Yeni',
        'where' => array('viewed' => 0)
    ),
    array(
        'type' => 'app-full',
        'class' => 'app-success',
        'module' => 'advertising',
        'icon'	=> 'fa-building-o',
        'label'	=> 'Kiralık Emlaklar',
        'where' => array('statusId' => '1'),
        'link' => 'advertising/records/?estateStatus=Kiralık',
        'info' => 'Kiralık',
    ),
    array(
        'type' => 'app-full',
        'class' => 'app-success',
        'module' => 'advertising',
        'icon'	=> 'fa-building-o',
        'label'	=> 'Satılık Emlaklar',
        'where' => array('statusId' => '2'),
        'link' => 'advertising/records/?estateStatus=Satılık',
        'info' => 'Satılık',
    ),
);
