<div class="row">
    <form action="" method="post" enctype="multipart/form-data">
        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-key"></i> Emlak Özellikleri
                </div>
<?php print_r($record) ?>

                <div class="panel-body">

                    <?php echo bsFormCheckbox('emergency', 'Acil İlan ?', array('value' => 1)) ?>

                    <?php echo bsFormDropdown('cityId', 'Şehir', array(
                        'required' => true,
                        'value' => $record->cityId,
                        'options' => prepareForSelect($this->appmodel->getCities(), 'id', 'title', 'Seçiniz'),
                        'class' => 'city-list'
                    )) ?>
                    <?php echo bsFormDropdown('townId', 'İlçe', array(
                        'required' => true,
                        'class' => 'town-list'
                    )) ?>

                    <?php echo bsFormDropdown('localeId', 'Semt', array(
                        'required' => true,
                        'class' => 'district-list'
                    )) ?>

                    <?php echo bsFormDropdown('statusId', 'Emlak Durumu', array(
                        'required' => true,
                        'options' => array('' => 'Seçiniz', '1' => 'Kiralık', '2' => 'Satılık'),
                    )) ?>

                    <?php echo bsFormDropdown('kind', 'Emlak Türü', array(
                        'required' => true,
                        'options' => prepareForSelect($this->appmodel->getKinds(), 'id', 'title', 'Seçiniz'),
                        'class' => 'kind-list',
                    )) ?>

                    <?php echo bsFormDropdown('type', 'Emlak Tipi', array(
                        'required' => true,
                        'options' => prepareForSelect($this->appmodel->getTypes($this->input->post('kind')), 'id', 'title', 'Emlak Türü Seçiniz'),
                        'class' => 'type-list'
                    )) ?>

                    <?php echo bsFormDropdown('investor', 'Yatırımcı Fırsatları?', array(
                        'options' => array('' => 'Seçiniz', '1' => 'Evet', '0' => 'Hayır'),
                        'class' => 'investor',
                    )) ?>

                    <?php echo bsFormDropdown('new_build', 'Yeni İnşaat', array(
                        'options' => array('' => 'Seçiniz', '1' => 'Evet', '0' => 'Hayır'),
                    )) ?>

                    <?php echo bsFormDropdown('month', 'Ayın Gayrimenkulü', array(
                        'options' => array('' => 'Seçiniz', '1' => 'Evet', '0' => 'Hayır'),
                    )) ?>

                    <?php echo bsFormDropdown('showcase', 'Ana Sayfada Göster', array(
                        'options' => array('' => 'Seçiniz', '1' => 'Evet', '0' => 'Hayır'),
                    )) ?>

                </div>

            </div>


            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-plus"></i> Diğer Özellikler
                </div>

                <div class="panel-body">

                    <div class="form-group">
                        <label>Emlak Özellikleri</label>
                        <?php echo form_multiselect('properties[]', prepareForSelect($this->appmodel->getProperties(), 'id', 'title'), set_value('properties[]'), 'class="form-control" id="properties" size="15"'); ?>
                        <div class="help-block"><kbd>Ctrl</kbd> tuşuna basılı tutarak birden fazla kategori seçebilirsiniz.</div>
                    </div>

                    <?= bsFormText('map', 'Google Map Koordinatları') ?>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <?php echo $this->utils->alert(); ?>

            <div class="panel panel-default">
                <div class="panel-heading"><i class="fa fa-plus-square"></i> Yeni Kayıt Ekle</div>
                <div class="panel-body">

                    <?php echo bsFormText('estateNo', 'İlan No', array('required' => true)) ?>
                    <?php echo bsFormText('title', 'İlan Başlığı', array('required' => true)) ?>

                    <?php echo bsFormSlug('slug', 'Slug', array('checked' => true)) ?>

                    <?php echo bsFormImage('image', 'Resim') ?>

                    <?php echo bsFormText('video', 'Youtube Video Linki', array()) ?>

                    <?php echo bsFormText('price', 'Fiyat', array('required' => true,
                        'class' => 'numeric')) ?>

                    <?php echo bsFormText('depozito', 'Depozito', array('class' => 'numeric')) ?>

                    <?php echo bsFormText('aidat', 'Aidat', array('required' => true,
                        'class' => 'numeric')) ?>

                    <div>
                        <?= bsFormText('kiraGetirisi', "Kira Getirisi", array('class' => 'numeric')) ?>
                    </div>

                    <?php echo bsFormText('squareMeter', 'Metre Kare (m2)', array('required' => true, 'class' =>
                        'numeric')) ?>

                    <div id="apart">
                    <?php echo bsFormDropdown('roomId', 'Oda Sayısı', array(
                        'options' => prepareForSelect($this->appmodel->getRooms(), 'id', 'title', 'Seçiniz'),
                    )) ?>
                        <p class="help-block">Bina için lütfen Bina seçeneğini seçiniz.</p>

                    <?php echo bsFormText('bathCount', 'Banyo Sayısı', array('class' =>
                        'numeric')) ?>

                    <?php echo bsFormText('floorCount', 'Binadaki Kat Sayısı') ?>

                    <?php echo bsFormDropdown('floor', 'Bulunduğu Kat', array(
                        'options' => prepareForSelect($this->appmodel->getFloors(), 'id', 'title', 'Seçiniz'),
                    )) ?>

                    <?php echo bsFormDropdown('buildingAge', 'Binanın Yaşı', array(
                        'options' => prepareForSelect($this->appmodel->getBuildingAges(), 'id', 'title', 'Seçiniz'),
                    )) ?>

                    <?php echo bsFormDropdown('heating', 'Isınma Şekli', array(
                        'options' => prepareForSelect($this->appmodel->getHeatings(), 'id', 'title', 'Seçiniz'),
                    )) ?>

                    <?php echo bsFormDropdown('usageStatus', 'Kullanım Durumu', array(
                        'options' => prepareForSelect($this->appmodel->getUsageStatuses(), 'id', 'title', 'Seçiniz'),
                    )) ?>
                    </div>

                    <div id="dukkan" class="hide">
                        <?= bsFormText('cepheGen', "Cephe Genişliği", array('class' => 'numeric')) ?>

                        <?= bsFormText('tavanYuk', "Tavan Yüksekliği", array('class' => 'numeric')) ?>

                        <?= bsFormText('bolumSay', "Bölüm Sayısı", array('class' => 'numeric')) ?>
                    </div>

                    <div id="arsa" class="hide">

<!--                        --><?php //echo bsFormDropdown('imar', 'İmar Durumu', array(
//                            'options' => prepareForSelect($this->appmodel->getImar(), 'id', 'title'),
//                        )) ?>
                        <?php echo bsFormText('ada', 'Ada No', array()) ?>
                        <?php echo bsFormText('parsel', 'Parsel No', array()) ?>
                        <?php echo bsFormText('pafta', 'Pafta No', array()) ?>
                        <?php echo bsFormDropdown('kaks', 'Kaks (Emsal)', array(
                            'options' => prepareForSelect($this->appmodel->getKaks(), 'id', 'title'),
                        )) ?>
                        <?php echo bsFormDropdown('gabari', 'Gabari', array(
                            'options' => prepareForSelect($this->appmodel->getGabari(), 'id', 'title'),
                        )) ?>
                        <?php echo bsFormDropdown('tapu', 'Tapu Durumu', array(
                            'options' => prepareForSelect($this->appmodel->getTapu(), 'id', 'title'),
                        )) ?>
                        <?php echo bsFormDropdown('kat', 'Kat Karşılığı', array(
                            'options' => array(
                                '1' => 'Evet',
                                '2' => 'Hayır'
                            ),
                        )) ?>
                        <?php echo bsFormDropdown('kredi', 'Krediye Uygunluk', array(
                            'options' => array(
                                '3' => 'Bilinmiyor',
                                '1' => 'Evet',
                                '2' => 'Hayır',
                            ),
                        )) ?>
                        <?php echo bsFormDropdown('takas', 'Takaslı', array(
                            'options' => array(
                                '3' => 'Bilinmiyor',
                                '1' => 'Evet',
                                '2' => 'Hayır',
                            ),
                        )) ?>
                    </div>




                </div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-commenting"></i> Detaylar
                </div>
                <div class="panel-body">

                    <?php echo bsFormText('metaTitle', 'Meta Title') ?>
                    <p class="help-block">
                        Temel anlamda paylaşımlar, seo, google arama sonuçlarında gösterilecek olan başlıktır.
                    </p>
                    <?php echo bsFormTextarea('metaDescription', 'Meta Description') ?>
                    <p class="help-block">
                        Arama motorlarında görünmesi için web sayfasının tanımının yapıldığı alandır.
                    </p>
                    <?php echo bsFormTextarea('metaKeywords', 'Meta Keywords') ?>
                    <p class="help-block">
                        Arama motorlarında aramalarda yardımcı olması açısından kullanılan anahtar kelimelerdir.
                        Kelimeleri virgül ile ayırınız.
                    </p>
                    <?php echo bsFormEditor('detail', 'Detaylar', array('required' => true)) ?>
                </div>


                <div class="panel-footer">
                    <button class="btn btn-success" type="submit">Kaydet</button>
                    <button class="btn btn-success" type="submit" name="redirect" value="<?php echo $this->module ?>/records">Kaydet ve Listeye Dön</button>
                    <a class="btn btn-default" href="<?php echo $this->module ?>/records">Vazgeç</a>
                </div>
            </div>
        </div>

    </form>
</div>