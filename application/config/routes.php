<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "HomeController/index";

$route['ilanlar/yazdir/([0-9]+)'] = 'AdvertisingController/yazdir/$1';
$route['ilanlar/send-mail/([0-9]+)'] = 'AdvertisingController/sendMail/$1';
$route['ilanlar/[a-zA-Z0-9_-]+/([0-9]+)'] = 'AdvertisingController/view/$1';
$route['ilanlar/search'] = 'AdvertisingController/search';
$route['ilanlar/towns'] = 'AdvertisingController/towns';
$route['ilanlar/districts'] = 'AdvertisingController/districts';
$route['ilanlar'] = 'AdvertisingController/index';
$route['kategori/[a-zA-Z0-9_-]+/([0-9]+)'] = 'AdvertisingController/index/$1';
$route['iletisim'] = 'ContactController/index';
$route['icerik/[a-zA-Z0-9_-]+/([0-9]+)'] = 'ContentController/view/$1';
$route['haber/[a-zA-Z0-9_-]+/([0-9]+)'] = 'NewsController/view/$1';
$route['haber'] = 'NewsController/index';
//$route['danismanlar/[a-zA-Z0-9_-]+/([0-9]+)'] = 'ConsultantController/view/$1';
//$route['danismanlar'] = 'ConsultantController/index';
$route['ekspertiz'] = 'ExpertiseController/index';
$route['emlak-ekle'] = 'AdvertisingController/insert';

$route['ajax/([a-zA-Z0-9_-]+)'] = 'AjaxController/$1';
$route['biz-sizi-arayalim'] = 'CallyouController/index';
$route['mail-ekle'] = 'MaillistController/index';
