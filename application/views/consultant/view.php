<section class="container relative" style="top: -23px;">
    <div class="row">
        <!-- banner -->
        <div class="col-md-8 col-sm-12 col-xs-12">
            <div class="row">
                <div class="col-md-12">
                    <h3 class="caption1"><?= $consultants->name ?> İlanları</h3>
                </div>
                <div class="col-md-12">
                    <div class="consultants">
                        <div class="row">
                            <div class="col-md-4 col-sm-5">
                                <img src="<?= uploadPath($consultants->image, 'consultant') ?>" alt="<?= $consultants->name ?>" />
                            </div>
                            <div class="col-md-8 col-sm-7">
                                <div class="consInformation">
                                    <h3><?= $consultants->name ?></h3>
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-sm-3 control-label ">Ünvanı</label>
                                            <div class="col-sm-9">
                                                <?= $consultants->degree ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-sm-3 control-label ">Telefon No</label>
                                            <div class="col-sm-9">
                                                <?= $consultants->phone ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-sm-3 control-label ">Cep Telefonu</label>
                                            <div class="col-sm-9">
                                                <?= $consultants->mobile ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-sm-3 control-label ">Email</label>
                                            <div class="col-sm-9">
                                                <?= $consultants->email ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-sm-3 control-label ">Adres</label>
                                            <div class="col-sm-9">
                                                <?= $consultants->address ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <h3>
                            Tüm Portfoyü
                        </h3>
                        <div class="consBg">
                            <div class="row">
                                <?php if (! empty($visions->adverts)): ?>
                                    <?php foreach ($visions->adverts as $vision): ?>
                                        <!-- adverd-item -->
                                        <div class="adverd-item col-md-4 col-sm-6 col-xs-12">
                                            <a href="<?= clink(array('@advertising', $vision->slug, $vision->id)) ?>">
                                                <img src="<?= uploadPath($vision->image, 'ilanlar') ?>"
                                                     class="img-responsive" alt=""/>
                                                <h5><?= $vision->title ?></h5>
                                            <span>
                                                <i class="fa fa-map-marker"></i> <?= mb_strtoupper($vision->cityTitle, "UTF-8") ?> -
                                                <?= mb_strtoupper($vision->townTitle, "UTF-8") ?>
                                            </span>

                                                <div class="clear"></div>
                                                <!-- room-size -->
                                                <div class="room-size pull-left">
                                                    <span><?= $vision->roomTitle ?></span>
                                                </div>
                                                <!-- room-size -->
                                                <!-- room-size -->
                                                <div class="room-type pull-left bg-<?= $vision->statusId == 1 ? 'blue' : 'yellow' ?>">
                                                    <span><?= $vision->statusId == 1 ? 'KİRALIK' : 'SATILIK' ?></span>
                                                </div>
                                                <!-- room-size -->
                                                <!-- room-mon -->
                                                <div class="room-mon pull-left">
                                                    <?= money($vision->price, true) ?> TL
                                                </div>
                                                <!-- room-mon -->
                                                <div class="clear"></div>
                                            </a>
                                        </div>
                                        <!-- adverd-item -->
                                    <?php endforeach; ?>
                                <?php endif; ?>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- banner -->
        <!-- filter -->
        <div class="col-md-4 col-sm-12 col-xs-12">
            <?php $this->view('parts/filter') ?>

            <div class="col-md-12">
                <h3 class="caption1">ACİL İLANLAR</h3>
            </div>
            <?php if (! empty($emergencies)): ?>
                <?php $this->view('parts/emergency') ?>
            <?php endif; ?>

        </div>
        <!-- filter -->
    </div>
</section>
