<section class="container relative" style="top: -23px;">
    <div class="row">
        <!-- banner -->
        <div class="col-md-8 col-sm-12 col-xs-12">
            <div class="col-md-12">
                <h3 class="caption1">DANIŞMANLARIMIZ</h3>
            </div>
            <div class="row">
                <?php if (! empty($consultants)): ?>
                    <?php foreach ($consultants as $consultant): ?>
                        <!-- adverd-item -->
                        <div class="adverd-item col-md-4 col-sm-4 col-xs-12">
                            <a href="<?= clink(array('@consultant', $consultant->slug, $consultant->id)) ?>">
                                <img src="<?= uploadPath($consultant->image, 'consultant') ?>" class="img-responsive"
                                     alt="<?= $consultant->name ?>"/>
                                <h5><?= $consultant->name ?></h5>
                                <span>
                                    <?= $consultant->degree ?>
                                </span>

                                <div class="clear"></div>
                            </a>
                        </div>
                        <!-- adverd-item -->
                    <?php endforeach; ?>
                <?php endif; ?>

            </div>
        </div>
        <!-- banner -->

        <!-- filter -->
        <div class="col-md-4 col-sm-12 col-xs-12">
            <?php $this->view('parts/filter') ?>

            <!--      Acil İlanlar      -->
            <div class="col-md-12" style="margin-top: 15px;">
                <h3 class="caption1">ACİL İLANLAR</h3>
            </div>
            <?php if (! empty($emergencies)): ?>
                <?php $this->view('parts/emergency') ?>
            <?php endif; ?>
        </div>
        <!-- filter -->
    </div>
</section>
