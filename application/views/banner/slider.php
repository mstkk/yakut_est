<div id="layerslider" style="width: 100%; height: 700px;">
    <?php foreach ($this->banner->all() as $banner): ?>
        <div class="ls-slide" data-ls="slidedelay: <?php echo $banner->delay ?>; <?php echo $banner->transition ?>">

            <?php if (! empty($banner->image)): ?>
			<img class="ls-bg" src="public/upload/banner/<?php echo $banner->image ?>" alt="<?php echo $banner->title ?>" />
                <?php
                $top = 320;
                $delayin = 200;
                ?>

                <?php if (! empty($banner->title)): ?>
                    <div class="ls-l text" style="background:transparent; top: <?php echo $top ?>px; left: 0;" data-ls="delayin: <?php echo $delayin ?>; offsetxin: -150; offsetxout: 150;">
                        <span style="background: rgba(25, 106, 173, 0.48); padding: 15px; border-radius: 2px; color: white; font-size: 18px;">
                            <span>
                                <strong><?= $banner->title ?></strong>
                            </span>
                        </span>
                    </div>
                   <?php if (! empty($banner->link)): ?> <a href="<?= $banner->link ?>" class="ls-link"></a><?php endif; ?>

                <?php endif; ?>
            <?php endif; ?>

<!--            --><?php //echo !empty($banner->link) ? '<a class="ls-link" href="'. $banner->link .'" title="'. $banner->title .'"></a>':'' ?>
        </div>

    <?php endforeach; ?>
</div>

