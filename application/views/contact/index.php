<section id="main">
    <div class="container">
        <!-- WRAPPER START -->
        <div class="col-sm-10 rightSide">
            <div class="col-sm-12 rightSideBack">
                <div class="row">
                    <div class="col-sm-6">
                        <h4 class="sayfaBaslik"><?php echo lang('contact-information') ?></h4>
                        <?php echo $this->module->arguments->detail ?>
                    </div>

                    <!-- HARİTA START-->
                    <div class="col-sm-6">

                        <h4 class="sayfaBaslik"><?php echo lang('contact-form') ?></h4>

                        <?= $this->site->alert(); ?>

                        <div class="row">

                            <form action="" method="post" accept-charset="utf-8">

                                <div class="col-sm-6">

                                    <div class="form-group">

                                        <input type="text" class="form-control" name="fullname" placeholder="Ad Soyad"

                                               required="required" value="<?php echo set_value('fullname') ?>">

                                    </div>

                                    <div class="form-group">

                                        <input type="email" class="form-control" name="email" placeholder="E-Posta"

                                               required="required" value="<?php echo set_value('email') ?>"/>

                                    </div>

                                    <div class="form-group">

                                        <input type="text" class="form-control mask-phone" name="phone"
                                               placeholder="Telefon"

                                               required="required" value="<?php echo set_value('phone') ?>"/>

                                    </div>

                                    <div class="form-group">

                                        <select name="subject" class="form-control" required>

                                            <option value="">Konu Seçiniz</option>

                                            <option value="Gayrimenkul Arıyorum">Gayrimenkul Arıyorum</option>

                                            <option value="Gayrimenkul Pazarlıyorum">Gayrimenkul Pazarlıyorum</option>

                                            <option value="Sizinle Çalışmak İstiyorum">Sizinle Çalışmak İstiyorum
                                            </option>

                                            <option value="Diğer">Diğer</option>

                                        </select>

                                    </div>

                                </div>

                                <div class="col-sm-6">

                                <textarea class="form-control" name="comment" placeholder="Mesajınız"

                                          style="min-height:178px; resize: none"

                                          required="required" rows="7"><?php echo set_value('comment') ?></textarea>

                                </div>

                                <div class="col-sm-12">

                                    <button type="submit" class="btn btn-primary btn-Search formMargin marginBottom15"

                                            style="width:100%"><?php echo lang('contact-send') ?></button>

                                </div>

                            </form>

                        </div>

                    </div>

                    <!--iletisim formu END-->

                </div>

                <!-- HARİTA END-->


                <!--divider START-->

                <div class="col-sm-12 colDivider"></div>

                <!--divider END-->


                <!--iletisim formu START-->

                <div class="col-xs-12">
                    <div id="googleMap" style="height: 300px;"></div>
                </div><!--row-->


            </div><!--col-sm-12-->


            <?php $this->view('parts/estate') ?>


        </div><!--col-sm-9-->

        <!-- WRAPPER END -->

                <?php $this->view('parts/leftSide') ?>

        </div>

    <?php if (! empty($this->module->arguments->googleMap)): ?>
        <section class="map">
            <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC5tsEc6YH4r7dOtYH16ddHKPWf75LyR50&libraries=places"></script>
            <script type="text/javascript">
                function initialize() {
                    var konum = new google.maps.LatLng(<?php echo $this->module->arguments->googleMap ?>);
                    var mapOptions = {
                        zoom: 17,
                        center: konum,
                        scrollwheel: false,
                        draggable : false,
                        mapTypeControl: true,
                        mapTypeControlOptions: {
                            position: google.maps.ControlPosition.LEFT_CENTER
                        },
                        zoomControl: true,
                        zoomControlOptions: {
                            position: google.maps.ControlPosition.RIGHT_CENTER
                        },
                        scaleControl: true,
                        streetViewControl: true,
                        streetViewControlOptions: {
                            position: google.maps.ControlPosition.LEFT_CENTER
                        },
                        mapTypeId: google.maps.MapTypeId.ROADMAP
                    };

                    var map = new google.maps.Map(document.getElementById('googleMap'), mapOptions);

                    var contentString = '<?php echo htmlspecialchars($this->module->arguments->googleMapText) ?>';

                    var infowindow = new google.maps.InfoWindow({
                        content: contentString
                    });

                    var marker = new google.maps.Marker({
                        position: konum,
                        map: map,
                        icon: 'public/img/marker.png',
                        title: '<?php htmlspecialchars($this->module->arguments->title) ?>'
                    });

                    if (marker.getAnimation() != null) {
                        marker.setAnimation(null);
                    } else {
                        marker.setAnimation(google.maps.Animation.DROP);
                    }

                    google.maps.event.addListener(marker, 'click', function() {
                        infowindow.open(map, marker);
                    });
                    console.log();
                }
                google.maps.event.addDomListener(window, 'load', initialize);
            </script>
        </section>
    <?php endif; ?>
    </div><!--container-->

</section>