<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Emlak</title>
    <meta http-equiv="Content-Language" content="tr" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <link href='https://fonts.googleapis.com/css?family=Oswald:400,700,300&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="public/plugin/bootstrap/css/bootstrap.min.css" />
    <link rel="stylesheet" href="public/css/main.css">

    <style type="text/css">
        a {color: #36f; text-decoration: none; outline: 0;}
        table {font: 12px/20px Verdana, sans-serif;}
        td {padding: 8px 10px; border-bottom: 1px dashed #ccc;}
        .label {font-weight: bold; width: 100px;}
        .passive {display: none !important;}
    </style>
    <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
    <script type="text/javascript">
        $(window).load(function () {
            window.print();
        });
    </script>
</head>
<body>

<center>
    <table cellspacing="0" cellpadding="0" width="700" style="text-align: left; color: #444; border: 1px solid #ddd;">
        <thead>
        <tr>
            <td colspan="2" align="center"><h3>Hakim 35 Gayrimenkul</h3> <br><br> <u><i><?= base_url().clink(array('@advertising', $advertising->slug, $advertising->id)) ?></i></u></td>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td class="font700">İlan Adı</td>
            <td><?= $advertising->title ?></td>
        </tr>
        <tr>
            <td class="font700" colspan="2">
                <?= $advertising->localeTitle ?>, <?= $advertising->townTitle ?>, <?= $advertising->cityTitle ?>
            </td>
        </tr>
        <tr>
            <td class="font700" >
                Fiyat
            </td>
            <td><?= money($advertising->price, true) ?> TL</td>
        </tr>
        <tr>
            <td class="font700">Emlak No</td>
            <td><?= $advertising->estateNo ?></td>
        </tr>
        <tr>
            <td class="font700">İl</td>
            <td><?= $advertising->cityTitle ?></td>
        </tr>
        <tr>
            <td class="font700">İlçe</td>
            <td><?= $advertising->townTitle ?></td>
        </tr>
        <tr>
            <td class="font700">Semt</td>
            <td><?= $advertising->localeTitle ?></td>
        </tr>
        <tr>
            <td class="font700">Emlak Türü</td>
            <td><?= $advertising->kindTitle ?></td>
        </tr>
        <tr>
            <td class="font700">Emlak Tipi</td>
            <td><?= $advertising->statusId == 1 ? 'Kiralık' : 'Satılık' ?> <?=
                $advertising->typeTitle ?></td>
        </tr>
        <tr>
            <td class="font700">Metrekare</td>
            <td><?= $advertising->squareMeter ?></td>
        </tr>
        <?php if ($advertising->estateKindId != 3): ?>
            <tr>
                <td class="font700">Oda Sayısı</td>
                <td><?= $advertising->roomTitle ?></td>
            </tr>
            <tr>
                <td class="font700">Bulunduğu Kat</td>
                <td><?= $advertising->floorTitle ?></td>
            </tr>
            <tr>
                <td class="font700">Binadaki Kat Sayısı</td>
                <td><?= $advertising->floorCount ?></td>
            </tr>
            <tr>
                <td class="font700">Binanın Yaşı</td>
                <td><?= $advertising->ageTitle ?></td>
            </tr>
            <tr>
                <td class="font700">Isınma Şekli</td>
                <td><?= $advertising->heatingTitle ?></td>
            </tr>
            <tr>
                <td class="font700">Kullanım Durumu</td>
                <td><?= $advertising->usageStatusTitle ?></td>
            </tr>
        <?php else: ?>
            <tr>
                <td class="font700">Metrekare Fİyatı</td>
                <td><?= money($advertising->price/$advertising->squareMeter, true); ?> TL</td>
            </tr>
            <tr>
                <td class="font700">İmar Durumu</td>
                <td><?= $advertising->imarTitle ?></td>
            </tr>
            <tr>
                <td class="font700">Ada No</td>
                <td><?= $advertising->ada ?></td>
            </tr>
            <tr>
                <td class="font700">Parsel No</td>
                <td><?= $advertising->parsel ?></td>
            </tr>
            <tr>
                <td class="font700">Pafta No</td>
                <td><?= $advertising->pafta ?></td>
            </tr>
            <tr>
                <td class="font700">Kaks (Emsal)</td>
                <td><?= $advertising->kaksTitle ?></td>
            </tr>
            <tr>
                <td class="font700">Gabari</td>
                <td><?= $advertising->gabariTitle ?></td>
            </tr>
            <tr>
                <td class="font700">Tapu Drumu</td>
                <td><?= $advertising->tapuTitle ?></td>
            </tr>
            <tr>
                <td class="font700">Kat Karşılığı</td>
                <td><?= $advertising->kat == 1 ? 'Evet' : 'Hayır' ?></td>
            </tr>
            <tr>
                <td class="font700">Krediye Uygunluk</td>
                <td>
                    <?php if($advertising->kredi == 1) echo 'Evet' ?>
                    <?php if($advertising->kredi == 2) echo 'Hayır' ?>
                    <?php if($advertising->kredi == 3) echo 'Bilinmiyor' ?>
                </td>
            </tr>
            <tr>
                <td class="font700">Takaslı</td>
                <td>
                    <?php if($advertising->takas == 1) echo 'Evet' ?>
                    <?php if($advertising->takas == 2) echo 'Hayır' ?>
                    <?php if($advertising->takas == 3) echo 'Bilinmiyor' ?>
                </td>
            </tr>

        <?php endif; ?>

            <tr>
                <td class="font700">
                    Özellikler
                </td>
                <td>
                    <?php if (! empty($properties)): ?>
                        <?php foreach ($properties->allProperties as $property): ?>
                            <div class="col-sm-3 font700 colorGrey tabOzellikler <?= in_array($property->id, $properties->relations) ? 'active' : 'passive' ?>">
                                <?= $property->title ?>
                            </div>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </td>
            </tr>

        </tbody>
    </table>

</center>

</body>
</html>