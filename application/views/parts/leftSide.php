<div class="col-sm-2 leftSide">
    <div class="row">

        <style type="text/css">
            .lsTB { padding: 0px; }
            .lsTB .lsT { padding: 15px 15px 0px 15px; background-color: #c1e3fc; }
            .lsTB .lsB { padding: 15px 15px 0px 15px; background-color: #3865d2; }
            .lsTB .lsT input { background-color: #86827F; border-color: #383534; }
            .lsTB .lsT button { background-color: #000; }
            .lsTB .lsT button i { color: #fff; font-size: 16px; }
            .portfoy-col { background-color: #3865D2; color: white; }
            .portfoy-col a { color: #fff; }
        </style>
        <!-- HIZLI ARAMA START
        <div class="col-sm-12 leftSideCol lsTB">
            <div class="lsT clearfix">
                <form method="get" action="<?= clink('@advertising') ?>/search" accept-charset="ıso-8859-15"
                      style="margin-bottom: 14px;">
                    <input type="hidden" name="display" value="0">
                    <div class="input-group">
                        <input type="text" class="form-control brack" name="estate"
                               placeholder="Kelime veya İlan No ile Ara">
                        <span class="input-group-btn">
                            <button type="submit" class="btn btn-default btn-form"><i class="fa fa-search"></i></button>
                        </span>
                    </div>
                </form>
            </div>
            <div class="lsB clearfix">
                <h4 class="moduleLinkBlue">Hızlı Arama</h4>
                <form method="get" action="<?= clink('@advertising') ?>/search" accept-charset="utf-8" role="search">
                    <select name="kind" class=" form-control formMargin">
                        <option value="">Kategori Seçiniz</option>
                        <?php foreach ($this->advertising->getKinds() as $item): ?>
                            <option
                                value="<?= $item->id ?>" <?= $this->input->get('kind') == $item->id ? 'selected' : '' ?>>
                                <?= $item->title ?>
                            </option>
                        <?php endforeach; ?>
                    </select>
                    <select name="status" class="form-control formMargin">
                        <option value="">Durum Seçiniz</option>
                        <option value="1" <?= $this->input->get('status') == "1" ? 'selected' : '' ?>>Kiralık</option>
                        <option value="2" <?= $this->input->get('status') == "2" ? 'selected' : '' ?>>Satılık</option>
                    </select>
                    <select name="age" class="form-control formMargin">
                        <option value="">Yaş Aralığı Seçiniz</option>
                        <?php foreach ($this->advertising->getAges() as $item): ?>
                            <option value="<?= $item->id ?>" <?= $this->input->get('age') == $item->id ? 'selected' : '' ?>>
                                <?= $item->title ?>
                            </option>
                        <?php endforeach; ?>
                    </select>
                    <div class="input-group formMargin">
                        <input type="text" name="pricemin" value="<?= $this->input->get('pricemin') ?>"
                               class="form-control form numeric" placeholder="Min. Fiyat"/>
                        <span class="input-group-addon">-</span>
                        <input type="text" name="pricemax" value="<?= $this->input->get('pricemax') ?>"
                               class="form-control form numeric" placeholder="Max. Fiyat"/>
                    </div>
                    <div class="text-center">
                        <button type="submit" class="btn btn-primary btn-Search formMargin marginBottom15"
                                style="">ARAMA YAP
                        </button>    
                    </div>

                </form>
            </div>
        </div>
         HIZLI ARAMA END -->

        <?php $count = $this->advertising->counts(); ?>
        <!-- PORTFÖYLERİMİZ START -->
        <div class="col-sm-12 leftSideCol portfoy-col">
                <h4 class="moduleLinkBlue">Portföylerimiz</h4>
            <h5>SATILIK</h5>
            <ul class="nazarTextBlue">
                <li>
                    <a href="<?= clink(array('@advertising')) ?>?kind=1&status=2#result">
                        <span class="badge"><?= $count->s_konut ?></span> Konut
                    </a>
                </li>
                <li>
                    <a href="<?= clink(array('@advertising')) ?>?kind=2&status=2#result">
                        <span class="badge"><?= $count->s_isyeri ?></span> İş Yeri
                    </a>
                </li>
                <li>
                    <a href="<?= clink(array('@advertising')) ?>?kind=3&status=2#result">
                        <span class="badge"><?= $count->s_arsa ?></span> Arsa
                    </a>
                </li>
                <li>
                    <a href="<?= clink(array('@advertising')) ?>?kind=6&status=2#result">
                        <span class="badge"><?= $count->s_yazlik ?></span> Yazlık
                    </a>
                </li>
            </ul>
            <h5>KİRALIK</h5>
            <ul class="nazarTextYellow">
                <li>
                    <a href="<?= clink(array('@advertising')) ?>?kind=1&status=1#result">
                        <span class="badge"><?= $count->k_konut ?></span> Konut
                    </a>
                </li>
                <li>
                    <a href="<?= clink(array('@advertising')) ?>?kind=2&status=1#result">
                        <span class="badge"><?= $count->k_isyeri ?></span> İş Yeri
                    </a>
                </li>
                <li>
                    <a href="<?= clink(array('@advertising')) ?>?kind=3&status=1#result">
                        <span class="badge"><?= $count->k_arsa ?></span> Arsa
                    </a>
                </li>
                <li>
                    <a href="<?= clink(array('@advertising')) ?>?kind=6&status=1#result">
                        <span class="badge"><?= $count->k_yazlik ?></span> Yazlık
                    </a>
                </li>
            </ul>
        </div>
        <!-- PORTFÖYLERİMİZ END -->


<!--        --><?php //$build = $this->advertising->newBuild(); ?>
<!--        --><?php //if (!empty($build)): ?>
<!--            <!-- YATIRIM START -->
<!--            <div class="col-sm-12 leftSideCol acilSatilik">-->
<!--                <h4 class="moduleLinkYellow">-->
<!--                    <a href="--><?//= clink(array('@advertising')) ?><!--?build=1">-->
<!--                        YENİ İNŞAATLAR-->
<!--                    </a>-->
<!--                </h4>-->
<!--                <a href="--><?//= clink(array('@advertising', $build->slug, $build->id)) ?><!--">-->
<!--                    <div class="box satilik">-->
<!--                        <div class="boxBack"-->
<!--                             style="background-image:url('--><?////= uploadPath($build->image, "ilanlar") ?><!--/*');"></div>*/-->
<!--                        <div class="boxName"><h5>*/--><?////= $build->title ?><!--<!--</h5></div>-->
<!--                        <div class="boxPrice"><h5>FİYAT : <span>--><?//= money($build->price, false) ?><!-- TL</span></h5></div>-->
<!--                        <div class="boxLoca"><h6>--><?//= $build->townTitle ?><!--, --><?//= $build->cityTitle ?><!--</h6></div>-->
<!--                    </div>-->
<!--                </a>-->
<!--            </div>-->
<!--            <!-- YATIRIM END -->
<!--        --><?php //endif; ?>
        <div class="col-sm-12 leftSideCol e-bulten">
            <a href="<?= clink(array('@advertising')) ?>?investor=1" target="_blank">
                <img  id="left" style="width: 285px;"  src="public/img/yatirimci-firsatlar.jpg" alt="ayin-gayrimenkulleri"
                     class="img-responsive">
            </a>
        </div>
        <div class="col-sm-12 leftSideCol e-bulten">
            <a href="<?= clink(array('@advertising')) ?>?monthly=1" target="_blank">
                <img  id="left" style="width: 285px;"  src="public/img/ayin-gayrimenkuller.jpg" alt="ayin-gayrimenkulleri"
                     class="img-responsive">
            </a>
        </div>
        <div class="col-sm-12 leftSideCol e-bulten">
            <a href="<?= $this->site->get('hurriyet') ?>" target="_blank">
                <img  id="left" style="width: 285px;"  src="public/img/hurriyet.png" alt="Hürriyet Emlak"
                     class="img-responsive">
            </a>
        </div>



        <?php if (!empty($emergencies)): ?>
            <?php $this->view('parts/emergency') ?>
        <?php endif; ?>


        <?php
        $this->session->set_userdata('captcha', '');
        $first = rand(1, 15);
        $result = rand($first, 20);
        $second = $result - $first;
        $captcha = (object)array(
            'first'  => $first,
            'second' => $second,
            'result' => $result
        );
        $this->session->set_userdata('captcha', $captcha);

        ?>
<!--        <!-- E-BULTEN START -->
<!--        <div class="col-sm-12 leftSideCol e-bulten">-->
<!--            <h4>E-POSTA LİSTEMİZE KAYIT OLUN !</h4>-->
<!---->
<!--            <form action="mail-ekle" method="post" accept-charset="utf-8" id="maillist">-->
<!--                --><?//= $this->site->alert(); ?>
<!---->
<!--                <input type="text" class="form-control" required name="fullname" placeholder="Ad Soyad"><br/>-->
<!---->
<!--                <div class="row">-->
<!--                    <div class="col-xs-12">-->
<!--                        <div class="input-group">-->
<!--                            <input type="text" class="form-control" required name="email" placeholder="isminiz@domain.com">-->
<!--                            <span class="input-group-btn">-->
<!--                              <button class="btn btn-default" type="submit"><i class="fa fa-envelope-o"></i></button>-->
<!--                            </span>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--                <div class="form-group">-->
<!--                    <p class="help-block">Lütfen aşağıdaki işlemi tamamlayınız.</p>-->
<!--                    <div style="padding: 3px 7px;" class="pull-left">-->
<!--                        --><?//= $captcha->first ?>
<!--                    </div>-->
<!--                    <div style="padding: 3px 7px;" class="pull-left">-->
<!--                        +-->
<!--                    </div>-->
<!--                    <div style="padding: 0 7px;" class="pull-left">-->
<!--                        <input type="text" style="width: 30px; padding: 3px; border-color: #ccc;-->
<!--                                        -webkit-border-radius: 4px;-moz-border-radius: 4px;border-radius: 4px;"-->
<!--                               name="cap" required>-->
<!--                    </div>-->
<!--                    <div style="padding: 3px 7px;" class="pull-left">-->
<!--                        =-->
<!--                    </div>-->
<!--                    <div style="padding: 3px 7px;" class="pull-left">-->
<!--                        --><?//= $captcha->result ?>
<!--                    </div>-->
<!--                    <div class="clear-fix"></div>-->
<!--                </div>-->
<!--            </form>-->
<!--        </div>-->
<!--        <!-- E-BULTEN END -->


    </div><!--row-->
</div>


