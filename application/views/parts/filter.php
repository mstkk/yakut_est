<div class="filter">
    <h3 class="caption1">GAYRİMENKUL ARAMA</h3>

    <form method="get" action="<?= clink('@advertising') ?>/search" accept-charset="utf-8">

        <div class="row">
            <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="form-group">
                    <select name="status" class="form-control">
                        <option value="">Seçiniz</option>
                        <option value="1" <?= $this->input->get('status') == "1" ? 'selected' : '' ?>>Kiralık</option>
                        <option value="2" <?= $this->input->get('status') == "2" ? 'selected' : '' ?>>Satılık</option>
                    </select>
                </div>
            </div>

            <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="form-group">
                    <select name="town" class="city-list form-control">
                        <option value="">İl</option>
                        <?php foreach ($this->advertising->getCity() as $item): ?>
                            <option value="<?= $item->id ?>" <?= $this->input->get('kind') == $item->id ? 'selected' : '' ?>>
                                <?= $item->title ?>
                            </option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">Emlak Kodu</label>
                    <input type="text" name="estateno" class="form-control form" value="<?= $this->input->get('estateno') ?>" />
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">İlçe</label>
                    <select name="town" class="town-list form-control">
                        <option value="">İlçe</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">Emlak Tipi</label>
                    <select name="kind" class=" form-control">
                        <option value="">Seçiniz</option>
                        <?php foreach ($this->advertising->getKinds() as $item): ?>
                            <option value="<?= $item->id ?>" <?= $this->input->get('kind') == $item->id ? 'selected' : '' ?>>
                                <?= $item->title ?>
                            </option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">Semt</label>
                    <select name="locale" class="district-list form-control">
                        <option value="">Semt</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">Fiyat Aralığı</label>
                    <div class="input-group">
                        <input type="text" name="pricemin" value="<?= $this->input->get('pricemin') ?>"
                               class="form-control form numeric" />
                        <span class="input-group-addon">-</span>
                        <input type="text" name="pricemax" value="<?= $this->input->get('pricemax') ?>"
                               class="form-control form numeric" />
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">Metre Kare</label>
                    <div class="input-group">
                        <input type="text" name="squaremin" value="<?= $this->input->get('squaremin') ?>"
                               class="form-control form numeric" />
                        <span class="input-group-addon">-</span>
                        <input type="text" name="squaremax" value="<?= $this->input->get('squaremax') ?>"
                               class="form-control form numeric" />
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 text-center">
                <button type="submit" class="btn">ARA</button>
            </div>
        </div>
    </form>
</div>