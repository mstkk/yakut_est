<?php foreach ($emergencies as $key => $emergency): ?>
<!-- ACİL SATILIK START -->
<div class="col-sm-12 leftSideCol acilSatilik">
    <h4 class="moduleLink<?= $emergency->statusId == 1 ? 'Yellow' : 'Blue' ?>">
        <a href="ilanlar?emergency=1" style="color: white;">
            ACİL İLANLAR
        </a>
    </h4>
    <a href="<?= clink(array('@advertising', $emergency->slug, $emergency->id)) ?>">
        <div class="box <?= $emergency->statusId == 1 ? 'kiralik' : 'satilik' ?>">
            <div class="boxBack" style="background-image:url('<?= uploadPath($emergency->image, 'ilanlar') ?>');"></div>
            <div class="boxName"><h5><?= $emergency->title ?></h5></div>
            <div class="boxPrice"><h5>FİYAT : <span><?= money($emergency->price, true) ?> <?php echo $emergency->moneyType ?></span></h5></div>
            <div class="boxLoca">
                <h6>
                    <?= $emergency->cityTitle ?>, <?= $emergency->townTitle ?>
                </h6>
            </div>
        </div>
    </a>
</div>
<!-- ACİL SATILIK END -->
    <?php if ($key == 0) break; ?>
<?php endforeach; ?>