<section id="main">

    <div class="container">


        <!-- WRAPPER START -->

        <div class="col-sm-10 rightSide">


            <div class="col-sm-12 rightSideBack">

                <h4 class="sayfaBaslik"><?= $content->title ?></h4>

                <div class="row">


                    <div class="col-sm-12">

                        <?= $content->detail ?>


                        <div class="buttons">

                            <a class="btn btn-xs btn-success" href="javascript:history.back();"><span
                                    class="glyphicon glyphicon-chevron-left"></span> <?php echo lang('content-go-back') ?>
                            </a>

                        </div>


                        <div class="share-box">

                            <p><strong><?php echo lang('content-share-social') ?></strong></p>

                            <a class="facebook" href="http://facebook.com/sharer.php?u=<?php echo current_url() ?>"
                               title="<?php echo lang('content-share-facebook') ?>"><?php echo lang('content-share-facebook') ?></a>

                            <a class="twitter"
                               href="https://twitter.com/share?url=<?php echo current_url() ?>&text=<?php echo htmlspecialchars($content->title) ?>"
                               title="<?php echo lang('content-share-twitter') ?>"><?php echo lang('content-share-twitter') ?></a>

                            <a class="google" href="https://plus.google.com/share?url=<?php echo current_url() ?>"
                               title="<?php echo lang('content-share-google') ?>"><?php echo lang('content-share-google') ?></a>

                        </div>

                    </div>


                    <!--divider START-->

                    <div class="col-sm-12 colDivider"></div>

                    <!--divider END-->


                </div>


            </div><!--col-sm-12-->


            <?php $this->view('parts/estate') ?>


        </div><!--col-sm-9-->


        <?php $this->view('parts/leftSide') ?>


        <!-- WRAPPER END -->

    </div><!--container-->

</section>