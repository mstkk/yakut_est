<section class="container relative" style="top: -23px;">
    <div class="row">
        <!-- banner -->
        <div class="col-md-8 col-sm-8 col-xs-12">
            <div class="col-md-12">
                <h3 class="caption1">HABERLER</h3>
            </div>
            <div class="row">
                <?php if (! empty($newscast)): ?>
                    <?php foreach ($newscast as $news): ?>
                        <!-- adverd-item -->
                        <div class="adverd-item col-md-4 col-sm-4 col-xs-12">
                            <a class="relative" href="<?= clink(array('@news', $news->slug, $news->id)) ?>">
                                <img src="<?= uploadPath($news->image, 'news') ?>" class="img-responsive"
                                     alt="<?= $news->title ?>"/>
                                <h5><?= $news->title ?></h5>
                                <div class="summary">
                                    <?= $news->summary ?>
                                </div>

                                <div class="clear"></div>

                                <div class="time">
                                    <i class="fa fa-calendar-times-o"></i>
                                    <?php echo $this->date->set($news->date)->dateWithName() ?>
                                </div>
                                <button type="button" class="btn btn-warning">İNCELE</button>
                            </a>
                        </div>
                        <!-- adverd-item -->
                    <?php endforeach; ?>
                <?php endif; ?>

            </div>

            <div class="text-center">
                <?php if (! empty($pagination)): ?>
                    <?php echo $pagination ?>
                <?php endif; ?>
            </div>
        </div>
        <!-- banner -->

        <!-- filter -->
        <div class="col-md-4 col-sm-4 col-xs-12">
            <?php $this->view('parts/filter') ?>

            <!--      Acil İlanlar      -->
            <div class="col-md-12" style="margin-top: 15px;">
                <h3 class="caption1">ACİL İLANLAR</h3>
            </div>
            <?php if (! empty($emergencies)): ?>
                <?php $this->view('parts/emergency') ?>
            <?php endif; ?>
        </div>
        <!-- filter -->
    </div>
</section>









