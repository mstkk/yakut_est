<section class="container relative" style="top: -23px;">
    <div class="row">
        <!-- banner -->
        <div class="col-md-8 col-sm-8 col-xs-12">
            <div class="col-md-12">
                <h3 class="caption1"><?php echo $news->title ?></h3>
            </div>
            <div class="clearfix"></div>
            <div class="text-container">
                <img src="<?= uploadPath($news->image, 'news') ?>" alt="<?php echo $news->title ?>" align="left" />

                <?php echo $news->detail ?>
            </div>

            <div class="buttons clearfix">
                <a class="btn btn-xs btn-success" href="javascript:history.back();"><span class="glyphicon glyphicon-chevron-left"></span> <?php echo lang('news-go-back') ?></a>
                <a class="btn btn-xs btn-success" href="<?php echo clink('@news') ?>"><?php echo lang('news-all-news') ?></a>
            </div>

            <div class="share-box">
                <p><strong><?php echo lang('news-share-social') ?></strong></p>
                <a class="facebook" href="http://facebook.com/sharer.php?u=<?php echo current_url() ?>" title="<?php echo lang('news-share-facebook') ?>"><?php echo lang('news-share-facebook') ?></a>
                <a class="twitter" href="https://twitter.com/share?url=<?php echo current_url() ?>&text=<?php echo htmlspecialchars($news->title) ?>" title="<?php echo lang('news-share-twitter') ?>"><?php echo lang('news-share-twitter') ?></a>
                <a class="google" href="https://plus.google.com/share?url=<?php echo current_url() ?>" title="<?php echo lang('news-share-google') ?>"><?php echo lang('news-share-google') ?></a>
            </div>

        </div>
        <!-- banner -->

        <!-- filter -->
        <div class="col-md-4 col-sm-4 col-xs-12">
            <?php $this->view('parts/filter') ?>

            <!--      Acil İlanlar      -->
            <div class="col-md-12" style="margin-top: 15px;">
                <h3 class="caption1">ACİL İLANLAR</h3>
            </div>
            <?php if (! empty($emergencies)): ?>
                <?php $this->view('parts/emergency') ?>
            <?php endif; ?>
        </div>
        <!-- filter -->
    </div>
</section>











