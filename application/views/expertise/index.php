<main id="main">

    <div class="container">
        <div class="col-md-12">
            <h3 class="caption1"><?= $this->module->arguments->title ?></h3>
        </div>
        <div class="row" style="margin-top: 30px;">
            <div class="col-md-6">
                <h3 class="caption1"><?php echo lang('contact-information') ?></h3>
                <?php echo $this->module->arguments->detail ?>
            </div>

            <div class="col-md-6">
                <h3 class="caption1"><?php echo lang('contact-form') ?></h3>

                <form method="post" action="<?php echo clink('@contact') ?>" accept-charset="utf-8">
                    <?php echo $this->site->alert() ?>

                    <div class="row expertise">
                        <h3 class="page-header" style="margin-top: 0; padding-top: 0;">Talep Eden Bilgileri</h3>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label class="control-label"> Talep Eden Kurum(Şahıs) :</label>
                                <input type="text" class="form-control" name="t_sube" value="<?php echo set_value('t_sube') ?>">
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="form-group">
                                <label class="control-label">Yetkili Ad Soyad *:</label>
                                <input type="email" class="form-control" name="t_fullname" required="required"
                                       value="<?php echo set_value('t_fullname') ?>" />
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="form-group">
                                <label class="control-label">Yetkili Telefon *:</label>
                                <input type="text" class="form-control mask-phone" name="t_phone" required="required"
                                       value="<?php echo set_value('t_phone') ?>" />
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="form-group">
                                <label class="control-label">Yetkili Mail *:</label>
                                <input type="text" class="form-control mask-phone" name="t_mail" required="required"
                                       value="<?php echo set_value('t_mail') ?>" />
                            </div>
                        </div>

                        <div class="clearfix"></div>
                        <h3 class="page-header" style="margin-top: 10px;">Gayrimenkul Bilgileri</h3>

                        <div class="col-sm-12">
                            <div class="form-group">
                                <label class="control-label">Bulunduğu Şehir *:</label>
                                <input type="text" class="form-control mask-phone" name="g_city" required="required"
                                       value="<?php echo set_value('g_city') ?>" />
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="form-group">
                                <label class="control-label">Bulunduğu İlçe *:</label>
                                <input type="text" class="form-control mask-phone" name="g_town" required="required"
                                       value="<?php echo set_value('g_town') ?>" />
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="form-group">
                                <label class="control-label">Bulunduğu Adres *:</label>
                                <textarea class="form-control" name="g_address" required="required" rows="5"><?php echo set_value('g_address') ?></textarea>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label">Pafta :</label>
                                <input type="text" class="form-control mask-phone" name="g_pafta" value="<?php echo set_value('g_pafta') ?>" />
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label">Ada :</label>
                                <input type="text" class="form-control mask-phone" name="g_ada" value="<?php echo set_value('g_ada') ?>" />
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label">Parsel:</label>
                                <input type="text" class="form-control mask-phone" name="g_parsel" value="<?php echo set_value('g_parsel') ?>" />
                            </div>
                        </div>

                        <div class="clearfix"></div>
                        <h3 class="page-header" style="margin-top: 10px;">Gayrimenkul Gösterecek Kişinin</h3>

                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="control-label">Ad Soyad *:</label>
                                <input type="text" class="form-control mask-phone" name="k_fullname"
                                       required="required" value="<?php echo set_value('k_fullname') ?>" />
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="control-label">Telefon *:</label>
                                <input type="text" class="form-control mask-phone" name="k_phone" required="required"
                                       value="<?php echo set_value('k_phone') ?>" />
                            </div>
                        </div>

                        <div class="clearfix"></div>
                        <h3 class="page-header" style="margin-top: 10px;">Müşteri Fatura Bilgileri</h3>

                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label">Firma Adı / Ünvanı :</label>
                                <input type="text" class="form-control mask-phone" name="m_fullname" value="<?php echo set_value('m_fullname') ?>" />
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label">Vergi Dairesi :</label>
                                <input type="text" class="form-control mask-phone" name="m_vergiD"
                                       value="<?php echo set_value('m_vergiD') ?>" />
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label">Vergi No:</label>
                                <input type="text" class="form-control mask-phone" name="m_vergiNo" value="<?php echo set_value('m_vergiNo') ?>" />
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="form-group">
                                <label class="control-label">Fatura Adresi :</label>
                                <textarea class="form-control" name="m_address" rows="5"><?php echo set_value('m_address') ?></textarea>
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="form-group">
                                <label class="control-label">Açıklama :</label>
                                <textarea class="form-control" name="m_comment" rows="5"><?php echo set_value('m_comment') ?></textarea>
                            </div>
                        </div>

                    </div>

                    <div class="form-group text-center">
                        <button type="submit" class="btn btn-warning"><i class="fa fa-check fa-lg"></i> &nbsp;
                            Gönder</button>
                    </div>

                </form>
            </div>
        </div>

    </div>

</main>