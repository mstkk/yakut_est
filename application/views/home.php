<section id="main">
    <div class="container-fluid" style="padding:0; height: 560px;">
                    <div class="sliderImage">
                        <?php
                        if (isset($this->banner)) {
                            $this->view('banner/slider');
                        }
                        ?>
                    </div>
                </div> 
    <div class="container">
        <div class="col-sm-12">

            <?= $this->site->alert(); ?>
        </div>
		<style>
            .fastSearch .form-control {
                padding: 5px 12px;
                font-size: 13px;
                margin-bottom: 18px;
                border-radius: 3px;
                height: 35px;
                background: rgba(255, 255, 255, 1);
            }
            .lsTB .lsT {
                padding: 15px 15px 0px 15px;
                background-color: transparent!important;
            }
            .lsTB .lsB {
                padding: 15px 0px 0px 0px;
                border-radius: 0px;
                border-bottom-left-radius: 25px;
                background-color: transparent!important;
            }
            #keyFilter button {
                background: #000;
                color: #fff;
                margin-top: -17px;
            }
		</style>
        <!-- SLIDER ve ARAMA START -->
        <div class="col-sm-12 marginTop15">
            <div class="row" style="position:absolute;">
            
                <div class="col-sm-4 pull-right fastSearch lsTB hidden-xs" style="position: relative; top: -485px; z-index: 2; border-radius: 2px; background-color: rgba(0, 81, 137, 0.80)!important;">
                    <div class="lsT clearfix">
                        <form method="get" action="<?= clink('@advertising') ?>/search" id="keyFilter" accept-charset="utf-8"
                              style="">
                            <input type="hidden" name="display" value="0">
                            <div class="input-group">
                                <input type="text" class="form-control brack" name="estate"
                                       placeholder="Kelime veya İlan No ile Ara">
                                <span class="input-group-btn">
                                    <button type="submit" class="btn btn-default btn-form"><i class="fa fa-search"></i></button>
                                </span>
                            </div>
                        </form>
                    </div>
                    <div class="lsB clearfix">
                       
                        <form method="get" action="<?= clink('@advertising') ?>/search" accept-charset="utf-8">
                            <input type="hidden" name="display" value="0">
							<div class="col-sm-12">
                            <select name="kind" class=" form-control ">
                                <option value="">Kategori Seçiniz</option>
                                <?php foreach ($this->advertising->getKinds() as $item): ?>
                                    <option
                                        value="<?= $item->id ?>" <?= $this->input->get('kind') == $item->id ? 'selected' : '' ?>>
                                        <?= $item->title ?>
                                    </option>
                                <?php endforeach; ?>
                            </select>
							    </div>
								<div class="col-md-12">
								
                            <select name="status" class="form-control ">
                                <option value="">Durum Seçiniz</option>
                                <option value="1" <?= $this->input->get('status') == "1" ? 'selected' : '' ?>>Kiralık
                                </option>
                                <option value="2" <?= $this->input->get('status') == "2" ? 'selected' : '' ?>>Satılık
                                </option>
                            </select>
								</div>
							 <div class="col-sm-12">

                            <select name="city" class="city-list form-control">

                                <option value="">Şehir Seçiniz</option>

                                <?php foreach ($this->advertising->getCity() as $item): ?>

                                    <option

                                        value="<?= $item->id ?>" <?= $this->input->get('city') == $item->id ? 'selected' : '' ?>>

                                        <?= $item->title ?>

                                    </option>

                                <?php endforeach; ?>

                            </select>

                        </div>

                        <div class="col-sm-12">
                            <select name="town" class="town-list form-control">
                                <option value="">İlçe Seçiniz</option>
                            </select>
                        </div>
                        <div class="col-sm-12">

                            <div class="row">

                                <div style="width:100%; padding:0px 15px; float:left;">

                                    <form class="form-inline">

                                        <div class="form-group">

                                           

                                            <div style=" float:left;">
<div class="col-md-6" style="padding-left:0!important;"> <input type="text"  class="form-control numeric" id="aralik1"

                                                       name="pricemin"

                                                       placeholder="Min. Fiyat" style=" float:left;"

                                                       value="<?= $this->input->get('pricemin') ?>"></div>
<div class="col-md-6" style="padding-right:0!important;"> <input type="text"   class="form-control numeric" id="aralik2"

                                                       name="pricemax"

                                                       placeholder="Max. Fiyat" style=" float:left;"

                                                       value="<?= $this->input->get('pricemax') ?>"></div>
                                               

                                               

                                            </div>

                                        </div>

                                    </form>

               <div class="column col-xs-6" style="padding-left:0!important;">
                        <div class="form-group">
                         
                            <input type="text" value="Min. m2" name="searchMinSquaremeter" id="banner-search-squaremeter-min" class="form-control input-sm numeric" />
                        </div>
                    </div>
                    <div class="column col-xs-6" style="padding-right:0!important;">
                        <div class="form-group">
                       
                            <input type="text" value="Max. m2" name="searchMaxSquaremeter" id="banner-search-squaremeter-max" class="form-control input-sm numeric" />
                        </div>
                    </div>

                          <select name="searchRoom" class="form-control formMargin">
                                <option value="">Oda Sayısı:</option>
                                 <?php foreach (prepareForSelect($this->common->rooms(), 'id', 'title', '') as $value => $label): ?>
                            <option value="<?php echo $value ?>"><?php echo $label ?></option>
                        <?php endforeach; ?>
                            </select>
                          
                            <div class="text-center">
                            <button type="submit" class="btn btn-primary btn-Search formMargin marginBottom15"
                                style="margin-bottom:19px !important;">ARAMA YAP
                            </button>
                            </div>
                        </form>
                    </div>
                </div>
                </div>
           
                </div>
                </div>
            </div>
        </div>
        <!-- SLIDER ARAMA END -->

        <?php $count = $this->advertising->counts(); ?>
        <!-- WRAPPER START -->
        <div class="col-sm-10 rightSide" style="    margin-top: 135px;">
            <div class="row">

                <div class="col-xs-12">
                    <h3 class="" style="margin-top:10px;color:white;background:#005189;    padding: 10px 10px 10px 20px;border-radius:2px;">VİTRİN İLANLARI</h3>
                </div>
                <?php if (!empty($visions)): ?>
                    <?php foreach ($visions as $vision): ?>

                            <?php $image = ! empty($vision->image) ? uploadPath($vision->image, 'ilanlar') : "public/img/ilan/big.jpg" ?>
                            <div class="col-md-4 col-sm-6" style="    margin-bottom: 10px;">
                                <a href="<?= clink(array('@advertising', $vision->slug, $vision->id)) ?>">
                                    <div class="box box-main <?= $vision->statusId == 1 ? 'kiralik' : 'satilik' ?>">
                                        <div class="boxBack"
                                             style="background-image:url('<?= $image ?>');">
<!--                                                --><?php //var_dump($vision);die; ?>
                                                <div class="ilan-fiyat">İlan No: <?= $vision->id ?></div>
                                             </div>
                                        <!--<div class="boxName"><h5><? //= $vision->title ?></h5></div> -->
                                        <div class="boxName"><h5> <?= $vision->title ?> </h5></div>
                                        <div class="boxName pull-right"><h5> <?= bd_nice_number($vision->price) ?> <?php echo $vision->moneyType ?></h5></div>
                                        <!--
                                        <div class="boxPrice"><h5>FİYAT : <span><?= bd_nice_number($vision->price) ?>
                                                    TL</span></h5></div>
                                        -->
                                        <div class="boxLoca">
                                            <h6>
                                                <?= $vision->cityTitle ?>,
                                                <?= $vision->townTitle ?>,
                                                <?= $vision->districtTitle ?>
                                            </h6>
                                        </div>
                                    </div>
                                </a>
                            </div>

                    <?php endforeach; ?>
                <?php endif; ?>

                <!--                <div class="col-xs-12">-->
                <!--                    <h3 class="captionOrange">SON EKLENEN KİRALIK İLANLARI</h3>-->
                <!--                </div>-->
                <!--            --><?php //if (! empty($visions)): ?>
                <!--                --><?php //foreach ($visions as $vision): ?>
                <!--                    --><?php //if ($vision->statusId == 1): ?>
                <!--                        <div class="col-md-4 col-sm-6">-->
                <!--                            <a href="-->
                <? //= clink(array('@advertising', $vision->slug, $vision->id)) ?><!--">-->
                <!--                                <div class="box -->
                <? //= $vision->statusId == 1 ? 'kiralik' : 'satilik' ?><!--">-->
                <!--                                    <div class="boxBack" style="background-image:url('-->
                <? //= uploadPath
                //($vision->image, 'ilanlar') ?><!--');"></div>
                                    <div class="boxName"><h5>--><? //= $vision->title ?><!--</h5></div>-->
                <!--                                    <div class="boxPrice"><h5>FİYAT : <span>-->
                <? //= bd_nice_number($vision->price) ?><!-- TL</span></h5></div>-->
                <!--                                    <div class="boxLoca">-->
                <!--                                        <h6>-->
                <!--                                            --><? //= $vision->cityTitle ?><!--,-->
                <!--                                            --><? //= $vision->townTitle ?><!--,-->
                <!--                                            --><? //= $vision->districtTitle ?>
                <!--                                        </h6>-->
                <!--                                    </div>-->
                <!--                                </div>-->
                <!--                            </a>-->
                <!--                        </div>-->
                <!--                    --><?php //endif; ?>
                <!--                --><?php //endforeach; ?>
                <!--            --><?php //endif; ?>



            </div><!--row-->
        </div><!--col-sm-9-->
        <div style="    margin-top: 145px;">
    <?php $this->view('parts/leftSide') ?>
         </div><!--container-->
    </div><!--container-->
</section>





