
<!DOCTYPE html>

<html lang="<?php echo $this->language ?>">

    <head>
        <meta charset="utf-8">
        <title><?php echo $this->site->get('metaTitle') ?></title>
        <meta name="description" content="<?php echo $this->site->get('metaDescription') ?>">
        <meta name="keywords" content="<?php echo $this->site->get('metaKeywords') ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <base href="<?php echo base_url('/') ?>"/>

        <link rel="shortcut icon" type="image/x-icon" href="public/img/favicon.ico">
        <!-- Font Pack -->
        <link href='https://fonts.googleapis.com/css?family=Oswald:400,700,300&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
        <link href="https://fonts.googleapis.com/css?family=Doppio+One" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="public/plugin/bootstrap/css/bootstrap.min.css"/>
        <link rel="stylesheet" type="text/css" href="public/plugin/font-awesome/css/font-awesome.min.css"/>
        <link rel="stylesheet" type="text/css" href="public/plugin/fancybox/jquery.fancybox.css"/>
        <link rel="stylesheet" type="text/css" href="public/plugin/bxslider/jquery.bxslider.css"/>
        <link rel="stylesheet" type="text/css" href="public/css/main.css"/>
        <link rel="stylesheet" type="text/css" href="public/css/custom.css"/>
        <?php foreach ($this->site->assets('css') as $asset): ?>
            <link rel="stylesheet" type="text/css" href="<?php echo $asset ?>"/>
        <?php endforeach; ?>
        <link rel="shortcut icon" type="image/x-icon" href="public/img/favicon.ico">
        <script type="text/javascript" src="public/js/jquery.js"></script>
        <script type="text/javascript" src="public/js/jquery.maskedinput.min.js"></script>
        <script type="text/javascript" src="public/js/jquery.numeric.min.js"></script>
        <script type="text/javascript" src="public/js/bootstrap.filestyle.min.js"></script>
        <script type="text/javascript" src="public/plugin/bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="public/plugin/fancybox/jquery.fancybox.js"></script>
        <script type="text/javascript" src="public/plugin/bxslider/jquery.bxslider.min.js"></script>
        <?php foreach ($this->site->assets('js') as $asset): ?>
            <script type="text/javascript" src="<?php echo $asset ?>"></script>
        <?php endforeach; ?>
        <script type="text/javascript" src="public/js/main.js"></script>
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <link rel="canonical" href="<?php echo current_url() ?>"/>

        <?php if ($ogType = $this->site->get('ogType')): ?>
            <meta property="og:type" content="<?php echo $ogType ?>"/>
        <?php endif; ?>
        <?php if ($ogTitle = $this->site->get('ogTitle')): ?>
            <meta property="og:title" content="<?php echo htmlspecialchars($ogTitle) ?>"/>
        <?php endif; ?>
        <?php if ($ogDescription = $this->site->get('ogDescription')): ?>
            <meta property="og:description" content="<?php echo htmlspecialchars($ogDescription) ?>"/>
        <?php endif; ?>
        <?php if ($ogImage = $this->site->get('ogImage')): ?>
            <link rel="image_src" href="<?php echo base_url('/') . $ogImage ?>">
            <meta property="og:image" content="<?php echo base_url('/') . $ogImage ?>"/>
        <?php endif; ?>
        <meta property="og:url" content="<?php echo current_url() ?>"/>
        <?php echo $this->site->get('customMeta') ?>
    </head>
    <body>
        <header id="tops" class="topCol" style="    background: white;">
            <nav class="navbar navbar-default">
                <div class="container">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="./">
                            <img class="img-responsive" src="public/img/logo.png" alt="hakimgayrimenkul">
                        </a>
                    </div>

                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav navbar-right">
                            <?php foreach ($this->menu->get('generalMenu') as $menu): ?>
                                <li style="padding-left: 5px;" class="<?= !empty($menu->childs) ? 'dropdown ' : '' ?><?= clink($menu->link) == uri_string() ? 'active' : '' ?>
                   <?= uri_string() == "" && clink($menu->link) == './' ? 'active' : '' ?>">
                                    <a href="<?= clink($menu->link) ?>" title="<?= $menu->hint ?>"
                                       class="<?= !empty($menu->childs) ? ' dropdown-toggle' : '' ?>"
                                        <?= !empty($menu->childs) ? 'data-toggle="dropdown" aria-expanded="false"' : '' ?>>
              <span data-hover="<?= htmlspecialchars($menu->title) ?>">
              <?= $menu->title; ?><?= !empty($menu->childs) ? '<span class="caret"></span>' : '' ?>
              </span>
                                    </a>
                                    <?php if (!empty($menu->childs)): ?>
                                        <ul class="dropdown-menu">
                                            <?php foreach ($menu->childs as $child): ?>
                                                <li>
                                                    <a href="<?= clink($child->link) ?>"
                                                       title="<?= htmlspecialchars($child->title) ?>">
                                                        <?= $child->title ?>
                                                    </a>
                                                </li>
                                            <?php endforeach; ?>
                                        </ul>
                                    <?php endif; ?>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                </div>
            </nav>


        </header>

        <!-- header -->


        <?php $this->view($view); ?>

        <style>
            .quick-links ul {
                padding: 0;
                text-decoration: none;
                list-style-type: none;
            }
            .quick-links ul a{
                color: white;
                font-size: 14px;
                transition: 0.2s;
            }
            .quick-links ul a:hover{
                color: #005da9;
                background: white;
                padding: 5px;
                border-radius: 2px;
            }
            .footer-header {
                color: #e4e4e4;
                padding-bottom: 5px;
                border-bottom: 3px solid #005189;
            }
            .footer-header p {
                margin: 0;
                font-size: 12px;
                padding: 0;
            }
            .footer-logo img{
                background: #ffffffb5;
                border-radius: 3px;padding: 2px;
                width: 100%;
            }
            .wecall{
                padding: 5px 19px 5px 19px;
                background: #005da9;
                font-size: 14px;
                border: 2px solid white;
                border-radius: 2px;
                transition: 0.2s;
            }
            .wecall:hover{
                background: white;
                border: 2px solid #005da9;
                color:#005da9;
                transition: 0.2s;
            }
            .alert-success {
                color: #1a1a1a;
                background-color: #c3ffb0;
                border-color: #005189;
                font-size: 16px;
                padding: 5px 0 5px 10px;
                border-radius: 2px;
                font-weight: 600;
            }
            #err-form, err-timedout, err-state {
                color: #171717;
                background: #f04343;
                padding: 5px 10px 5px 10px;
                border-radius: 2px;
                font-weight: 600;
            }
        </style>
        <footer id="footer">
            <div class="container">
                <div class="row">
                    <div class="col-sm-4 footerContact">
                        <div class="col-md-12">
                            <h4 class="footer-header">İletişim Bilgileri
                                <p>Bize ulaşabileceğiniz tüm yollar</p>
                            </h4>
                            <p><strong>Adres :</strong>  <?= $this->site->get('adress') ?> </p>
                            <p><strong>Telefon - Fax:</strong>  <a href="tel:05372290729"><?= $this->site->get('sitePhone') ?></a></p>
                            <p><strong>E-posta :</strong>  <a href="mailto:<?= $this->site->get('maill') ?>"><?= $this->site->get('maill') ?></a></p>
                            <a href="https://www.savk.work" target="_blank" style="padding:2px 0;" title="Web sitesi, web tasarım ve daha fazlası">
                                <img src="https://www.savk.work/public/assets/img/icon/wback.png" alt="www.savk.work" class="img-responsive">
                            </a>
                        </div>
                    </div>
                    <div class="col-sm-4">
                            <div class="col-sm-12">
                                <h4 class="footer-header">Hızlı linkler
                                    <p>Yararlı ve Hızlı olabileceğini düşündüğümüz bağlantılar</p>
                                </h4>
                                <div class="quick-links">
                                    <ul class="unstyled" style="">
                                        <?php foreach ($this->menu->get('footer') as $menu): ?>
                                            <li style="padding-left: 5px;" class="<?= !empty($menu->childs) ? 'dropdown ' : '' ?><?= clink($menu->link) == uri_string() ? 'active' : '' ?>
                   <?= uri_string() == "" && clink($menu->link) == './' ? 'active' : '' ?>">
                                                <a href="<?= clink($menu->link) ?>" title="<?= $menu->hint ?>"
                                                   class="<?= !empty($menu->childs) ? ' dropdown-toggle' : '' ?>"
                                                    <?= !empty($menu->childs) ? 'data-toggle="dropdown" aria-expanded="false"' : '' ?>>
              <span data-hover="<?= htmlspecialchars($menu->title) ?>">
              <?= $menu->title; ?><?= !empty($menu->childs) ? '<span class="caret"></span>' : '' ?>
              </span>
                                                </a>
                                                <?php if (!empty($menu->childs)): ?>
                                                    <ul class="dropdown-menu">
                                                        <?php foreach ($menu->childs as $child): ?>
                                                            <li>
                                                                <a href="<?= clink($child->link) ?>"
                                                                   title="<?= htmlspecialchars($child->title) ?>">
                                                                    <?= $child->title ?>
                                                                </a>
                                                            </li>
                                                        <?php endforeach; ?>
                                                    </ul>
                                                <?php endif; ?>
                                            </li>
                                        <?php endforeach; ?>
                                    </ul>
                                </div>
                            </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="row">
                            <div class="col-sm-12" style="">
                                <div class="col-md-12">
                                    <h4 class="footer-header">Biz sizi arayalım
                                        <p>Danışmak istediğiniz konularda, biz sizi arayalım</p>
                                    </h4>
                                </div>
                                <form name="ajax-form" id="ajax-form" action="biz-sizi-arayalim" method="post">
                                    <div class="col-md-12" style="    margin-bottom: 20px;">
                                        <label for="fullname"><span style="color: #005da9;    position: absolute;
    bottom: 8px;
    right: 25px;" class="fa fa-user" aria-hidden="true"></span>
                                            <span class="error" id="err-name" style="display: none; color: #f04343;"> Ad soyad boş bırakılamaz.</span>
                                        </label>
                                        <input class="form-control" name="fullname" id="name" placeholder="Ad ve Soyadız" type="text">
                                    </div>
                                    <div class="col-md-7">
                                        <label for="phone"><span style="color: #005da9;    position: absolute;
    bottom: 8px;
    right: 25px;" class="fa fa-phone" aria-hidden="true"></span>
                                            <span class="error" id="err-phone" style="display: none; color: #f04343;"> Telefon boş bırakılamaz.</span>
                                        </label>
                                        <input name="phone" id="phone" class="form-control mask-phone" placeholder="Telefon" type="text">
                                    </div>
                                    <div class="col-md-5">
                                        <label for="comment">
                                        </label>
                                        <div id="button-con"><button class="send_message wecall" id="send">Gönder <span style="color: #fff;" class="fa fa-paper-plane" aria-hidden="true"></span></button></div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="error text-align-center" style="display: none;" id="err-form">İleti Başarısız</div>
                                        <div class="error text-align-center" style="display: none;" id="err-timedout">İleti Başarısız</div>
                                        <div class="error" id="err-state"></div>
                                    </div>
                                </form>
                                <div class="clear"></div>
                                <div class="col-md-12">
                                    <div id="ajaxsuccess" style="display: none;" class="alert alert-success">Telefon numarası başarıyla gönderildi, en kısa sürede dönüş yapılacaktır.</div>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <div class="container">
            <div class="col-md-12">
                <h4 style="text-align: center"><i class="fa fa-copyright"></i> 2018 Konfor Urla. Tüm hakları saklıdır.</h4>
            </div>
        </div>
    
    </body>
    <script>
        jQuery(document).ready(function ($) { // wait until the document is ready
            $('#send').click(function(){ // when the button is clicked the code executes
                $('.error').fadeOut('slow'); // reset the error messages (hides them)

                var error = false; // we will set this true if the form isn't valid

                var name = $('input#name').val(); // get the value of the input field
                if(name == "" || name == " ") {
                    $('#err-name').fadeIn('slow'); // show the error message
                    error = true; // change the error state to true
                }

                var phone = $('input#phone').val(); // get the value of the input field
                if(phone == "" || phone == " ") {
                    $('#err-phone').fadeIn('slow'); // show the error message
                    error = true; // change the error state to true
                }

                if(error == true) {
                    $('#err-form').slideDown('slow');
                    return false;
                }

                var data_string = $('#ajax-form').serialize(); // Collect data from form

                $.ajax({
                    type: "POST",
                    url: $('#ajax-form').attr('action'),
                    data: data_string,
                    timeout: 6000,
                    error: function(request,error) {
                        if (error == "timeout") {
                            $('#err-timedout').slideDown('slow');
                        }
                        else {
                            $('#err-state').slideDown('slow');
                            $("#err-state").html('Mesaj Gönderilemedi: ' + error + '');
                        }
                    },
                    success: function() {
                        $('#ajax-form').slideUp('slow');
                        $('#ajaxsuccess').slideDown('slow');
                    }
                });

                return false; // stops user browser being directed to the php file
            }); // end click function
        });
    </script>
</html>

