<section id="main">
    <div class="container">
        <!-- Modal -->
        <div class="modal fade" id="email" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">E-Posta İle Gönder</h4>
                    </div>
                    <form action="ilanlar/send-mail/<?= $advertising->id ?>" method="post" accept-charset="utf-8">
                        <div class="modal-body">
                            <label for="">E-Posta</label>
                            <div class="form-group">
                                <input type="email" name="email" class="form-control">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Kapat</button>
                            <button type="submit" class="btn btn-primary">Gönder</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="mapModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Haritada Gör</h4>
                    </div>
                    <div class="modal-body">
                        <?php if (!empty($advertising->map)): ?>
                            <?php
                            $maps = explode(',', $advertising->map);
                            ?>
                            <div class="map-canvas" data-lat="<?= htmlspecialchars($maps[0]) ?>"
                                 data-lng="<?= htmlspecialchars($maps[1]) ?>" style="height: 450px;">

                            </div>
                        <?php endif; ?>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Kapat</button>
                    </div>
                </div>
            </div>
        </div>


        <!-- WRAPPER START -->

        <div class="col-sm-9 rightSide">
            <div class="col-sm-12 rightSideBack">
                <?= $this->site->alert(); ?>
                <h4 class="sayfaBaslik"><?= $advertising->title ?></h4>

                <div class="row">
                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-md-5">
                                <h4 class="sayfaBaslik hide">İlan Detayları</h4>
                                <table class="table table-striped">
                                    <tbody>
                                    <tr>
                                        <td class="font700">Fiyat</td>
                                        <td class="fiyatRenk"> 
                                            <strong>
                                                <?= bd_nice_number($advertising->price) ?> <?php echo $advertising->moneyType ?>
                                            </strong>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="font700">Emlak No</td>
                                        <td><?= $advertising->id ?></td>
                                    </tr>
                                    <tr>
                                        <td class="font700">İl</td>
                                        <td><?= $advertising->cityTitle ?></td>
                                    </tr>
                                    <tr>
                                        <td class="font700">İlçe</td>
                                        <td><?= $advertising->townTitle ?></td>
                                    </tr>
                                    <tr>
                                        <td class="font700">Semt</td>
                                        <td><?= $advertising->localeTitle ?></td>
                                    </tr>
                                    <tr>
                                        <td class="font700">Emlak Türü</td>
                                        <td><?= $advertising->statusId == 1 ? 'Kiralık' : 'Satılık' ?> <?= $advertising->kindTitle ?></td>
                                    </tr>
                                    <tr>
                                        <td class="font700">Emlak Tipi</td>
                                        <td><?= $advertising->typeTitle ?></td>
                                    </tr>


                                    <?php if ($advertising->estateKindId == 3): ?>
                                        <?php if (! empty($advertising->squareMeter)): ?>
                                            <tr>
                                                <td class="font700">Metrekare </td>
                                                <td><?= $advertising->squareMeter ?> m<sup>2</sup></td>
                                            </tr>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                    <?php if (!empty($advertising->tavanYuk)): ?>
                                        <tr>
                                            <td class="font700">Tavan Yüksekliği</td>
                                            <td><?= $advertising->tavanYuk ?></td>
                                        </tr>
                                    <?php endif; ?>
                                    <?php if (!empty($advertising->bolumSay)): ?>
                                        <tr>
                                            <td class="font700">Bölüm Sayısı</td>
                                            <td><?= $advertising->bolumSay ?></td>
                                        </tr>
                                    <?php endif; ?>
                                    <?php if ($advertising->estateKindId != 3): ?>
                                        <?php if (!empty($advertising->roomTitle)): ?>
                                            <tr>
                                                <td class="font700">Oda Sayısı</td>
                                                <td><?= $advertising->roomTitle ?></td>
                                            </tr>
                                        <?php endif; ?>
                                        <?php if (! empty($advertising->squareMeter)): ?>
                                            <tr>
                                                <td class="font700">Metrekare</td>
                                                <td><?= $advertising->squareMeter ?> m<sup>2</sup></td>
                                            </tr>
                                        <?php endif; ?>
                                        <?php if (!empty($advertising->bathCount)): ?>
                                            <tr>
                                                <td class="font700">Banyo Sayısı</td>
                                                <td><?= $advertising->bathCount ?></td>
                                            </tr>
                                        <?php endif; ?>
                                        <?php if (!empty($advertising->floorCount)): ?>
                                            <tr>
                                                <td class="font700">Binadaki Kat Sayısı</td>
                                                <td><?= $advertising->floorCount ?></td>
                                            </tr>
                                        <?php endif; ?>
                                        <?php if (!empty($advertising->floorTitle)): ?>
                                            <tr>
                                                <td class="font700">Bulunduğu Kat</td>
                                                <td><?= $advertising->floorTitle ?></td>
                                            </tr>
                                        <?php endif; ?>
                                        <?php if (!empty($advertising->ageTitle)): ?>
                                            <tr>
                                                <td class="font700">Binanın Yaşı</td>
                                                <td><?= $advertising->ageTitle ?></td>
                                            </tr>
                                        <?php endif; ?>
                                        <?php if (!empty($advertising->heatingTitle)): ?>
                                            <tr>
                                                <td class="font700">Isınma Şekli</td>
                                                <td><?= $advertising->heatingTitle ?></td>
                                            </tr>
                                        <?php endif; ?>
                                        <?php if (!empty($advertising->usageStatusTitle)): ?>
                                            <tr>
                                                <td class="font700">Kullanım Durumu</td>
                                                <td><?= $advertising->usageStatusTitle ?></td>
                                            </tr>
                                        <?php endif; ?>
                                        <?php if ($advertising->depozito != 0): ?>
                                            <tr>
                                                <td class="font700">Depozito</td>
                                                <td><?= bd_nice_number($advertising->depozito) ?> <?php echo $advertising->depozitoMoneyType  ?> </td>
                                            </tr>
                                        <?php endif; ?>
                                        <?php if ($advertising->aidat != 0): ?>
                                            <tr>
                                                <td class="font700">Aidat</td>
                                                <td><?= bd_nice_number($advertising->aidat) ?>  <?php echo $advertising->aidatMoneyType ?></td>
                                            </tr>
                                        <?php endif; ?>
                                        <?php if ($advertising->kiraGetirisi != 0): ?>
                                            <tr>
                                                <td class="font700">Kira Getirisi</td>
                                                <td><?= bd_nice_number($advertising->kiraGetirisi) ?> <?php echo $advertising->kiraGetirisiMoneyType ?></td>
                                            </tr>
                                        <?php endif; ?>
                                    <?php else: ?>
<!--                                        --><?php //if (!empty($advertising->squareMeter)): ?>
<!--                                            <tr>-->
<!--                                                <td class="font700">Metrekare Fİyatı</td>-->
<!--                                                <td>--><?// //= money($advertising->price / $advertising->squareMeter, true); ?>
<!--                                                    TL-->
<!--                                                </td>-->
<!--                                            </tr>-->
<!--                                        --><?php //endif; ?>
<!--                                        --><?php //if (!empty($advertising->imarTitle)): ?>
<!--                                            <tr>-->
<!--                                                <td class="font700">İmar Durumu</td>-->
<!--                                                <td>--><?//= $advertising->imarTitle ?><!--</td>-->
<!--                                            </tr>-->
<!--                                        --><?php //endif; ?>
                                        <?php if (!empty($advertising->ada)): ?>
                                            <tr>
                                                <td class="font700">Ada No</td>
                                                <td><?= $advertising->ada ?></td>
                                            </tr>
                                        <?php endif; ?>
                                        <?php if (!empty($advertising->parsel)): ?>
                                            <tr>
                                                <td class="font700">Parsel No</td>
                                                <td><?= $advertising->parsel ?></td>
                                            </tr>
                                        <?php endif; ?>
                                        <?php if (!empty($advertising->pafta)): ?>
                                            <tr>
                                                <td class="font700">Pafta No</td>
                                                <td><?= $advertising->pafta ?></td>
                                            </tr>
                                        <?php endif; ?>
                                        <?php if (!empty($advertising->kaksTitle)): ?>
                                            <tr>
                                                <td class="font700">Kaks (Emsal)</td>
                                                <td><?= $advertising->kaksTitle ?></td>
                                            </tr>
                                        <?php endif; ?>
                                        <?php if (!empty($advertising->gabariTitle)): ?>
                                            <tr>
                                                <td class="font700">Gabari</td>
                                                <td><?= $advertising->gabariTitle ?></td>
                                            </tr>
                                        <?php endif; ?>
                                        <?php if (!empty($advertising->tapuTitle)): ?>
                                            <tr>
                                                <td class="font700">Tapu Drumu</td>
                                                <td><?= $advertising->tapuTitle ?></td>
                                            </tr>
                                        <?php endif; ?>
                                        <?php if (!empty($advertising->kat)): ?>
                                            <tr>
                                                <td class="font700">Kat Karşılığı</td>
                                                <td><?= $advertising->kat == 1 ? 'Evet' : 'Hayır' ?></td>
                                            </tr>
                                        <?php endif; ?>
                                        <?php if (!empty($advertising->kredi)): ?>
                                            <tr>
                                                <td class="font700">Krediye Uygunluk</td>
                                                <td>
                                                    <?php if ($advertising->kredi == 1) echo 'Evet' ?>
                                                    <?php if ($advertising->kredi == 2) echo 'Hayır' ?>
                                                    <?php if ($advertising->kredi == 3) echo 'Bilinmiyor' ?>
                                                </td>
                                            </tr>
                                        <?php endif; ?>
                                        <?php if (!empty($advertising->takas)): ?>
                                            <tr>
                                                <td class="font700">Takaslı</td>
                                                <td>
                                                    <?php if ($advertising->takas == 1) echo 'Evet' ?>
                                                    <?php if ($advertising->takas == 2) echo 'Hayır' ?>
                                                    <?php if ($advertising->takas == 3) echo 'Bilinmiyor' ?>
                                                </td>
                                            </tr>
                                        <?php endif; ?>

                                    <?php endif; ?>

                                    </tbody>
                                </table>

                                <div class="col-xs-12 btns">
                                    <div class="print-box">
                                        <a target="_blank" class="btn btn-info btn-sm" data-toggle="tooltip"
                                           title="Yazdır"
                                           href="ilanlar/yazdir/<?= $advertising->id ?>">
                                            <i class="fa fa-print"></i> YAZDIR
                                        </a>
                                        <a class="btn btn-success btn-sm" data-tooltip="true"
                                           title="Mail Olarak Gönder"
                                           data-toggle="modal"
                                           data-target="#email"
                                           href="#" tabindex="-1">
                                            <i class="fa fa-envelope-o"></i> GÖNDER
                                        </a>
                                        <?php if (! empty($advertising->map)): ?>
                                            <a href="#" tabindex="-1" class="btn btn-warning btn-sm"
                                               data-tooltip="tooltip"
                                               data-toggle="modal" data-target="#mapModal" title="Haritada Göster">
                                                <i class="fa fa-map-marker"></i> HARİTA
                                            </a>
                                        <?php endif; ?>
                                    </div>
                                </div>



                                <div class="col-xs-12 scl-btns">

                                    <div class="share-box">
                                        <p><strong><?php echo lang('content-share-social') ?></strong></p>
                                        <a class="facebook"
                                           href="http://facebook.com/sharer.php?u=<?php echo current_url() ?>"
                                           title="<?php echo lang('content-share-facebook') ?>"><?php echo lang('content-share-facebook') ?></a>
                                        <a class="twitter"
                                           href="https://twitter.com/share?url=<?php echo current_url() ?>&text=<?php echo htmlspecialchars($advertising->title) ?>"
                                           title="<?php echo lang('content-share-twitter') ?>"><?php echo lang
                                            ('content-share-twitter') ?></a>
                                        <a class="google"
                                           href="https://plus.google.com/share?url=<?php echo current_url() ?>"
                                           title="<?php echo lang('content-share-google') ?>"><?php echo lang('content-share-google') ?></a>
                                    </div>
                                </div>
                            </div>


                            <div class="col-sm-7">

                                <ul class="bxsliderBig">
                                    <li>
                                        <?php if (!empty($advertising->image)): ?>
                                            <a href="<?= uploadPath($advertising->image, 'ilanlar') ?>"
                                               rel="gallery" class="galeriListe">
                                                <div class="bxItemBig"
                                                     style="background-image:url('<?= uploadPath($advertising->image, 'ilanlar/thumb') ?>');"></div>
                                            </a>
                                        <?php else: ?>
                                            <a href="public/img/ilan/big.jpg"
                                               rel="gallery" class="galeriListe">
                                                <div class="bxItemBig"
                                                     style="background-image:url('public/img/ilan/show.jpg');"></div>
                                            </a>
                                        <?php endif; ?>
                                    </li>
                                    <?php if (!empty($advertising->images)): ?>
                                        <?php foreach ($advertising->images as $image): ?>
                                            <li>
                                                <a href="<?= uploadPath($image->image, 'ilanlar/normal') ?>"
                                                   rel="gallery" class="galeriListe">
                                                    <div class="bxItemBig"
                                                         style="background-image:url('<?= uploadPath($image->image, 'ilanlar/normal') ?>');"></div>
                                                </a>
                                            </li>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </ul>
                                <div class="clearfix"></div>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="row">
                                            <div id="bx-pager" class="bxsliderThumb1" style="margin-top: 15px;">
                                                <div class="col-md-3 col-sm-4 col-xs-6 thumb-reklam">
                                                    <a data-slide-index="0" href="">
                                                        <div class="bxItemThumb"
                                                             style="background-image:url('<?= ! empty
                                                             ($advertising->image) ? uploadPath($advertising->image, 'ilanlar/thumb') : "public/img/ilan/thumb.jpg" ?>');"></div>
                                                    </a>
                                                </div>

                                                <?php if (!empty($advertising->images)): ?>
                                                    <?php foreach ($advertising->images as $key => $image): ?>
                                                        <div class="col-md-3 col-sm-4 col-xs-6 thumb-reklam">
                                                            <a data-slide-index="<?= $key + 1 ?>" href="">
                                                                <div class="bxItemThumb"
                                                                     style="background-image:url('<?= uploadPath($image->image, 'ilanlar/thumb') ?>');"></div>
                                                            </a>
                                                        </div>
                                                    <?php endforeach; ?>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xs-12">
                                <h4 class="sayfaBaslik" style="color: #003362;">İlan Açıklaması</h4>
                                <?= $advertising->detail ?>
                            </div>

                            <!--divider START-->
                            <div class="col-sm-12 colDivider"></div>
                            <!--divider END-->

                            <div class="col-sm-12">
                                <div class="col-sm-12">

                                    <!-- TAB BUTONLARI -->
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation" class="active">
                                            <a class="font700" href="#ozellikler"
                                               aria-controls="ozellikler" role="tab"
                                               data-toggle="tab"
                                               style="background-color: #005189 !important; color: white;">
                                                Diğer Özellikler
                                            </a>
                                        </li>
<!--                                        --><?php //if (!empty($advertising->map)): ?>
<!--                                            <li role="presentation"><a class="font700" href="#detMap"-->
<!--                                                                       aria-controls="detMap"-->
<!--                                                                       role="tab" data-toggle="tab">Harita</a></li>-->
<!--                                        --><?php //endif; ?>
                                    </ul>

                                    <!-- TAB PANELLERİ -->
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane paddingTop15 active" id="ozellikler">
                                            <?php if (!empty($properties)): ?>
                                                <?php foreach ($properties->allProperties as $property): ?>
                                                    <div
                                                        class="col-sm-3 font700 colorGrey tabOzellikler <?= in_array($property->id, $properties->relations) ? 'active' : 'passive' ?>">
                                                        <i class="fa fa-check-circle"></i><?= $property->title ?>
                                                    </div>
                                                <?php endforeach; ?>
                                            <?php endif; ?>
                                        </div>

                                        <div role="tabpanel" class="tab-pane paddingTop15" id="detMap">
                                            <?php if (!empty($advertising->map)): ?>
                                                <?php $maps = explode(',', $advertising->map); ?>
                                                <div class="map-canvas" data-lat="<?= htmlspecialchars($maps[0]) ?>"
                                                     data-lng="<?= htmlspecialchars($maps[1]) ?>"
                                                     style="height: 400px;"></div>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-xs-12">
                                    <div class="buttons">
                                        <a class="btn btn-xs btn-success" href="javascript:history.back();"><span
                                                class="glyphicon glyphicon-chevron-left"></span> <?php echo lang('content-go-back') ?>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!--row-->
            </div><!--col-sm-12-->
            <?php $this->view('parts/estate') ?>
        </div><!--col-sm-9-->
        <?php $this->view('parts/leftSide') ?>
        <!-- WRAPPER END -->
    </div><!--container-->
</section>