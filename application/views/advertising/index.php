<section id="main">

    <div class="container">
        <!-- WRAPPER START -->

        <div class="col-sm-10 rightSide">



            <div class="col-sm-12 rightSideBack" style="min-height: 800px;">

                <h4 class="sayfaBaslik"><?= $inText.' '.$status ?> <?= $this->input->get('monthly') == 1 ? 'Ayın
                Gayrimenkulleri' : '' ?> <?= $buildText ?></h4>

                <div class="row">
                    <?php $status = $this->input->get('status'); ?>
                    <?php $investor = $this->input->get('investor'); ?>
                    <?php $build = $this->input->get('build'); ?>
                    <?php $monthly = $this->input->get('monthly'); ?>
                    <?php $emergency = $this->input->get('emergency'); ?>
                    <?php $kind = $this->input->get('kind'); ?>
                    <?php $square = ! $this->input->get('square') ? "asc" : '' ?>
                    <?php $square = $this->input->get('square') == "asc" ? "desc" : "asc"; ?>
                    <?php $price = ! $this->input->get('price') ? "asc" : '' ?>
                    <?php $price = $this->input->get('price') == "asc" ? "desc" : "asc"; ?>
                    <div class="col-sm-12 " style="padding: 0; margin-bottom: 50px;">
                        <div class="col-md-3 col-xs-5" style=" padding-right: 0 !important; ">
                            <select name="kind" id="kinds" data-status="<?= $status ?>"
                                    data-build="<?= $build ?>" data-investor="<?= $investor ?>"
                                    data-emergency="<?= $emergency ?>" data-monthly="<?= $monthly ?>"
                                    class="form-control">
                                <option value="">Emlak Türü</option>
                                <option value="1" <?= $kind == 1 ? 'selected' : '' ?>>Konut</option>
                                <option value="2" <?= $kind == 2 ? 'selected' : '' ?>>İş Yeri</option>
                                <option value="3" <?= $kind == 3 ? 'selected' : '' ?>>Arsa</option>
                                <option value="6" <?= $kind == 6 ? 'selected' : '' ?>>Yazlık</option>
                            </select>
                        </div>
                        <div class="col-md-2 col-xs-3">
                        <a href="ilanlar?status=<?= $status ?><?= ! empty($kind) ? '&kind='.$kind : '' ?><?= !empty($emergency) ? '&emergency='.$emergency : '' ?><?= !empty($investor) ? '&investor='.$investor : '' ?><?= ! empty($build) ? '&build='.$build : '' ?><?= ! empty($monthly) ? '&monthly='.$monthly : '' ?>&square=<?= $square ?>" type="button" class="btn btn-sm thmm-btn">
                            M²<?php if ($this->input->get('square') == "asc") echo '<i class="fa fa-angle-down"></i>' ?>
                            <?php if ($this->input->get('square') == "desc") echo '<i class="fa fa-angle-up"></i>' ?>
                        </a>
                            </div>
                        <div class="col-md-2 col-xs-3" style="padding:0 ;">
                        <a href="ilanlar?status=<?= $status ?><?= ! empty($kind) ? '&kind='.$kind : '' ?><?= !empty($emergency) ? '&emergency='.$emergency : '' ?><?= !empty($investor) ? '&investor='.$investor : '' ?><?= ! empty($build) ? '&build='.$build : '' ?><?= ! empty($monthly) ? '&monthly='.$monthly : '' ?>&price=<?= $price ?>"
                           type="button" class="btn btn-sm thmm-btn">
                            Fiyat <?php if ($this->input->get('price') == "asc") echo '<i class="fa fa-angle-down"></i>' ?>
                            <?php if ($this->input->get('price') == "desc") echo '<i class="fa fa-angle-up"></i>' ?>
                        </a>
                         </div>
                    </div>
                <?php if (! empty($advertisings)): ?>
                    <?php foreach ($advertisings as $vision): ?>
                        <?php $image = ! empty($vision->image) ? uploadPath($vision->image, 'ilanlar') : "public/img/ilan/big.jpg" ?>
                        <div class="col-md-4 col-sm-6" style="    margin-bottom: 10px;">
                            <a href="<?= clink(array('@advertising', $vision->slug, $vision->id)) ?>">
                                <div class="box box-main <?= $vision->statusId == 1 ? 'kiralik' : 'satilik' ?>">
                                    <div class="boxBack"
                                         style="background-image:url('<?= $image ?>');">
                                        <!--                                                --><?php //var_dump($vision);die; ?>
                                        <div class="ilan-fiyat">İlan No: <?= $vision->id ?></div>
                                    </div>
                                    <!--<div class="boxName"><h5><? //= $vision->title ?></h5></div> -->
                                    <div class="boxName"><h5> <?= $vision->title ?> </h5></div>
                                    <div class="boxName pull-right"><h5> <?= bd_nice_number($vision->price) ?> <?php echo $vision->moneyType ?></h5></div>
                                    <!--
                                        <div class="boxPrice"><h5>FİYAT : <span><?= bd_nice_number($vision->price) ?>
                                                    TL</span></h5></div>
                                        -->
                                    <div class="boxLoca">
                                        <h6>
                                            <?= $vision->cityTitle ?>,
                                            <?= $vision->townTitle ?>,
                                            <?= $vision->districtTitle ?>
                                        </h6>
                                    </div>
                                </div>
                            </a>
                        </div>
                    <?php endforeach; ?>
                <?php else: ?>

                    <div class="col-xs-12"><p class="help-block">Gösterilecek ilan bulunamamıştır.</p></div>

                <?php endif; ?>





                    <div class="clearfix"></div>

                    <div class="text-center">

                        <?php if (! empty($pagination)) echo $pagination ?>

                    </div>



                </div><!--row-->

            </div><!--col-sm-12-->



<!--            --><?php //$this->view('parts/estate') ?>





        </div><!--col-sm-9-->

        <?php $this->view('parts/leftSide') ?>

        <!-- WRAPPER END -->

    </div><!--container-->

</section>





