<section id="main">
    <div class="container">


            <!-- WRAPPER START -->



            <div class="rightSide col-sm-9">

                <div class="box">
                    <div class="box-header">
                        Emlak Ekle
                    </div>

                    <div class="estate-insert box-body">

                        <?php echo $this->site->alert() ?>



                        <form method="post" action="<?php echo current_url() ?>" enctype="multipart/form-data">
                            <div class="row">



                                <div class="column col-md-6">

                                    <h3>Emlak Bilgileri</h3>

                                    <div class="form-group">
                                        <label for="estate-insert-status">Emlak Durumu *</label>
                                        <?php echo form_dropdown('insertStatus', array('' => '', '1' => 'Kiralık', '2' => 'Satılık'), set_value('insertStatus'), 'id="estate-insert-status" class="form-control" data-placeholder="Seçiniz"') ?>
                                    </div>

                                    <div class="form-group">
                                        <label for="estate-insert-kind">Emlak Türü *</label>
                                        <?php echo form_dropdown('insertKind', prepareForSelect($this->common->kinds(), 'id', 'title', ''), set_value('insertKind'), 'id="estate-insert-kind" class="form-control update-childs" data-url="ajax/types" data-target="#estate-insert-type" data-placeholder="Seçiniz"') ?>
                                    </div>

                                    <div class="form-group">
                                        <label for="estate-insert-type">Emlak Tipi *</label>
                                        <?php echo form_dropdown('insertType', prepareForSelect($this->common->types(set_value('insertKind')), 'id', 'title', ''), set_value('insertType'), 'id="estate-insert-type" class="form-control" data-placeholder="Seçiniz"') ?>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-5">
                                            <?php echo bsFormText('insertPrice', 'Fiyat', array('required' => true,
                                                'class' => 'form-control')) ?>
                                        </div>
                                        <div class="col-md-5">
                                            <?php echo bsFormDropdown('moneyType','Kur Biçimi ',array('options'=>['TL'=>'Türk Lirası','EU' =>'Euro','USD'=>'Dolar'])) ?>
                                        </div>

                                    </div>



                                    <div class="form-group">
                                        <label for="estate-insert-description">Açıklama *</label>
                                        <textarea name="insertDescription" id="estate-insert-description" class="form-control" rows="5"><?php echo set_value('insertDescription') ?></textarea>
                                    </div>

                                    <div class="form-group">
                                        <label for="estate-insert-room">Oda Sayısı</label>
                                        <?php echo form_dropdown('insertRoom', prepareForSelect($this->common->rooms(), 'id', 'title', ''), set_value('insertRoom'), 'id="estate-insert-room" class="form-control" data-placeholder="Seçiniz"') ?>
                                    </div>

                                    <div class="form-group">
                                        <label for="estate-insert-bath">Banyo Sayısı</label>
                                        <input type="text" name="insertBath" id="estate-insert-bath" class="form-control numeric" value="<?php echo set_value('insertBath') ?>" />
                                    </div>

                                    <div class="form-group">
                                        <label for="estate-insert-squaremeter">Metrekare (m2)</label>
                                        <input type="text" name="insertSquaremeter" id="estate-insert-squaremeter" class="form-control numeric" value="<?php echo set_value('insertSquaremeter') ?>" />
                                    </div>

                                    <div class="form-group">
                                        <label for="estate-insert-floor">Bulunduğu Kat</label>
                                        <?php echo form_dropdown('insertFloor', prepareForSelect($this->common->floors(), 'id', 'title', ''), set_value('insertFloor'), 'id="estate-insert-floor" class="form-control" data-placeholder="Seçiniz"') ?>
                                    </div>

                                    <div class="form-group">
                                        <label for="estate-insert-storey-count">Binadaki Kat Sayısı</label>
                                        <input type="text" name="insertStoreyCount" id="estate-insert-storey-count" class="form-control numeric" value="<?php echo set_value('insertStoreyCount') ?>" />
                                    </div>

                                    <div class="form-group">
                                        <label for="estate-insert-building-age">Binanın Yaşı</label>
                                        <?php echo form_dropdown('insertBuildingAge', prepareForSelect($this->common->buildingAges(), 'id', 'title', ''), set_value('insertBuildingAge'), 'id="estate-insert-building-age" class="form-control" data-placeholder="Seçiniz"') ?>
                                    </div>

                                    <div class="form-group">
                                        <label for="estate-insert-heating">Isınma Şekli</label>
                                        <?php echo form_dropdown('insertHeating', prepareForSelect($this->common->heatings(), 'id', 'title', ''), set_value('inserHeating'), 'id="estate-insert-heating" class="form-control" data-placeholder="Seçiniz"') ?>
                                    </div>


                                    <div class="form-group">
                                        <label for="estate-insert-usage-status">Kullanım Durumu</label>
                                        <?php echo form_dropdown('insertUsageStatus', prepareForSelect($this->common->usageStatuses(), 'id', 'title', ''), set_value('insertUsageStatus'), 'id="estate-insert-usage-status" class="form-control" data-placeholder="Seçiniz"') ?>
                                    </div>

                                </div>




                                <div class="column col-md-6">
                                    <h3>Konum Bilgileri</h3>

                                    <div class="form-group">
                                        <label for="estate-insert-city">Şehir *</label>
                                        <?php echo form_dropdown('insertCity', prepareForSelect($this->common->cities(), 'id', 'title', ''), set_value('insertCity'), 'id="estate-insert-city" class="form-control update-childs" data-url="ajax/towns" data-target="#estate-insert-town" data-placeholder="Seçiniz"') ?>
                                    </div>

                                    <div class="form-group">
                                        <label for="estate-insert-town">İlçe *</label>
                                        <?php echo form_dropdown('insertTown', prepareForSelect($this->common->towns(set_value('insertCity')), 'id', 'title', ''), set_value('insertTown'), 'id="estate-insert-town" class="form-control update-childs" data-url="ajax/districts" data-target="#estate-insert-district" data-placeholder="Seçiniz"') ?>
                                    </div>

                                    <div class="form-group">
                                        <label for="estate-insert-district">Semt *</label>
                                        <?php echo form_dropdown('insertDistrict', prepareForSelect($this->common->districts(set_value('insertTown')), 'id', 'title', ''), set_value('insertDistrict'), 'id="estate-insert-district" class="form-control" data-placeholder="Seçiniz"') ?>
                                    </div>



                                    <p>&nbsp;</p>
                                    <h3>Kişisel Bilgiler</h3>

                                    <div class="form-group">
                                        <label for="estate-insert-fullname">Adınız Soyadınız  *</label>
                                        <input type="text" name="insertFullname" id="estate-insert-fullname" class="form-control" value="<?php echo set_value('insertFullname') ?>" />
                                    </div>

                                    <div class="form-group">
                                        <label for="estate-insert-email">E-Mail *</label>
                                        <input type="email" name="insertEmail" id="estate-insert-email" class="form-control" value="<?php echo set_value('insertEmail') ?>" />
                                    </div>
                                    <div class="form-group">
                                        <label for="phone">Telefon *</label>
                                        <input type="text" name="insertPhone" id="estate-insert-phone" class="form-control mask-phone" value="<?php echo set_value('insertPhone') ?>" />
                                    </div>

                                    <div class="form-group">
                                        <label for="estate-insert-message">Mesajınız</label>
                                        <textarea name="insertMessage" id="estate-insert-message" class="form-control" rows="5"><?php echo set_value('insertMessage') ?></textarea>
                                    </div>



                                    <p>&nbsp;</p>
                                    <h3>Emlak Özellikleri</h3>
                                    <div class="form-group">
                                        <?php echo form_multiselect('properties[]', prepareForSelect($this->common->properties(), 'id', 'title'), set_value('properties[]'), 'class="form-control" size="10"'); ?>
                                        <div class="help-block"><kbd>Ctrl</kbd> tuşuna basılı tutarak birden fazla kategori seçebilirsiniz.</div>
                                    </div>



                                    <p>&nbsp;</p>
                                    <h3>Emlak Resmi</h3>
                                    <p>Resim boyutları en az 600x450px olmalıdır. Resim dosya formatı PNG, GIF veya JPG olabilir.</p>
                                    <input class="filestyle" type="file" name="insertImage" id="file" data-buttonText="Resim Seçin" data-icon="false">
                                </div>

                            </div>


                            <div class="form-group text-center">
                                <p>&nbsp;</p>
                                <button type="submit" class="btn btn-success">İlanı Gönder</button>
                            </div>
                        </form>


                    </div>

                </div>

            </div>
    
            <?php $this->view('parts/leftSide') ?>
	</div>
</section>


