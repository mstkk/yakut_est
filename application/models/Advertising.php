<?php


class Advertising extends  CI_Model
{

    protected $vt = 'advertisings';

    public function findId($id, $countUp = false)
    {
        $first = $this->db->from($this->vt)->where('id', $id)->get()->row();

        if ($first->estateKindId == 3) {

            $this->db
                ->select("{$this->vt}.*,
                imar.title imarTitle,
                kaks.title kaksTitle,
                gabari.title gabariTitle,
                tapu.title tapuTitle", false);
            $this->db
                ->join("imar", "imar.id = {$this->vt}.imar", 'left')
                ->join("kaks", "kaks.id = {$this->vt}.kaks", 'left')
                ->join("gabari", "gabari.id = {$this->vt}.gabari", 'left')
                ->join("tapu", "tapu.id = {$this->vt}.tapu", 'left');

        } else {

            $this->db
                ->select("{$this->vt}.*,
                floors.title floorTitle,
                building_ages.title ageTitle,
                heatings.title heatingTitle,
                usage_statuses.title usageStatusTitle,", false);

            $this->db
                ->join('floors', 'floors.id = advertisings.floorId', 'left')
                ->join('building_ages', 'building_ages.id = advertisings.ageId', 'left')
                ->join('heatings', 'heatings.id = advertisings.heatId', 'left')
                ->join('usage_statuses', 'usage_statuses.id = advertisings.usageId', 'left');

        }
        $result = $this->db
            ->select("{$this->vt}.*,
                cities.title cityTitle,
                towns.title townTitle,
                districts.title localeTitle,
                kinds.title kindTitle,
                types.title typeTitle,
                ", false)
            ->from($this->vt)
            ->join('cities', 'cities.id = advertisings.cityId', "left")
            ->join('towns', 'towns.id = advertisings.townId', "left")
            ->join('districts', 'districts.id = advertisings.localeId', "left")
            ->join('kinds', 'kinds.id = advertisings.estateKindId', "left")
            ->join('types', 'types.id = advertisings.estateTypeId', "left")
            ->where("{$this->vt}.id", $id)
            ->where('isActive', '1')
            ->get()
            ->row();

        $result->roomTitle = '';
        if ($result->roomId != 0) {
            $room = $this->db
                ->from('rooms')
                ->where('id', $result->roomId)
                ->get()
                ->row();

            $result->roomTitle = $room->title;
        }

        $result->images = $this->images($id);

        if ($countUp === true) $this->countUp($id);

        return $result;
    }

    public function getKindname($id)
    {
        return $this->db
            ->from('kinds')
            ->where('id',$id)
            ->get()
            ->row();
    }

    public function yazdir($id, $countUp = false)
    {
        $first = $this->db->from($this->vt)->where('id', $id)->get()->row();

        if ($first->estateKindId == 3) {

            $this->db
                ->select("{$this->vt}.*,
                imar.title imarTitle,
                kaks.title kaksTitle,
                gabari.title gabariTitle,
                tapu.title tapuTitle", false);
            $this->db
                ->join("imar", "imar.id = {$this->vt}.imar", 'left')
                ->join("kaks", "kaks.id = {$this->vt}.kaks", 'left')
                ->join("gabari", "gabari.id = {$this->vt}.gabari", 'left')
                ->join("tapu", "tapu.id = {$this->vt}.tapu", 'left');

        } else {

            $this->db
                ->select("{$this->vt}.*,
                floors.title floorTitle,
                building_ages.title ageTitle,
                heatings.title heatingTitle,
                usage_statuses.title usageStatusTitle,", false);

            $this->db
                ->join('floors', 'floors.id = advertisings.floorId', 'left')
                ->join('building_ages', 'building_ages.id = advertisings.ageId', 'left')
                ->join('heatings', 'heatings.id = advertisings.heatId', 'left')
                ->join('usage_statuses', 'usage_statuses.id = advertisings.usageId', 'left');

        }
        $result = $this->db
            ->select("{$this->vt}.*,
                cities.title cityTitle,
                towns.title townTitle,
                districts.title localeTitle,
                kinds.title kindTitle,
                types.title typeTitle,
                ", false)
            ->from($this->vt)
            ->join('cities', 'cities.id = advertisings.cityId', "left")
            ->join('towns', 'towns.id = advertisings.townId', "left")
            ->join('districts', 'districts.id = advertisings.localeId', "left")
            ->join('kinds', 'kinds.id = advertisings.estateKindId', "left")
            ->join('types', 'types.id = advertisings.estateTypeId', "left")
            ->where("{$this->vt}.id", $id)
            ->get()
            ->row();

        $result->roomTitle = '';
        if ($result->roomId != 0) {
            $room = $this->db
                ->from('rooms')
                ->where('id', $result->roomId)
                ->get()
                ->row();

            $result->roomTitle = $room->title;
        }


        if ($countUp === true) $this->countUp($id);

        return $result;
    }


    public function sendMail($id, $countUp = false)
    {
        $first = $this->db->from($this->vt)->where('id', $id)->get()->row();

        if ($first->estateKindId == 3) {

            $this->db
                ->select("{$this->vt}.*,
                imar.title imarTitle,
                kaks.title kaksTitle,
                gabari.title gabariTitle,
                tapu.title tapuTitle", false);
            $this->db
                ->join("imar", "imar.id = {$this->vt}.imar", 'left')
                ->join("kaks", "kaks.id = {$this->vt}.kaks", 'left')
                ->join("gabari", "gabari.id = {$this->vt}.gabari", 'left')
                ->join("tapu", "tapu.id = {$this->vt}.tapu", 'left');

        } else {

            $this->db
                ->select("{$this->vt}.*,
                floors.title floorTitle,
                building_ages.title ageTitle,
                heatings.title heatingTitle,
                usage_statuses.title usageStatusTitle,", false);

            $this->db
                ->join('floors', 'floors.id = advertisings.floorId', 'left')
                ->join('building_ages', 'building_ages.id = advertisings.ageId', 'left')
                ->join('heatings', 'heatings.id = advertisings.heatId', 'left')
                ->join('usage_statuses', 'usage_statuses.id = advertisings.usageId', 'left');

        }
        $result = $this->db
            ->select("{$this->vt}.*,
                cities.title cityTitle,
                towns.title townTitle,
                districts.title localeTitle,
                kinds.title kindTitle,
                types.title typeTitle,
                ", false)
            ->from($this->vt)
            ->join('cities', 'cities.id = advertisings.cityId', "left")
            ->join('towns', 'towns.id = advertisings.townId', "left")
            ->join('districts', 'districts.id = advertisings.localeId', "left")
            ->join('kinds', 'kinds.id = advertisings.estateKindId', "left")
            ->join('types', 'types.id = advertisings.estateTypeId', "left")
            ->where("{$this->vt}.id", $id)
            ->get()
            ->row();

        $result->roomTitle = '';
        if ($result->roomId != 0) {
            $room = $this->db
                ->from('rooms')
                ->where('id', $result->roomId)
                ->get()
                ->row();

            $result->roomTitle = $room->title;
        }

        $this->load->library('email');
        $this->email->initialize(array(
            'smtp_host' => $this->site->get('smtpHost'),
            'smtp_port' => $this->site->get('smtpPort'),
            'smtp_user' => $this->site->get('smtpUser'),
            'smtp_pass' => $this->site->get('smtpPass'),
        ));

        $data = array('advertising' => $result,'properties' => $this->advertising->propertyRelations($id));

        $this->email->from($this->site->get('smtpUser'), $this->site->get('smtpUser'));
        $this->email->to($this->input->post('email'));
        $this->email->subject('Nazar Emlak İlan');
        $this->email->message($this->load->view('mail/estate', $data, true));

        
        if (! $this->email->send()) {
            return false;
        } else {
            return true;
        }
    }

    private function images($id)
    {
        return $this->db->from('advertising_images')
            ->where('advertisingId', $id)
            ->order_by('order', 'asc')
            ->get()
            ->result();
    }

    public function getCity()
    {
        return $this->db
            ->from('cities')
            ->order_by('order', 'asc')
            ->order_by('id', 'asc')
            ->get()
            ->result();
    }

    public function getTowns($cityId)
    {
        return $this->db
            ->from('towns')
            ->where('cityId', $cityId)
            ->order_by('order', 'asc')
            ->order_by('id', 'asc')
            ->get()
            ->result();
    }

    public function getDistricts($townId)
    {
        return $this->db
            ->from('districts')
            ->where('townId', $townId)
            ->order_by('order', 'asc')
            ->order_by('id', 'asc')
            ->get()
            ->result();
    }

    public function getKinds()
    {
        return $this->db
            ->from('kinds')
            ->order_by('order', 'asc')
            ->order_by('id', 'asc')
            ->get()
            ->result();
    }

    public function getAges()
    {
        return $this->db
            ->from('building_ages')
            ->order_by('order', 'asc')
            ->get()
            ->result();
    }
    public function getRoomname($id)
    {
        return $this->db
            ->from('rooms')
            ->where('id', $id)
            ->get()
            ->row();
    }

    private function consultant($id)
    {
        return $this->db
            ->from('profiles')
            ->where('id', $id)
            ->get()
            ->row();
    }

    public function investor()
    {

        $result =  $this->db
            ->select("{$this->vt}.*,towns.title townTitle, cities.title cityTitle,", false)
            ->from($this->vt)
            ->join('cities', 'cities.id = advertisings.cityId', "left")
            ->join('towns', 'towns.id = advertisings.townId', "left")
            ->where('investor', 1)
            ->where('isActive', '1')
            ->order_by('date', 'desc')
            ->order_by('id', 'desc')
            ->limit(1)
            ->get()
            ->row();

        return $result;
    }

    public function newBuild()
    {

        $result =  $this->db
            ->select("{$this->vt}.*,towns.title townTitle, cities.title cityTitle,", false)
            ->from($this->vt)
            ->join('cities', 'cities.id = advertisings.cityId', "left")
            ->join('towns', 'towns.id = advertisings.townId', "left")
            ->order_by('date', 'desc')
            ->order_by('id', 'desc')
            ->where('new_build', 1)
            ->where('isActive', '1')
            ->limit(1)
            ->get()
            ->row();

        return $result;
    }

    public function all($limit = null, $offset = null, $emergency = false, $status = null, $investor = null,
                        $newbuild = null, $kind = null)
    {

        if ($limit != null) {
            $this->db->limit($limit, $offset);
        }

  
       
        if ($emergency === true || $this->input->get('emergency') == 1) $this->db->where('emergency', 1);
        if ($status !== null) $this->db->where('statusId', $status);
        if ($kind != null) $this->db->where('estateKindId', $kind);
        if ($newbuild !== null) $this->db->where('new_build', 1);
        if ($square = $this->input->get('square')) $this->db->order_by('squareMeter', $square);
        if ($price = $this->input->get('price')) $this->db->order_by('price', $price);
        if ($investor !== null) $this->db->where('investor', 1);

        return $this->db
            ->select("{$this->vt}.*,towns.title townTitle, cities.title cityTitle, districts.title districtTitle",
                false)
            ->from($this->vt)
            ->join('cities', 'cities.id = advertisings.cityId', "left")
            ->join('towns', 'towns.id = advertisings.townId', "left")
            ->join('districts', 'districts.id = advertisings.localeId', "left")
            ->order_by('date', 'desc')
            ->where('isActive', '1')
            ->order_by('id', 'desc')
            ->get()
            ->result();
    }

    public function count($emergency = false, $status = null, $investor = null,
                          $newbuild = null)
    {
   



        if ($emergency === true || $this->input->get('emergency') == 1) $this->db->where('emergency', 1);
        if ($status !== null) $this->db->where('statusId', $status);
        if ($newbuild !== null) $this->db->where('new_build', 1);
        if ($investor !== null) $this->db->where('investor', 1);
        return $this->db
            ->from('advertisings')
            ->where('language', $this->language)
            ->count_all_results();
    }

    public function vision($limit = null, $offset = null, $emergency = false)
    {

        if ($limit != null) {
            $this->db->limit($limit, $offset);
        }

        if ($emergency === true) $this->db->where('emergency', 1);

        $result =  $this->db
            ->select("{$this->vt}.*,towns.title townTitle, cities.title cityTitle, districts.title districtTitle,
            types.title typeTitle", false)
            ->from($this->vt)
            ->join('cities', 'cities.id = advertisings.cityId', "left")
            ->join('towns', 'towns.id = advertisings.townId', "left")
            ->join('districts', 'districts.id = advertisings.localeId', "left")
            ->join("types", "types.id = {$this->vt}.estateTypeId", "left")
            ->where('isActive', '1')
            ->order_by('date', 'desc')
            ->order_by('id', 'desc')
            ->get()
            ->result();

        return $result;
    }
    public function vision2($limit = null, $offset = null, $emergency = false)
    {

        if ($limit != null) {
            $this->db->limit($limit, $offset);
        }

        if ($emergency === true) $this->db->where('emergency', 1);

        $result =  $this->db
            ->select("{$this->vt}.*,towns.title townTitle, cities.title cityTitle, districts.title districtTitle,
            types.title typeTitle", false)
            ->from($this->vt)
            ->join('cities', 'cities.id = advertisings.cityId', "left")
            ->join('towns', 'towns.id = advertisings.townId', "left")
            ->join('districts', 'districts.id = advertisings.localeId', "left")
            ->join("types", "types.id = {$this->vt}.estateTypeId", "left")
            ->where('vitrinIlan', "1")
            ->where('isActive', '1')
            ->order_by('date', 'desc')
            ->order_by('id', 'desc')
            ->get()
            ->result();

        return $result;
    }

    public function filter ($limit = null, $offset = null)
    {
        $ids = array();
        if ($this->input->get('properties')) {
            $properties = $this->input->get('properties');

            $advs = $this->db
                ->select("advertise_properties.estateId", false)
                ->from('advertise_properties')
                ->where_in('propertyId', $properties)
                ->order_by('id', 'asc')
                ->get()
                ->result();

            foreach ($advs as $key => $item) {
                $ids[] = $item->estateId;
            }

        }

        if ($limit != null) {
            $this->db->limit($limit, $offset);
        }

        if ($this->input->get('estate')) {
            $this->db->like('advertisings.title', $this->input->get('estate'));
            $this->db->or_like('advertisings.id', $this->input->get('estate'));
            $this->db->or_like('advertisings.detail', turn($this->input->get('estate')));
        }

        if ($this->input->get('status')) {
            $this->db->where('advertisings.statusId', $this->input->get('status'));
        }

        if ($this->input->get('kind')) {
            $this->db->where('advertisings.estateKindId', $this->input->get('kind'));
        }

        if ($this->input->get('age')) {
            $this->db->where('advertisings.ageId', $this->input->get('age'));
        }

        if ($this->input->get('city')) {
            $this->db->where('advertisings.cityId', $this->input->get('city'));
        }

        if ($this->input->get('town')) {
            $this->db->where('advertisings.townId', $this->input->get('town'));
        }

        if ($this->input->get('locale')) {
            $this->db->where('advertisings.localeId', $this->input->get('locale'));
        }

        if ($this->input->get('pricemin')) {
            $this->db->where('advertisings.price >=', $this->input->get('pricemin'));
        }

        if ($this->input->get('pricemax')) {
            $this->db->where('advertisings.price <=', $this->input->get('pricemax'));
        }

        $sortPrice = $this->input->get('sortPrice');

        if ($sortPrice == 'asc' || $sortPrice = 'desc') {
            $this->db->order_by('advertisings.price', $sortPrice);
        } else {
            $this->db->order_by('advertisings.price', 'desc');
        }
        if ($this->input->get('properties')) {

            $result = $this->db
                ->select('advertisings.*, districts.title districtTitle, types.title typeTitle, advertisings.price, advertisings.image, rooms.title roomTitle, towns.title townTitle, cities.title cityTitle', false)
                ->from('advertisings')
                ->join('districts', 'districts.id = advertisings.localeId', "left")
                ->join('types', 'types.id = advertisings.estateTypeId', "left")
                ->join('cities', 'cities.id = advertisings.cityId', "left")
                ->join('towns', 'towns.id = advertisings.townId', "left")
                ->join('rooms', 'rooms.id = advertisings.roomId', "left")
                ->where_in('advertisings.id', $ids)
                ->get()
                ->result();

        } else {

            $result = $this->db
                ->select('advertisings.*, districts.title districtTitle, types.title typeTitle, advertisings.price, advertisings.image, rooms.title roomTitle, towns.title townTitle, cities.title cityTitle', false)
                ->from('advertisings')
                ->join('districts', 'districts.id = advertisings.localeId', "left")
                ->join('types', 'types.id = advertisings.estateTypeId', "left")
                ->join('cities', 'cities.id = advertisings.cityId', "left")
                ->join('towns', 'towns.id = advertisings.townId', "left")
                ->join('rooms', 'rooms.id = advertisings.roomId', "left")
                ->get()
                ->result();
        }

        return $result;

    }



    public function filterCount()
    {
        $ids = array();
        if ($this->input->get('properties')) {
            $properties = $this->input->get('properties');

            $advs = $this->db
                ->select("advertise_properties.estateId", false)
                ->from('advertise_properties')
                ->where_in('propertyId', $properties)
                ->get()
                ->result();

            foreach ($advs as $key => $item) {
                $ids[] = $item->estateId;
            }

        }
        if ($this->input->get('estate')) {
            $this->db->like('advertisings.title', $this->input->get('estate'));
            $this->db->or_like('advertisings.id', $this->input->get('estate'));
            $this->db->or_like('advertisings.detail', turn($this->input->get('estate')));

        }

        if ($this->input->get('status')) {
            $this->db->where('advertisings.statusId', $this->input->get('status'));
        }

        if ($this->input->get('kind')) {
            $this->db->where('advertisings.estateKindId', $this->input->get('kind'));
        }

        if ($this->input->get('age')) {
            $this->db->where('advertisings.ageId', $this->input->get('age'));
        }

        if ($this->input->get('city')) {
            $this->db->where('advertisings.cityId', $this->input->get('city'));
        }

        if ($this->input->get('town')) {
            $this->db->where('advertisings.townId', $this->input->get('town'));
        }

        if ($this->input->get('locale')) {
            $this->db->where('advertisings.localeId', $this->input->get('locale'));
        }

        if ($this->input->get('pricemin')) {
            $this->db->where('advertisings.price >=', $this->input->get('pricemin'));
        }

        if ($this->input->get('pricemax')) {
            $this->db->where('advertisings.price <=', $this->input->get('pricemax'));
        }

        $lastResults = array();
        $result = $this->db
            ->select('*')
            ->from('advertisings')
            ->where('isActive', '1')
            ->get()
            ->result();

        if ($this->input->get('properties')) {
            foreach ($result as $key => $item) {
                if (in_array($item->id, $ids)) {
                    $lastResults[] = $item;
                }
            }

            return count($lastResults);
        }

        return count((array) $result);
    }

    public function propertyRelations($id)
    {
        $properties = $this->properties();

        $relations = $this->db
            ->from('advertise_properties')
            ->where('estateId', $id)
            ->get()
            ->result();

        $prop_ids = array();

        foreach ($relations as $item) {
            $prop_ids[] = $item->propertyId;
        }

        return (object) array(
            'relations' => $prop_ids,
            'allProperties' => $properties
        );
    }

    protected function properties()
    {
        return $this->db
            ->from('properties')
            ->order_by('order', 'asc')
            ->order_by('id', 'asc')
            ->get()
            ->result();
    }

    protected function countUp($id)
    {
        $adv = $this->db->from($this->vt)->where('id', $id)->get()->row();

        $count = $adv->views + 1;

        $this->db->where('id', $id)->update($this->vt, [
            'views' => $count
        ]);
    }



    public function insert($data)
    {
        $posts = $this->input->post();
        $data = array(
            'fullname' => $this->input->post('insertFullname'),
            'email' => $this->input->post('insertEmail'),
            'phone' => $this->input->post('insertPhone'),
            'message' => $this->input->post('insertMessage'),
            'cityId' => $this->input->post('insertCity'),
            'townId' => $this->input->post('insertTown'),
            'districtId' => $this->input->post('insertDistrict'),
            'estateStatus' => $this->input->post('insertStatus'),
            'kindId' => $this->input->post('insertKind'),
            'typeId' => $this->input->post('insertType'),
            'price' => $this->input->post('insertPrice'),
            'image' => $data['name'],
            'description' => $this->input->post('insertDescription'),
            'roomId' => empty($posts['insertRoom']) ? null : $posts['insertRoom'],
            'bath' => $this->input->post('insertBath'),
            'squaremeter' => $this->input->post('insertSquaremeter'),
            'floorId' =>  empty($posts['insertFloor']) ? null : $posts['insertFloor'],
            'storeyCount' => $this->input->post('insertStoreyCount'),
            'buildingAgeId' => empty($posts['insertBuildingAge']) ? null : $posts['insertBuildingAge'],
            'heatingId' => $this->input->post('insertHeating'),
            'usageStatusId' => empty($posts['insertUsageStatus']) ? null : $posts['insertUsageStatus'],
            'properties' => json_encode($this->input->post('properties')),
            'date' => $this->date->set()->mysqlDatetime(),
            'ip' => $this->input->ip_address(),
            'moneyType'=>$this->input->post('moneyType')
        );

        $this->db->insert('estate_inserts', $data);
        $success = $this->db->insert_id();


        if ($success && $this->module->arguments->notification == true) {
            $this->load->library('email');
            $this->email->initialize(array(
                'smtp_host' => $this->site->get('smtpHost'),
                'smtp_port' => $this->site->get('smtpPort'),
                'smtp_user' => $this->site->get('smtpUser'),
                'smtp_pass' => $this->site->get('smtpPass'),
            ));

            $this->email->from($this->input->post('email'), htmlspecialchars($this->input->post('fullname')));
            $this->email->to($this->module->arguments->notificationMail);
            $this->email->subject('İlan Ekleme Bildirimi');
            $this->email->message($this->load->view('advertising/insertmail', $data, true));
            $this->email->send();
        }

        return $success;
    }

    public function counts()
    {
        $counts = (object) array();

        // Satılık Konut Sayısı
        $counts->s_konut = $this->db
            ->from('advertisings')
            ->where('statusId', 2)
            ->where('estateKindId', 1)
            ->where('isActive', 1)
            ->count_all_results();

        // Satılık İş Yeri Sayısı
        $counts->s_isyeri = $this->db
            ->from('advertisings')
            ->where('statusId', 2)
            ->where('estateKindId', 2)
            ->where('isActive', 1)
            ->count_all_results();

        // Satılık Arsa Sayısı
        $counts->s_arsa = $this->db
            ->from('advertisings')
            ->where('statusId', 2)
            ->where('estateKindId', 3)
            ->where('isActive', 1)
            ->count_all_results();

        // Satılık Yazlık Sayısı
        $counts->s_yazlik = $this->db
            ->from('advertisings')
            ->where('statusId', 2)
            ->where('isActive', 1)
            ->where('estateKindId', 6)
            ->count_all_results();

        // Kiralık Konut Sayısı
        $counts->k_konut = $this->db
            ->from('advertisings')
            ->where('statusId', 1)
            ->where('estateKindId', 1)
            ->where('isActive', 1)
            ->count_all_results();

        // Kiralık İş Yeri Sayısı
        $counts->k_isyeri = $this->db
            ->from('advertisings')
            ->where('statusId', 1)
            ->where('estateKindId', 2)
            ->where('isActive', 1)
            ->count_all_results();

        // Kiralık Arsa Sayısı
        $counts->k_arsa = $this->db
            ->from('advertisings')
            ->where('statusId', 1)
            ->where('estateKindId', 3)
            ->where('isActive', 1)
            ->count_all_results();

        // Kiralık Yazlık Sayısı
        $counts->k_yazlik = $this->db
            ->from('advertisings')
            ->where('statusId', 1)
            ->where('estateKindId', 6)
            ->where('isActive', 1)
            ->count_all_results();

        return $counts;
    }





}