<?php

class Expertise extends CI_Model
{
    public function insert()
    {
        $data = array(
            't_sube' => $this->input->post('t_sube'),
            't_fullname' => $this->input->post('t_fullname'),
            't_email' => $this->input->post('t_email'),
            't_phone' => $this->input->post('t_phone'),
            'g_city' => $this->input->post('g_city'),
            'g_town' => $this->input->post('g_town'),
            'g_address' => $this->input->post('g_address'),
            'g_pafta' => $this->input->post('g_pafta'),
            'g_ada' => $this->input->post('g_ada'),
            'g_parsel' => $this->input->post('g_parsel'),
            'k_fullname' => $this->input->post('k_fullname'),
            'k_phone' => $this->input->post('k_phone'),
            'm_fullname' => $this->input->post('m_fullname'),
            'm_vergiD' => $this->input->post('m_vergiD'),
            'm_vergiNo' => $this->input->post('m_vergiNo'),
            'm_address' => $this->input->post('m_address'),
            'm_comment' => $this->input->post('m_comment'),
            'date' => $this->date->set()->mysqlDatetime(),
            'ip' => $this->input->ip_address()
        );

        $this->db->insert('contacts', $data);
        $success = $this->db->insert_id();


        if ($success && $this->module->arguments->notification == true) {
            $this->load->library('email');
            $this->email->initialize(array(
                'smtp_host' => $this->site->get('smtpHost'),
                'smtp_port' => $this->site->get('smtpPort'),
                'smtp_user' => $this->site->get('smtpUser'),
                'smtp_pass' => $this->site->get('smtpPass'),
            ));

            $this->email->from($this->input->post('email'), htmlspecialchars($this->input->post('fullname')));
            $this->email->to($this->module->arguments->notificationMail);
            $this->email->subject('Expertiz Talep Formu');
            $this->email->message($this->load->view('expertise/mail', $data, true));
            $this->email->send();
        }

        return $success;
    }



}