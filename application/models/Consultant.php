<?php


class Consultant extends  CI_Model
{

    protected $vt = 'profiles';

    public function findId($id, $countUp = false)
    {
        $result = $this->db
            ->from($this->vt)
            ->where('id', $id)
            ->get()
            ->row();

        return $result;
    }

    private function images($id)
    {
        return $this->db->from('advertising_images')
            ->where('advertisingId', $id)
            ->order_by('order', 'asc')
            ->get()
            ->result();
    }

    public function all($limit = null, $offset = null, $emergency = false) {

        if ($limit != null) {
            $this->db->limit($limit, $offset);
        }

        return $this->db
            ->from($this->vt)
            ->order_by('order', 'desc')
            ->order_by('id', 'desc')
            ->get()
            ->result();
    }

    public function count()
    {
        return $this->db
            ->from('profiles')
            ->where('language', $this->language)
            ->count_all_results();
    }

    public function vision($id, $limit = null, $offset = null, $emergency = false)
    {
        $result = $this->db
            ->from($this->vt)
            ->where('id', $id)
            ->get()
            ->row();

        if ($limit != null) {
            $this->db->limit($limit, $offset);
        }

        if ($emergency === true) {
            $this->db->where('emergency', 1);
        } else {
            $this->db->where('consultantId', $id);
        }

        $result->adverts = $this->db
            ->select("advertisings.*,rooms.title roomTitle, towns.title townTitle, cities.title cityTitle,", false)
            ->from('advertisings')
            ->join('cities', 'cities.id = advertisings.cityId')
            ->join('towns', 'towns.id = advertisings.townId')
            ->join('rooms', 'rooms.id = advertisings.roomId')
            ->where('situation', 1)
            ->order_by('order', 'desc')
            ->order_by('id', 'desc')
            ->get()
            ->result();

        return $result;
    }

    public function propertyRelations($id)
    {
        $properties = $this->properties();

        $relations = $this->db
            ->from('advertise_properties')
            ->where('estateId', $id)
            ->get()
            ->result();

        $prop_ids = array();

        foreach ($relations as $item) {
            $prop_ids[] = $item->propertyId;
        }

        return (object) array(
            'relations' => $prop_ids,
            'allProperties' => $properties
        );
    }

    protected function properties()
    {
        return $this->db
            ->from('properties')
            ->order_by('order', 'asc')
            ->order_by('id', 'asc')
            ->get()
            ->result();
    }

    protected function countUp($id)
    {
        $adv = $this->db->from($this->vt)->where('id', $id)->get()->row();

        $count = $adv->views + 1;

        $this->db->where('id', $id)->update($this->vt, [
            'views' => $count
        ]);
    }





}