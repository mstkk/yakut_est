<?php


class Common extends  CI_Model
{
    private $cities = array();
    private $towns = array();
    private $districts = array();
    private $kinds = array();
    private $types = array();
    private $rooms = array();
    private $floors = array();
    private $buildingAges = array();
    private $heatings = array();
    private $usageStatuses = array();
    private $properties = array();



    public function cities()
    {
        if (empty($this->cities)) {
            $this->cities = $this->db
                ->from('cities')
                ->order_by('order', 'asc')
                ->get()
                ->result();
        }

        return $this->cities;
    }



    public function towns($city)
    {
        if (is_object($city)) {
            $city = $city->id;
        }

        if (empty($this->towns[$city])) {
            $this->towns[$city] = $this->db
                ->from('towns')
                ->where('cityId', $city)
                ->order_by('order', 'asc')
                ->get()
                ->result();
        }

        return $this->towns[$city];
    }


    public function districts($town)
    {
        if (is_object($town)) {
            $town = $town->id;
        }

        if (empty($this->districts[$town])) {
            $this->districts[$town] = $this->db
                ->from('districts')
                ->where('townId', $town)
                ->order_by('order', 'asc')
                ->get()
                ->result();
        }

        return $this->districts[$town];
    }




    public function kinds()
    {
        if (empty($this->kinds)) {
            $this->kinds = $this->db
                ->from('kinds')
                ->order_by('order', 'asc')
                ->get()
                ->result();
        }

        return $this->kinds;
    }



    public function types($kind)
    {
        if (is_object($kind)) {
            $kind = $kind->id;
        }

        if (empty($this->types[$kind])) {
            $this->types[$kind] = $this->db
                ->from('types')
                ->where('kindId', $kind)
                ->order_by('order', 'asc')
                ->get()
                ->result();
        }

        return $this->types[$kind];
    }

    public function rooms()
    {
        if (empty($this->rooms)) {
            $this->rooms = $this->db
                ->from('rooms')
                ->order_by('order', 'asc')
                ->get()
                ->result();
        }

        return $this->rooms;
    }



    public function floors()
    {
        if (empty($this->floors)) {
            $this->floors = $this->db
                ->from('floors')
                ->order_by('order', 'asc')
                ->get()
                ->result();
        }

        return $this->floors;
    }


    public function buildingAges()
    {
        if (empty($this->buildingAges)) {
            $this->buildingAges = $this->db
                ->from('building_ages')
                ->order_by('order', 'asc')
                ->get()
                ->result();
        }

        return $this->buildingAges;
    }


    public function heatings()
    {
        if (empty($this->heatings)) {
            $this->heatings = $this->db
                ->from('heatings')
                ->order_by('order', 'asc')
                ->get()
                ->result();
        }

        return $this->heatings;
    }


    public function usageStatuses()
    {
        if (empty($this->usageStatuses)) {
            $this->usageStatuses = $this->db
                ->from('usage_statuses')
                ->order_by('order', 'asc')
                ->get()
                ->result();
        }

        return $this->usageStatuses;
    }



    public function properties()
    {
        if (empty($this->properties)) {
            $this->properties = $this->db
                ->from('properties')
                ->order_by('order', 'asc')
                ->get()
                ->result();
        }

        return $this->properties;
    }


    public function exchangeInformation()
    {
        if (! $this->session->userdata('exchangeInformation')) {
            $open = simplexml_load_file('http://www.tcmb.gov.tr/kurlar/today.xml');

            $results = array();

            $results['usdBuy'] = (string) $open->Currency[0]->BanknoteBuying;
            $results['usdSell'] = (string) $open->Currency[0]->BanknoteSelling;

            $results['euroBuy'] = (string) $open->Currency[3]->BanknoteBuying;
            $results['euroSell'] = (string) $open->Currency[3]->BanknoteSelling;

            $results['sterlinBuy'] = (string) $open->Currency[4]->BanknoteBuying;
            $results['sterlinSell'] = (string) $open->Currency[4]->BanknoteSelling;

            $this->session->set_userdata('exchangeInformation', $results);
        }


        return $this->session->userdata('exchangeInformation');
    }



}