<?php

class Property extends CI_Model
{
    public function getProperties(){
      
        return $this->db
            ->from('properties')
            ->where('approved', 1)
            ->limit(18)
            ->get()
            ->result();
    }
    public function findProperty($id){
        return $this->db
            ->from('properties')
            ->where('id', $id)

            ->get()
            ->row();
    }



} 