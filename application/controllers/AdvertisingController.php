<?php


class AdvertisingController  extends CI_Controller
{

    public $module = 'advertising';



    public function index()
    {
        $this->load->model('advertising');

        $advertisings = array();
        $pagination = null;

        $status = $this->input->get('status');
        $investor = $this->input->get('investor');
        $build = $this->input->get('build');
        $kind = $this->input->get('kind');
        $inText = '';
        $buildText = '';
        if (empty($status)) {
            $status = null;
            $stText = '';
        } else if ($status == 1) {
            $stText = 'Kiralık';
        } else {
            $stText = 'Satılık';
        }

        if (empty($investor)) {
            $investor = null;
        }

        if ($investor == 1) {
            $inText = 'Yatırımcı Fırsatları';
        }

        if (empty($build)) {
            $build = null;
        }

        if ($build == 1) {
            $buildText = 'Yeni İnşaatlar';
        }

        $advertisingCount = $this->advertising->count(false, $status, $investor, $build);

        if ($advertisingCount > 0) {
            $config = array(
                'base_url' => clink(array('@advertising')),
                'total_rows' => $advertisingCount,
                'per_page' => 9
            );

            $this->load->library('pagination');
            $this->pagination->initialize($config);


            $advertisings = $this->advertising->all($this->pagination->per_page, $this->pagination->offset, false,
                $status, $investor, $build, $kind);
            $pagination = $this->pagination->create_links();
        }


        $this->load->view('master', array(
            'view' => 'advertising/index',
            'advertisings' => $advertisings,
            'emergencies' => $this->advertising->all(5, null, true),
            'pagination' => $pagination,
            'status' => $stText,
            'kind' => $kind,
            'inText' => $inText,
            'buildText' => $buildText,
        ));
    }

    public function search()
    {
        $this->load->model('advertising');
        $this->load->model('property');

        $advertisings = array();
        $pagination = null;
        $stText = '';
        if ($this->input->get()) {

            $advertisingCount = $this->advertising->filterCount();

            $status = $this->input->get('status');

            if (empty($status)) {
                $status = null;
            } else if ($status == 1) {
                $stText = 'Kiralık';
            } else {
                $stText = 'Satılık';
            }

            if ($advertisingCount > 0) {
                $config = array(
                    'base_url' => clink(array('@advertising', 'search')),
                    'total_rows' => $advertisingCount,
                    'per_page' => 8
                );

                $this->load->library('pagination');
                $this->pagination->initialize($config);


                $advertisings = $this->advertising->filter($this->pagination->per_page, $this->pagination->offset);
                $pagination = $this->pagination->create_links();
            }

        }

        $this->load->view('master', array(
            'view' => 'advertising/search',
            'advertisings' => $advertisings,
            'pagination' => $pagination,
            'status' => $stText
        ));
    }

    public function districts()
    {
        $this->load->model('advertising');
        $json = array('success' => true, 'data' => array());

        $townId = $this->input->post('townId');
        $districts = $this->advertising->getDistricts($townId);

        $json['data'][] = array('value' => '', 'label' => 'Seçiniz');
        foreach ($districts as $district) {
            $json['data'][] = array('value' => $district->id, 'label' => $district->title);
        }

        echo json_encode($json);
    }

    public function towns()
    {
        $this->load->model('advertising');
        $json = array('success' => true, 'data' => array());

        $townId = $this->input->post('cityId');
        $districts = $this->advertising->getTowns($townId);

        $json['data'][] = array('value' => '', 'label' => 'Seçiniz');
        foreach ($districts as $district) {
            $json['data'][] = array('value' => $district->id, 'label' => $district->title);
        }

        echo json_encode($json);
    }



    public function view($id)
    {
        $this->load->model('advertising');

        if (! $advertising = $this->advertising->findId($id, true)) {
            show_404();
        }

        $this->site->set('metaTitle', $advertising->title);

        $this->site->set('ogType', 'article');
        $this->site->set('ogTitle', $advertising->title);
        $this->site->set('ogImage', uploadPath($advertising->image, 'ilanlar'));

        $this->load->view('master', array(
            'view' => 'advertising/view',
            'advertising' => $advertising,
            'properties' => $this->advertising->propertyRelations($id),
            'emergencies' => $this->advertising->vision(5,null,true),
            'visions' => $this->advertising->vision(12),
        ));


    }

    public function yazdir($id)
    {
        $this->load->model('advertising');

        if (! $advertising = $this->advertising->yazdir($id)) {
            show_404();
        }

        $this->load->view('print', array(
            'advertising' => $advertising,
            'properties' => $this->advertising->propertyRelations($id),
        ));
    }

    public function sendMail($id)
    {
        $this->load->model('advertising');

        $advertising = $this->advertising->sendMail($id);

        if ($advertising) {
            $this->site->setAlert("success", "İlan başarıyla gönderildi.");
        } else {
            $this->site->setAlert("warning", "Gönderim sırasında bir sorun oluştu. Lütfen daha sonra tekrar deneyiniz.");
        }

        redirect($this->agent->referrer());
    }



    public function insert()
    {
        $this->load->model('advertising');
        $this->load->helper('form');
        $this->load->helper('bsform');


        if ($this->input->post()) {

            $this->load->library('form_validation');
            $this->form_validation->set_rules('insertStatus', 'Lütfen Emlak Durumunu seçiniz.', 'required');
            $this->form_validation->set_rules('insertCity', 'Lütfen Şehir seçiniz.', 'required');
            $this->form_validation->set_rules('insertTown', 'Lütfen İlçe seçiniz.', 'required');
            $this->form_validation->set_rules('insertDistrict', 'Lütfen Semt seçiniz.', 'required');
            $this->form_validation->set_rules('insertKind', 'Lütfen Emlak Türü seçiniz.', 'required');
            $this->form_validation->set_rules('insertType', 'Lütfen Emlak Tipi seçiniz.', 'required');
            $this->form_validation->set_rules('insertPrice', 'Lütfen Fiyat yazınız.', 'required');
            $this->form_validation->set_rules('insertDescription', 'Lütfen Açıklama yazınız.', 'required');
            $this->form_validation->set_rules('insertFullname', 'Lütfen Adınızı Soyadınızı yazınız.', 'required');
            $this->form_validation->set_rules('insertPhone', 'Lütfen Telefon numaranızı adresinizi yazınız.', 'required');
            $this->form_validation->set_rules('insertEmail', 'Lütfen E-Posta adresinizi yazınız.', 'required|valid_email');

            if ($this->form_validation->run() == false) {
                $this->site->setAlert('danger', $this->form_validation->error_string('<div>&bull; ', '</div>'));
            }


            if (! $this->site->isAlert()) {
                $config = array(
                    'upload_path' => 'public/upload/ilanlar',
                    'allowed_types' => 'gif|jpg|png',
                    'encrypt_name' => true
                );
                $this->load->library('upload', $config);

                if (! $this->upload->do_upload('insertImage')) {
                    $this->site->setAlert('danger', $this->upload->display_errors('<div>&bull; ', '</div>'));
                }
            }


            if (! $this->site->isAlert()) {
                $data = $this->upload->data();
                $imageData =  array(
                    'name' => $data['file_name'],
                    'path' => $data['full_path'],
                    'ext' => $data['file_ext'],
                    'width' => $data['image_width'],
                    'height' => $data['image_height']
                );

                if ($imageData['width'] < 600 || $imageData['height'] < 450) {
                    $this->site->setAlert('danger', '<div>&bull; Resim boyutları en az 600x450px olmalı.</div>');
                }
            }


            if (! $this->site->isAlert()) {
                $success = $this->advertising->insert($imageData);

                if ($success) {
                    $this->site->setAlert('success', 'Emlak ilanınız başarıyla kaldedildi.');
                    redirect(current_url());
                }
            }
        }



        $this->load->view('master', array(
            'view' => 'advertising/insert',
        ));

    }



} 