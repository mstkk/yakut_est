<?php


class ConsultantController  extends CI_Controller
{

    public $module = 'consultant';



    public function index()
    {
        $this->load->model('consultant');
        $this->load->model('advertising');

        $consultant = array();
        $pagination = null;
        $consultantCount = $this->consultant->count();

        if ($consultantCount > 0) {
            $config = array(
                'base_url' => clink(array('@consultant')),
                'total_rows' => $consultantCount,
                'per_page' => 20
            );

            $this->load->library('pagination');
            $this->pagination->initialize($config);


            $consultant = $this->consultant->all($this->pagination->per_page, $this->pagination->offset);
            $pagination = $this->pagination->create_links();
        }


        $this->load->view('master', array(
            'view' => 'consultant/index',
            'consultants' => $consultant,
            'emergencies' => $this->advertising->vision(5,null,true),
            'pagination' => $pagination,

        ));
    }




    public function view($id)
    {
        $this->load->model('consultant');

        if (! $consultants = $this->consultant->findId($id, true)) {
            show_404();
        }

        // @todo pagination yapılacak ve düzenlenecek.
        $consultant = array();
        $pagination = null;
        $consultantCount = $this->consultant->count();

        if ($consultantCount > 0) {
            $config = array(
                'base_url' => clink(array('@consultant')),
                'total_rows' => $consultantCount,
                'per_page' => 20
            );

            $this->load->library('pagination');
            $this->pagination->initialize($config);


            $consultant = $this->consultant->vision($id, $this->pagination->per_page, $this->pagination->offset);
            $pagination = $this->pagination->create_links();
        }
        /* ################### */

        $this->site->set('metaTitle', $consultants->name);

        $this->site->set('ogType', 'article');
        $this->site->set('ogTitle', $consultants->name);
        $this->site->set('ogImage', uploadPath($consultants->image, 'consultant'));

        $emergncy = $this->consultant->vision($id, 5, null, true);

        $this->load->view('master', array(
            'view' => 'consultant/view',
            'consultants' => $consultants,
            'emergencies' => $emergncy->adverts,
            'visions' => $consultant,
            'paginate' => $pagination
        ));


    }



} 