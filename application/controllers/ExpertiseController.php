<?php


class ExpertiseController  extends CI_Controller
{
    public $module = 'expertise';

    public function index()
    {
        $this->load->model('expertise');
        $this->load->helper('form');

        if ($this->input->post()) {

            $this->load->library('form_validation');
            $this->form_validation->set_rules('t_fullname', 'Talep eden ad soyad boş bırakılmamalı.', 'required');
            $this->form_validation->set_rules('t_phone', 'Talep eden telefon no boş bırakılmamalı', 'required');
            $this->form_validation->set_rules('t_email', 'Talep eden mail boş bırakılmamalı', 'required|valid_email');
            $this->form_validation->set_rules('g_city', 'Gayrimenkul şehir boş bırakılmamalı', 'required');
            $this->form_validation->set_rules('g_town', 'Gayrimenkul ilçe boş bırakılmamalı', 'required');
            $this->form_validation->set_rules('g_address', 'Gayrimenkul adres boş bırakılmamalı', 'required');
            $this->form_validation->set_rules('k_fullname', 'Gösterecek kişi ad soyad boş bırakılmamalı', 'required');
            $this->form_validation->set_rules('k_phone', 'Gösterecek kişi telefon boş bırakılmamalı', 'required');

            if ($this->form_validation->run() == true) {
                $success = $this->expertise->insert();

                if ($success) {
                    $this->site->setAlert('success', 'Talebiniz başarıyla bize ulaşmıştır.');
                    redirect(current_url());
                }
            } else {
                $this->site->setAlert('danger', $this->form_validation->error_string('<div>&bull; ', '</div>'));
            }
        }



        $this->load->view('master', array(
            'view' => 'expertise/index',
        ));

    }



} 