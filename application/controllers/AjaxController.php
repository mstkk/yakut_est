<?php


class AjaxController  extends CI_Controller
{


    public function towns()
    {
        $json = array('success' => true, 'data' => array());


        if ($this->input->post()) {
            $id = $this->input->post('id');
            $records = $this->common->towns($id);

            $json['data'][] = array('value' => '', 'label' => '');
            foreach ($records as $record) {
                $json['data'][] = array('value' => $record->id, 'label' => $record->title);
            }
        }

        echo json_encode($json);
    }




    public function districts()
    {
        $json = array('success' => true, 'data' => array());


        if ($this->input->post()) {
            $id = $this->input->post('id');
            $records = $this->common->districts($id);

            $json['data'][] = array('value' => '', 'label' => '');
            foreach ($records as $record) {
                $json['data'][] = array('value' => $record->id, 'label' => $record->title);
            }
        }

        echo json_encode($json);
    }



    public function types()
    {
        $json = array('success' => true, 'data' => array());


        if ($this->input->post()) {
            $id = $this->input->post('id');
            $records = $this->common->types($id);

            $json['data'][] = array('value' => '', 'label' => '');
            foreach ($records as $record) {
                $json['data'][] = array('value' => $record->id, 'label' => $record->title);
            }
        }

        echo json_encode($json);
    }


} 